<?php

namespace foues\FPBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CatTrataResta
 *
 * @ORM\Table(name="cat_trata_resta", uniqueConstraints={@ORM\UniqueConstraint(name="cat_trata_resta_pk", columns={"id_cat_resta"})})
 * @ORM\Entity
 */
class CatTrataResta
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_cat_resta", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="cat_trata_resta_id_cat_resta_seq", allocationSize=1, initialValue=1)
     */
    private $idCatResta;

    /**
     * @var string
     *
     * @ORM\Column(name="nom_tratam", type="string", length=40, nullable=true)
     */
    private $nomTratam;



    /**
     * Get idCatResta
     *
     * @return integer 
     */
    public function getIdCatResta()
    {
        return $this->idCatResta;
    }

    /**
     * Set nomTratam
     *
     * @param string $nomTratam
     * @return CatTrataResta
     */
    public function setNomTratam($nomTratam)
    {
        $this->nomTratam = $nomTratam;

        return $this;
    }

    /**
     * Get nomTratam
     *
     * @return string 
     */
    public function getNomTratam()
    {
        return $this->nomTratam;
    }
}
