<?php

namespace foues\FPBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ControlPerio
 *
 * @ORM\Table(name="control_perio", uniqueConstraints={@ORM\UniqueConstraint(name="control_perio_pk", columns={"id_control"})}, indexes={@ORM\Index(name="fk_control__se_somete_f_period_", columns={"id_f_perio"})})
 * @ORM\Entity
 */
class ControlPerio
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_control", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="control_perio_id_control_seq", allocationSize=1, initialValue=1)
     */
    private $idControl;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha_inicio", type="date", nullable=true)
     */
    private $fechaInicio;

    /**
     * @var integer
     *
     * @ORM\Column(name="cant_cara_co", type="integer", nullable=true)
     */
    private $cantCaraCo;

    /**
     * @var integer
     *
     * @ORM\Column(name="cant_d_pres", type="integer", nullable=true)
     */
    private $cantDPres;

    /**
     * @var float
     *
     * @ORM\Column(name="resultado_t", type="float", precision=10, scale=0, nullable=true)
     */
    private $resultadoT;

    /**
     * @var string
     *
     * @ORM\Column(name="imagen_cpd", type="string", length=255, nullable=true)
     */
    private $imagenCpd;

    /**
     * @var \FPeriodoncia
     *
     * @ORM\ManyToOne(targetEntity="FPeriodoncia")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_f_perio", referencedColumnName="id_f_perio")
     * })
     */
    private $idFPerio;



    /**
     * Get idControl
     *
     * @return integer 
     */
    public function getIdControl()
    {
        return $this->idControl;
    }

    /**
     * Set fechaInicio
     *
     * @param \DateTime $fechaInicio
     * @return ControlPerio
     */
    public function setFechaInicio($fechaInicio)
    {
        $this->fechaInicio = $fechaInicio;

        return $this;
    }

    /**
     * Get fechaInicio
     *
     * @return \DateTime 
     */
    public function getFechaInicio()
    {
        return $this->fechaInicio;
    }

    /**
     * Set cantCaraCo
     *
     * @param integer $cantCaraCo
     * @return ControlPerio
     */
    public function setCantCaraCo($cantCaraCo)
    {
        $this->cantCaraCo = $cantCaraCo;

        return $this;
    }

    /**
     * Get cantCaraCo
     *
     * @return integer 
     */
    public function getCantCaraCo()
    {
        return $this->cantCaraCo;
    }

    /**
     * Set cantDPres
     *
     * @param integer $cantDPres
     * @return ControlPerio
     */
    public function setCantDPres($cantDPres)
    {
        $this->cantDPres = $cantDPres;

        return $this;
    }

    /**
     * Get cantDPres
     *
     * @return integer 
     */
    public function getCantDPres()
    {
        return $this->cantDPres;
    }

    /**
     * Set resultadoT
     *
     * @param float $resultadoT
     * @return ControlPerio
     */
    public function setResultadoT($resultadoT)
    {
        $this->resultadoT = $resultadoT;

        return $this;
    }

    /**
     * Get resultadoT
     *
     * @return float 
     */
    public function getResultadoT()
    {
        return $this->resultadoT;
    }

    /**
     * Set imagenCpd
     *
     * @param string $imagenCpd
     * @return ControlPerio
     */
    public function setImagenCpd($imagenCpd)
    {
        $this->imagenCpd = $imagenCpd;

        return $this;
    }

    /**
     * Get imagenCpd
     *
     * @return string 
     */
    public function getImagenCpd()
    {
        return $this->imagenCpd;
    }

    /**
     * Set idFPerio
     *
     * @param \foues\FPBundle\Entity\FPeriodoncia $idFPerio
     * @return ControlPerio
     */
    public function setIdFPerio(\foues\FPBundle\Entity\FPeriodoncia $idFPerio = null)
    {
        $this->idFPerio = $idFPerio;

        return $this;
    }

    /**
     * Get idFPerio
     *
     * @return \foues\FPBundle\Entity\FPeriodoncia 
     */
    public function getIdFPerio()
    {
        return $this->idFPerio;
    }
}
