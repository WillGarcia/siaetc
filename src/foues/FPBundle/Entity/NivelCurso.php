<?php

namespace foues\FPBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * NivelCurso
 *
 * @ORM\Table(name="nivel_curso", uniqueConstraints={@ORM\UniqueConstraint(name="nivel_curso_pk", columns={"id_nivel"})}, indexes={@ORM\Index(name="fk_nivel_cu_contiene3_ciclo_fk", columns={"id_ciclo"})})
 * @ORM\Entity
 */
class NivelCurso
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_nivel", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="nivel_curso_id_nivel_seq", allocationSize=1, initialValue=1)
     */
    private $idNivel;

    /**
     * @var string
     *
     * @ORM\Column(name="nom_curso", type="string", length=15, nullable=false)
     */
    private $nomCurso;

    /**
     * @var \Ciclo
     *
     * @ORM\ManyToOne(targetEntity="Ciclo")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_ciclo", referencedColumnName="id_ciclo")
     * })
     */
    private $idCiclo;



    /**
     * Get idNivel
     *
     * @return integer 
     */
    public function getIdNivel()
    {
        return $this->idNivel;
    }

    /**
     * Set nomCurso
     *
     * @param string $nomCurso
     * @return NivelCurso
     */
    public function setNomCurso($nomCurso)
    {
        $this->nomCurso = $nomCurso;

        return $this;
    }

    /**
     * Get nomCurso
     *
     * @return string 
     */
    public function getNomCurso()
    {
        return $this->nomCurso;
    }

    /**
     * Set idCiclo
     *
     * @param \foues\FPBundle\Entity\Ciclo $idCiclo
     * @return NivelCurso
     */
    public function setIdCiclo(\foues\FPBundle\Entity\Ciclo $idCiclo = null)
    {
        $this->idCiclo = $idCiclo;

        return $this;
    }

    /**
     * Get idCiclo
     *
     * @return \foues\FPBundle\Entity\Ciclo 
     */
    public function getIdCiclo()
    {
        return $this->idCiclo;
    }
}
