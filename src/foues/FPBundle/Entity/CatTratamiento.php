<?php

namespace foues\FPBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CatTratamiento
 *
 * @ORM\Table(name="cat_tratamiento", uniqueConstraints={@ORM\UniqueConstraint(name="cat_tratamiento_pk", columns={"id_cata_trata"})})
 * @ORM\Entity
 */
class CatTratamiento
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_cata_trata", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="cat_tratamiento_id_cata_trata_seq", allocationSize=1, initialValue=1)
     */
    private $idCataTrata;

    /**
     * @var string
     *
     * @ORM\Column(name="nom_trata", type="string", length=60, nullable=true)
     */
    private $nomTrata;

    /**
     * @var string
     *
     * @ORM\Column(name="precio", type="decimal", precision=5, scale=2, nullable=true)
     */
    private $precio;



    /**
     * Get idCataTrata
     *
     * @return integer 
     */
    public function getIdCataTrata()
    {
        return $this->idCataTrata;
    }

    /**
     * Set nomTrata
     *
     * @param string $nomTrata
     * @return CatTratamiento
     */
    public function setNomTrata($nomTrata)
    {
        $this->nomTrata = $nomTrata;

        return $this;
    }

    /**
     * Get nomTrata
     *
     * @return string 
     */
    public function getNomTrata()
    {
        return $this->nomTrata;
    }

    /**
     * Set precio
     *
     * @param string $precio
     * @return CatTratamiento
     */
    public function setPrecio($precio)
    {
        $this->precio = $precio;

        return $this;
    }

    /**
     * Get precio
     *
     * @return string 
     */
    public function getPrecio()
    {
        return $this->precio;
    }
}
