<?php

namespace foues\FPBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ImagenControlp
 *
 * @ORM\Table(name="imagen_controlp")
 * @ORM\Entity
 */
class ImagenControlp
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_imagenc", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="imagen_controlp_id_imagenc_seq", allocationSize=1, initialValue=1)
     */
    private $idImagenc;

    /**
     * @var string
     *
     * @ORM\Column(name="imagen", type="string", length=255, nullable=false)
     */
    private $imagen;



    /**
     * Get idImagenc
     *
     * @return integer 
     */
    public function getIdImagenc()
    {
        return $this->idImagenc;
    }

    /**
     * Set imagen
     *
     * @param string $imagen
     * @return ImagenControlp
     */
    public function setImagen($imagen)
    {
        $this->imagen = $imagen;

        return $this;
    }

    /**
     * Get imagen
     *
     * @return string 
     */
    public function getImagen()
    {
        return $this->imagen;
    }
}
