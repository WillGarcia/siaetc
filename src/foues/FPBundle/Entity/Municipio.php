<?php

namespace foues\FPBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Municipio
 *
 * @ORM\Table(name="municipio", indexes={@ORM\Index(name="IDX_FE98F5E06325E299", columns={"id_departamento"})})
 * @ORM\Entity
 */
class Municipio
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_municipio", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="municipio_id_municipio_seq", allocationSize=1, initialValue=1)
     */
    private $idMunicipio;

    /**
     * @var string
     *
     * @ORM\Column(name="nom_municipio", type="string", length=30, nullable=false)
     */
    private $nomMunicipio;

    /**
     * @var \Departamento
     *
     * @ORM\ManyToOne(targetEntity="Departamento")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_departamento", referencedColumnName="id_departamento")
     * })
     */
    private $idDepartamento;



    /**
     * Get idMunicipio
     *
     * @return integer 
     */
    public function getIdMunicipio()
    {
        return $this->idMunicipio;
    }

    /**
     * Set nomMunicipio
     *
     * @param string $nomMunicipio
     * @return Municipio
     */
    public function setNomMunicipio($nomMunicipio)
    {
        $this->nomMunicipio = $nomMunicipio;

        return $this;
    }

    /**
     * Get nomMunicipio
     *
     * @return string 
     */
    public function getNomMunicipio()
    {
        return $this->nomMunicipio;
    }

    /**
     * Set idDepartamento
     *
     * @param \foues\FPBundle\Entity\Departamento $idDepartamento
     * @return Municipio
     */
    public function setIdDepartamento(\foues\FPBundle\Entity\Departamento $idDepartamento = null)
    {
        $this->idDepartamento = $idDepartamento;

        return $this;
    }

    /**
     * Get idDepartamento
     *
     * @return \foues\FPBundle\Entity\Departamento 
     */
    public function getIdDepartamento()
    {
        return $this->idDepartamento;
    }
}
