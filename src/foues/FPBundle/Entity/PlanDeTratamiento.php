<?php

namespace foues\FPBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PlanDeTratamiento
 *
 * @ORM\Table(name="plan_de_tratamiento", uniqueConstraints={@ORM\UniqueConstraint(name="plan_de_tratamiento_pk", columns={"id_plan"})}, indexes={@ORM\Index(name="relationship_89_fk2", columns={"id_f_dx"}), @ORM\Index(name="relationship_88_fk", columns={"id_cata_trata"})})
 * @ORM\Entity
 */
class PlanDeTratamiento
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_plan", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="plan_de_tratamiento_id_plan_seq", allocationSize=1, initialValue=1)
     */
    private $idPlan;

    /**
     * @var string
     *
     * @ORM\Column(name="descripcion", type="string", length=100, nullable=true)
     */
    private $descripcion;

    /**
     * @var \CatTratamiento
     *
     * @ORM\ManyToOne(targetEntity="CatTratamiento")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_cata_trata", referencedColumnName="id_cata_trata")
     * })
     */
    private $idCataTrata;

    /**
     * @var \FDiagnostico
     *
     * @ORM\ManyToOne(targetEntity="FDiagnostico")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_f_dx", referencedColumnName="id_f_dx")
     * })
     */
    private $idFDx;



    /**
     * Get idPlan
     *
     * @return integer 
     */
    public function getIdPlan()
    {
        return $this->idPlan;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     * @return PlanDeTratamiento
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string 
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set idCataTrata
     *
     * @param \foues\FPBundle\Entity\CatTratamiento $idCataTrata
     * @return PlanDeTratamiento
     */
    public function setIdCataTrata(\foues\FPBundle\Entity\CatTratamiento $idCataTrata = null)
    {
        $this->idCataTrata = $idCataTrata;

        return $this;
    }

    /**
     * Get idCataTrata
     *
     * @return \foues\FPBundle\Entity\CatTratamiento 
     */
    public function getIdCataTrata()
    {
        return $this->idCataTrata;
    }

    /**
     * Set idFDx
     *
     * @param \foues\FPBundle\Entity\FDiagnostico $idFDx
     * @return PlanDeTratamiento
     */
    public function setIdFDx(\foues\FPBundle\Entity\FDiagnostico $idFDx = null)
    {
        $this->idFDx = $idFDx;

        return $this;
    }

    /**
     * Get idFDx
     *
     * @return \foues\FPBundle\Entity\FDiagnostico 
     */
    public function getIdFDx()
    {
        return $this->idFDx;
    }
}
