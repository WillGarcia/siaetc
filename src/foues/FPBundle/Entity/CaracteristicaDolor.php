<?php

namespace foues\FPBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CaracteristicaDolor
 *
 * @ORM\Table(name="caracteristica_dolor", uniqueConstraints={@ORM\UniqueConstraint(name="caracteristica_dolor_pk", columns={"id_caracteristica"})}, indexes={@ORM\Index(name="relationship_91_fk", columns={"id_f_dx"})})
 * @ORM\Entity
 */
class CaracteristicaDolor
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_caracteristica", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="caracteristica_dolor_id_caracteristica_seq", allocationSize=1, initialValue=1)
     */
    private $idCaracteristica;

    /**
     * @var string
     *
     * @ORM\Column(name="intensidad", type="string", length=60, nullable=true)
     */
    private $intensidad;

    /**
     * @var string
     *
     * @ORM\Column(name="frecuencia", type="string", length=60, nullable=true)
     */
    private $frecuencia;

    /**
     * @var string
     *
     * @ORM\Column(name="duracion", type="string", length=50, nullable=true)
     */
    private $duracion;

    /**
     * @var string
     *
     * @ORM\Column(name="localizacion", type="string", length=80, nullable=true)
     */
    private $localizacion;

    /**
     * @var string
     *
     * @ORM\Column(name="caracteristica", type="string", length=70, nullable=true)
     */
    private $caracteristica;

    /**
     * @var string
     *
     * @ORM\Column(name="fact__dispara_grav", type="string", length=50, nullable=true)
     */
    private $factDisparaGrav;

    /**
     * @var string
     *
     * @ORM\Column(name="fact_mejoria", type="string", length=50, nullable=true)
     */
    private $factMejoria;

    /**
     * @var string
     *
     * @ORM\Column(name="signos_asocia", type="string", length=80, nullable=true)
     */
    private $signosAsocia;

    /**
     * @var string
     *
     * @ORM\Column(name="patrones_ref", type="string", length=80, nullable=true)
     */
    private $patronesRef;

    /**
     * @var \FDiagnostico
     *
     * @ORM\ManyToOne(targetEntity="FDiagnostico")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_f_dx", referencedColumnName="id_f_dx")
     * })
     */
    private $idFDx;



    /**
     * Get idCaracteristica
     *
     * @return integer 
     */
    public function getIdCaracteristica()
    {
        return $this->idCaracteristica;
    }

    /**
     * Set intensidad
     *
     * @param string $intensidad
     * @return CaracteristicaDolor
     */
    public function setIntensidad($intensidad)
    {
        $this->intensidad = $intensidad;

        return $this;
    }

    /**
     * Get intensidad
     *
     * @return string 
     */
    public function getIntensidad()
    {
        return $this->intensidad;
    }

    /**
     * Set frecuencia
     *
     * @param string $frecuencia
     * @return CaracteristicaDolor
     */
    public function setFrecuencia($frecuencia)
    {
        $this->frecuencia = $frecuencia;

        return $this;
    }

    /**
     * Get frecuencia
     *
     * @return string 
     */
    public function getFrecuencia()
    {
        return $this->frecuencia;
    }

    /**
     * Set duracion
     *
     * @param string $duracion
     * @return CaracteristicaDolor
     */
    public function setDuracion($duracion)
    {
        $this->duracion = $duracion;

        return $this;
    }

    /**
     * Get duracion
     *
     * @return string 
     */
    public function getDuracion()
    {
        return $this->duracion;
    }

    /**
     * Set localizacion
     *
     * @param string $localizacion
     * @return CaracteristicaDolor
     */
    public function setLocalizacion($localizacion)
    {
        $this->localizacion = $localizacion;

        return $this;
    }

    /**
     * Get localizacion
     *
     * @return string 
     */
    public function getLocalizacion()
    {
        return $this->localizacion;
    }

    /**
     * Set caracteristica
     *
     * @param string $caracteristica
     * @return CaracteristicaDolor
     */
    public function setCaracteristica($caracteristica)
    {
        $this->caracteristica = $caracteristica;

        return $this;
    }

    /**
     * Get caracteristica
     *
     * @return string 
     */
    public function getCaracteristica()
    {
        return $this->caracteristica;
    }

    /**
     * Set factDisparaGrav
     *
     * @param string $factDisparaGrav
     * @return CaracteristicaDolor
     */
    public function setFactDisparaGrav($factDisparaGrav)
    {
        $this->factDisparaGrav = $factDisparaGrav;

        return $this;
    }

    /**
     * Get factDisparaGrav
     *
     * @return string 
     */
    public function getFactDisparaGrav()
    {
        return $this->factDisparaGrav;
    }

    /**
     * Set factMejoria
     *
     * @param string $factMejoria
     * @return CaracteristicaDolor
     */
    public function setFactMejoria($factMejoria)
    {
        $this->factMejoria = $factMejoria;

        return $this;
    }

    /**
     * Get factMejoria
     *
     * @return string 
     */
    public function getFactMejoria()
    {
        return $this->factMejoria;
    }

    /**
     * Set signosAsocia
     *
     * @param string $signosAsocia
     * @return CaracteristicaDolor
     */
    public function setSignosAsocia($signosAsocia)
    {
        $this->signosAsocia = $signosAsocia;

        return $this;
    }

    /**
     * Get signosAsocia
     *
     * @return string 
     */
    public function getSignosAsocia()
    {
        return $this->signosAsocia;
    }

    /**
     * Set patronesRef
     *
     * @param string $patronesRef
     * @return CaracteristicaDolor
     */
    public function setPatronesRef($patronesRef)
    {
        $this->patronesRef = $patronesRef;

        return $this;
    }

    /**
     * Get patronesRef
     *
     * @return string 
     */
    public function getPatronesRef()
    {
        return $this->patronesRef;
    }

    /**
     * Set idFDx
     *
     * @param \foues\FPBundle\Entity\FDiagnostico $idFDx
     * @return CaracteristicaDolor
     */
    public function setIdFDx(\foues\FPBundle\Entity\FDiagnostico $idFDx = null)
    {
        $this->idFDx = $idFDx;

        return $this;
    }

    /**
     * Get idFDx
     *
     * @return \foues\FPBundle\Entity\FDiagnostico 
     */
    public function getIdFDx()
    {
        return $this->idFDx;
    }
}
