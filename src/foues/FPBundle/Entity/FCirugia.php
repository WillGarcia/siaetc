<?php

namespace foues\FPBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * FCirugia
 *
 * @ORM\Table(name="f_cirugia", uniqueConstraints={@ORM\UniqueConstraint(name="f_cirugia_pk", columns={"id_f_cirugia"})}, indexes={@ORM\Index(name="fk_f_cirugi_forma_una_ficha_cl_", columns={"num_expediente"})})
 * @ORM\Entity
 */
class FCirugia
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_f_cirugia", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="f_cirugia_id_f_cirugia_seq", allocationSize=1, initialValue=1)
     */
    private $idFCirugia;

    /**
     * @var integer
     *
     * @ORM\Column(name="area_ficha", type="integer", nullable=false)
     */
    private $areaFicha;

    /**
     * @var string
     *
     * @ORM\Column(name="motivo_cons", type="string", length=100, nullable=true)
     */
    private $motivoCons;

    /**
     * @var string
     *
     * @ORM\Column(name="hist_medica", type="string", length=100, nullable=true)
     */
    private $histMedica;

    /**
     * @var string
     *
     * @ORM\Column(name="hist_odonto", type="string", length=100, nullable=true)
     */
    private $histOdonto;

    /**
     * @var string
     *
     * @ORM\Column(name="exa_clini_ci", type="string", length=100, nullable=true)
     */
    private $exaCliniCi;

    /**
     * @var string
     *
     * @ORM\Column(name="diagnostico_ci", type="string", length=100, nullable=true)
     */
    private $diagnosticoCi;

    /**
     * @var string
     *
     * @ORM\Column(name="plan_tratam", type="string", length=200, nullable=true)
     */
    private $planTratam;

    /**
     * @var string
     *
     * @ORM\Column(name="desc_pro_1", type="string", length=100, nullable=true)
     */
    private $descPro1;

    /**
     * @var string
     *
     * @ORM\Column(name="ind_post_oper_1", type="string", length=100, nullable=true)
     */
    private $indPostOper1;

    /**
     * @var string
     *
     * @ORM\Column(name="desc_pro_2", type="string", length=100, nullable=true)
     */
    private $descPro2;

    /**
     * @var string
     *
     * @ORM\Column(name="ind_post_oper_2", type="string", length=100, nullable=true)
     */
    private $indPostOper2;

    /**
     * @var string
     *
     * @ORM\Column(name="desc_pro_3", type="string", length=100, nullable=true)
     */
    private $descPro3;

    /**
     * @var string
     *
     * @ORM\Column(name="ind_post_oper_3", type="string", length=100, nullable=true)
     */
    private $indPostOper3;

    /**
     * @var \FichaClinica
     *
     * @ORM\ManyToOne(targetEntity="FichaClinica")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="num_expediente", referencedColumnName="num_expediente")
     * })
     */
    private $numExpediente;



    /**
     * Get idFCirugia
     *
     * @return integer 
     */
    public function getIdFCirugia()
    {
        return $this->idFCirugia;
    }

    /**
     * Set areaFicha
     *
     * @param integer $areaFicha
     * @return FCirugia
     */
    public function setAreaFicha($areaFicha)
    {
        $this->areaFicha = $areaFicha;

        return $this;
    }

    /**
     * Get areaFicha
     *
     * @return integer 
     */
    public function getAreaFicha()
    {
        return $this->areaFicha;
    }

    /**
     * Set motivoCons
     *
     * @param string $motivoCons
     * @return FCirugia
     */
    public function setMotivoCons($motivoCons)
    {
        $this->motivoCons = $motivoCons;

        return $this;
    }

    /**
     * Get motivoCons
     *
     * @return string 
     */
    public function getMotivoCons()
    {
        return $this->motivoCons;
    }

    /**
     * Set histMedica
     *
     * @param string $histMedica
     * @return FCirugia
     */
    public function setHistMedica($histMedica)
    {
        $this->histMedica = $histMedica;

        return $this;
    }

    /**
     * Get histMedica
     *
     * @return string 
     */
    public function getHistMedica()
    {
        return $this->histMedica;
    }

    /**
     * Set histOdonto
     *
     * @param string $histOdonto
     * @return FCirugia
     */
    public function setHistOdonto($histOdonto)
    {
        $this->histOdonto = $histOdonto;

        return $this;
    }

    /**
     * Get histOdonto
     *
     * @return string 
     */
    public function getHistOdonto()
    {
        return $this->histOdonto;
    }

    /**
     * Set exaCliniCi
     *
     * @param string $exaCliniCi
     * @return FCirugia
     */
    public function setExaCliniCi($exaCliniCi)
    {
        $this->exaCliniCi = $exaCliniCi;

        return $this;
    }

    /**
     * Get exaCliniCi
     *
     * @return string 
     */
    public function getExaCliniCi()
    {
        return $this->exaCliniCi;
    }

    /**
     * Set diagnosticoCi
     *
     * @param string $diagnosticoCi
     * @return FCirugia
     */
    public function setDiagnosticoCi($diagnosticoCi)
    {
        $this->diagnosticoCi = $diagnosticoCi;

        return $this;
    }

    /**
     * Get diagnosticoCi
     *
     * @return string 
     */
    public function getDiagnosticoCi()
    {
        return $this->diagnosticoCi;
    }

    /**
     * Set planTratam
     *
     * @param string $planTratam
     * @return FCirugia
     */
    public function setPlanTratam($planTratam)
    {
        $this->planTratam = $planTratam;

        return $this;
    }

    /**
     * Get planTratam
     *
     * @return string 
     */
    public function getPlanTratam()
    {
        return $this->planTratam;
    }

    /**
     * Set descPro1
     *
     * @param string $descPro1
     * @return FCirugia
     */
    public function setDescPro1($descPro1)
    {
        $this->descPro1 = $descPro1;

        return $this;
    }

    /**
     * Get descPro1
     *
     * @return string 
     */
    public function getDescPro1()
    {
        return $this->descPro1;
    }

    /**
     * Set indPostOper1
     *
     * @param string $indPostOper1
     * @return FCirugia
     */
    public function setIndPostOper1($indPostOper1)
    {
        $this->indPostOper1 = $indPostOper1;

        return $this;
    }

    /**
     * Get indPostOper1
     *
     * @return string 
     */
    public function getIndPostOper1()
    {
        return $this->indPostOper1;
    }

    /**
     * Set descPro2
     *
     * @param string $descPro2
     * @return FCirugia
     */
    public function setDescPro2($descPro2)
    {
        $this->descPro2 = $descPro2;

        return $this;
    }

    /**
     * Get descPro2
     *
     * @return string 
     */
    public function getDescPro2()
    {
        return $this->descPro2;
    }

    /**
     * Set indPostOper2
     *
     * @param string $indPostOper2
     * @return FCirugia
     */
    public function setIndPostOper2($indPostOper2)
    {
        $this->indPostOper2 = $indPostOper2;

        return $this;
    }

    /**
     * Get indPostOper2
     *
     * @return string 
     */
    public function getIndPostOper2()
    {
        return $this->indPostOper2;
    }

    /**
     * Set descPro3
     *
     * @param string $descPro3
     * @return FCirugia
     */
    public function setDescPro3($descPro3)
    {
        $this->descPro3 = $descPro3;

        return $this;
    }

    /**
     * Get descPro3
     *
     * @return string 
     */
    public function getDescPro3()
    {
        return $this->descPro3;
    }

    /**
     * Set indPostOper3
     *
     * @param string $indPostOper3
     * @return FCirugia
     */
    public function setIndPostOper3($indPostOper3)
    {
        $this->indPostOper3 = $indPostOper3;

        return $this;
    }

    /**
     * Get indPostOper3
     *
     * @return string 
     */
    public function getIndPostOper3()
    {
        return $this->indPostOper3;
    }

    /**
     * Set numExpediente
     *
     * @param \foues\FPBundle\Entity\FichaClinica $numExpediente
     * @return FCirugia
     */
    public function setNumExpediente(\foues\FPBundle\Entity\FichaClinica $numExpediente = null)
    {
        $this->numExpediente = $numExpediente;

        return $this;
    }

    /**
     * Get numExpediente
     *
     * @return \foues\FPBundle\Entity\FichaClinica 
     */
    public function getNumExpediente()
    {
        return $this->numExpediente;
    }
}
