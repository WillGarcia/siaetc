<?php

namespace foues\FPBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Estudiante
 *
 * @ORM\Table(name="estudiante", uniqueConstraints={@ORM\UniqueConstraint(name="estudiante_pk", columns={"due"})})
 * @ORM\Entity
 */
class Estudiante
{
    /**
     * @var string
     *
     * @ORM\Column(name="due", type="string", length=7, nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="estudiante_due_seq", allocationSize=1, initialValue=1)
     */
    private $due;

    /**
     * @var string
     *
     * @ORM\Column(name="genero", type="string", length=10, nullable=false)
     */
    private $genero;

    /**
     * @var string
     *
     * @ORM\Column(name="nombres", type="string", length=70, nullable=false)
     */
    private $nombres;

    /**
     * @var string
     *
     * @ORM\Column(name="apellidos", type="string", length=70, nullable=false)
     */
    private $apellidos;

    /**
     * @var boolean
     *
     * @ORM\Column(name="inactivo", type="boolean", nullable=true)
     */
    private $inactivo;



    /**
     * Get due
     *
     * @return string 
     */
    public function getDue()
    {
        return $this->due;
    }

    /**
     * Set genero
     *
     * @param string $genero
     * @return Estudiante
     */
    public function setGenero($genero)
    {
        $this->genero = $genero;

        return $this;
    }

    /**
     * Get genero
     *
     * @return string 
     */
    public function getGenero()
    {
        return $this->genero;
    }

    /**
     * Set nombres
     *
     * @param string $nombres
     * @return Estudiante
     */
    public function setNombres($nombres)
    {
        $this->nombres = $nombres;

        return $this;
    }

    /**
     * Get nombres
     *
     * @return string 
     */
    public function getNombres()
    {
        return $this->nombres;
    }

    /**
     * Set apellidos
     *
     * @param string $apellidos
     * @return Estudiante
     */
    public function setApellidos($apellidos)
    {
        $this->apellidos = $apellidos;

        return $this;
    }

    /**
     * Get apellidos
     *
     * @return string 
     */
    public function getApellidos()
    {
        return $this->apellidos;
    }

    /**
     * Set inactivo
     *
     * @param boolean $inactivo
     * @return Estudiante
     */
    public function setInactivo($inactivo)
    {
        $this->inactivo = $inactivo;

        return $this;
    }

    /**
     * Get inactivo
     *
     * @return boolean 
     */
    public function getInactivo()
    {
        return $this->inactivo;
    }
}
