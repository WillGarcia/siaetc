<?php

namespace foues\FPBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PruebasTeDx
 *
 * @ORM\Table(name="pruebas_te_dx", uniqueConstraints={@ORM\UniqueConstraint(name="pruebas_te_dx_pk", columns={"id_prueba"})}, indexes={@ORM\Index(name="fk_pruebas__considera_eva_pulp_", columns={"id_eva_pul"})})
 * @ORM\Entity
 */
class PruebasTeDx
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_prueba", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="pruebas_te_dx_id_prueba_seq", allocationSize=1, initialValue=1)
     */
    private $idPrueba;

    /**
     * @var string
     *
     * @ORM\Column(name="frio", type="string", length=2, nullable=true)
     */
    private $frio;

    /**
     * @var string
     *
     * @ORM\Column(name="calor", type="string", length=2, nullable=true)
     */
    private $calor;

    /**
     * @var string
     *
     * @ORM\Column(name="p_electrica", type="string", length=2, nullable=true)
     */
    private $pElectrica;

    /**
     * @var \EvaPulparDx
     *
     * @ORM\ManyToOne(targetEntity="EvaPulparDx")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_eva_pul", referencedColumnName="id_eva_pul")
     * })
     */
    private $idEvaPul;



    /**
     * Get idPrueba
     *
     * @return integer 
     */
    public function getIdPrueba()
    {
        return $this->idPrueba;
    }

    /**
     * Set frio
     *
     * @param string $frio
     * @return PruebasTeDx
     */
    public function setFrio($frio)
    {
        $this->frio = $frio;

        return $this;
    }

    /**
     * Get frio
     *
     * @return string 
     */
    public function getFrio()
    {
        return $this->frio;
    }

    /**
     * Set calor
     *
     * @param string $calor
     * @return PruebasTeDx
     */
    public function setCalor($calor)
    {
        $this->calor = $calor;

        return $this;
    }

    /**
     * Get calor
     *
     * @return string 
     */
    public function getCalor()
    {
        return $this->calor;
    }

    /**
     * Set pElectrica
     *
     * @param string $pElectrica
     * @return PruebasTeDx
     */
    public function setPElectrica($pElectrica)
    {
        $this->pElectrica = $pElectrica;

        return $this;
    }

    /**
     * Get pElectrica
     *
     * @return string 
     */
    public function getPElectrica()
    {
        return $this->pElectrica;
    }

    /**
     * Set idEvaPul
     *
     * @param \foues\FPBundle\Entity\EvaPulparDx $idEvaPul
     * @return PruebasTeDx
     */
    public function setIdEvaPul(\foues\FPBundle\Entity\EvaPulparDx $idEvaPul = null)
    {
        $this->idEvaPul = $idEvaPul;

        return $this;
    }

    /**
     * Get idEvaPul
     *
     * @return \foues\FPBundle\Entity\EvaPulparDx 
     */
    public function getIdEvaPul()
    {
        return $this->idEvaPul;
    }
}
