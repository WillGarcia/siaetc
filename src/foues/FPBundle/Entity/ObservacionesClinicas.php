<?php

namespace foues\FPBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ObservacionesClinicas
 *
 * @ORM\Table(name="observaciones_clinicas", uniqueConstraints={@ORM\UniqueConstraint(name="observaciones_clinicas_pk", columns={"id_obs"})}, indexes={@ORM\Index(name="fk_observac_tiene_eva_pulp_fk", columns={"id_eva_pul"})})
 * @ORM\Entity
 */
class ObservacionesClinicas
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_obs", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="observaciones_clinicas_id_obs_seq", allocationSize=1, initialValue=1)
     */
    private $idObs;

    /**
     * @var string
     *
     * @ORM\Column(name="caries", type="string", length=2, nullable=true)
     */
    private $caries;

    /**
     * @var string
     *
     * @ORM\Column(name="restauracion", type="string", length=2, nullable=true)
     */
    private $restauracion;

    /**
     * @var string
     *
     * @ORM\Column(name="frac_dentaria", type="string", length=2, nullable=true)
     */
    private $fracDentaria;

    /**
     * @var string
     *
     * @ORM\Column(name="pul_expuesta", type="string", length=2, nullable=true)
     */
    private $pulExpuesta;

    /**
     * @var string
     *
     * @ORM\Column(name="decoloracion", type="string", length=2, nullable=true)
     */
    private $decoloracion;

    /**
     * @var string
     *
     * @ORM\Column(name="fistula", type="string", length=2, nullable=true)
     */
    private $fistula;

    /**
     * @var string
     *
     * @ORM\Column(name="inflamacion", type="string", length=2, nullable=true)
     */
    private $inflamacion;

    /**
     * @var string
     *
     * @ORM\Column(name="bolsa_perio", type="string", length=2, nullable=true)
     */
    private $bolsaPerio;

    /**
     * @var string
     *
     * @ORM\Column(name="mov_grado", type="string", length=2, nullable=true)
     */
    private $movGrado;

    /**
     * @var string
     *
     * @ORM\Column(name="mal_pos_dent", type="string", length=2, nullable=true)
     */
    private $malPosDent;

    /**
     * @var \EvaPulparDx
     *
     * @ORM\ManyToOne(targetEntity="EvaPulparDx")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_eva_pul", referencedColumnName="id_eva_pul")
     * })
     */
    private $idEvaPul;



    /**
     * Get idObs
     *
     * @return integer 
     */
    public function getIdObs()
    {
        return $this->idObs;
    }

    /**
     * Set caries
     *
     * @param string $caries
     * @return ObservacionesClinicas
     */
    public function setCaries($caries)
    {
        $this->caries = $caries;

        return $this;
    }

    /**
     * Get caries
     *
     * @return string 
     */
    public function getCaries()
    {
        return $this->caries;
    }

    /**
     * Set restauracion
     *
     * @param string $restauracion
     * @return ObservacionesClinicas
     */
    public function setRestauracion($restauracion)
    {
        $this->restauracion = $restauracion;

        return $this;
    }

    /**
     * Get restauracion
     *
     * @return string 
     */
    public function getRestauracion()
    {
        return $this->restauracion;
    }

    /**
     * Set fracDentaria
     *
     * @param string $fracDentaria
     * @return ObservacionesClinicas
     */
    public function setFracDentaria($fracDentaria)
    {
        $this->fracDentaria = $fracDentaria;

        return $this;
    }

    /**
     * Get fracDentaria
     *
     * @return string 
     */
    public function getFracDentaria()
    {
        return $this->fracDentaria;
    }

    /**
     * Set pulExpuesta
     *
     * @param string $pulExpuesta
     * @return ObservacionesClinicas
     */
    public function setPulExpuesta($pulExpuesta)
    {
        $this->pulExpuesta = $pulExpuesta;

        return $this;
    }

    /**
     * Get pulExpuesta
     *
     * @return string 
     */
    public function getPulExpuesta()
    {
        return $this->pulExpuesta;
    }

    /**
     * Set decoloracion
     *
     * @param string $decoloracion
     * @return ObservacionesClinicas
     */
    public function setDecoloracion($decoloracion)
    {
        $this->decoloracion = $decoloracion;

        return $this;
    }

    /**
     * Get decoloracion
     *
     * @return string 
     */
    public function getDecoloracion()
    {
        return $this->decoloracion;
    }

    /**
     * Set fistula
     *
     * @param string $fistula
     * @return ObservacionesClinicas
     */
    public function setFistula($fistula)
    {
        $this->fistula = $fistula;

        return $this;
    }

    /**
     * Get fistula
     *
     * @return string 
     */
    public function getFistula()
    {
        return $this->fistula;
    }

    /**
     * Set inflamacion
     *
     * @param string $inflamacion
     * @return ObservacionesClinicas
     */
    public function setInflamacion($inflamacion)
    {
        $this->inflamacion = $inflamacion;

        return $this;
    }

    /**
     * Get inflamacion
     *
     * @return string 
     */
    public function getInflamacion()
    {
        return $this->inflamacion;
    }

    /**
     * Set bolsaPerio
     *
     * @param string $bolsaPerio
     * @return ObservacionesClinicas
     */
    public function setBolsaPerio($bolsaPerio)
    {
        $this->bolsaPerio = $bolsaPerio;

        return $this;
    }

    /**
     * Get bolsaPerio
     *
     * @return string 
     */
    public function getBolsaPerio()
    {
        return $this->bolsaPerio;
    }

    /**
     * Set movGrado
     *
     * @param string $movGrado
     * @return ObservacionesClinicas
     */
    public function setMovGrado($movGrado)
    {
        $this->movGrado = $movGrado;

        return $this;
    }

    /**
     * Get movGrado
     *
     * @return string 
     */
    public function getMovGrado()
    {
        return $this->movGrado;
    }

    /**
     * Set malPosDent
     *
     * @param string $malPosDent
     * @return ObservacionesClinicas
     */
    public function setMalPosDent($malPosDent)
    {
        $this->malPosDent = $malPosDent;

        return $this;
    }

    /**
     * Get malPosDent
     *
     * @return string 
     */
    public function getMalPosDent()
    {
        return $this->malPosDent;
    }

    /**
     * Set idEvaPul
     *
     * @param \foues\FPBundle\Entity\EvaPulparDx $idEvaPul
     * @return ObservacionesClinicas
     */
    public function setIdEvaPul(\foues\FPBundle\Entity\EvaPulparDx $idEvaPul = null)
    {
        $this->idEvaPul = $idEvaPul;

        return $this;
    }

    /**
     * Get idEvaPul
     *
     * @return \foues\FPBundle\Entity\EvaPulparDx 
     */
    public function getIdEvaPul()
    {
        return $this->idEvaPul;
    }
}
