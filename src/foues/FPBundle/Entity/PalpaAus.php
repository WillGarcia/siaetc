<?php

namespace foues\FPBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PalpaAus
 *
 * @ORM\Table(name="palpa_aus", uniqueConstraints={@ORM\UniqueConstraint(name="palpa_aus_pk", columns={"id_palpa_aus"})}, indexes={@ORM\Index(name="relationship_92_fk", columns={"id_f_dx"})})
 * @ORM\Entity
 */
class PalpaAus
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_palpa_aus", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="palpa_aus_id_palpa_aus_seq", allocationSize=1, initialValue=1)
     */
    private $idPalpaAus;

    /**
     * @var boolean
     *
     * @ORM\Column(name="dol_lizq", type="boolean", nullable=true)
     */
    private $dolLizq;

    /**
     * @var boolean
     *
     * @ORM\Column(name="dol_pr_aizq", type="boolean", nullable=true)
     */
    private $dolPrAizq;

    /**
     * @var boolean
     *
     * @ORM\Column(name="dol_ain_izq", type="boolean", nullable=true)
     */
    private $dolAinIzq;

    /**
     * @var boolean
     *
     * @ORM\Column(name="clic_ar_izq", type="boolean", nullable=true)
     */
    private $clicArIzq;

    /**
     * @var boolean
     *
     * @ORM\Column(name="pop_ar_izq", type="boolean", nullable=true)
     */
    private $popArIzq;

    /**
     * @var boolean
     *
     * @ORM\Column(name="crep_ar_izq", type="boolean", nullable=true)
     */
    private $crepArIzq;

    /**
     * @var boolean
     *
     * @ORM\Column(name="dol_rar_izq", type="boolean", nullable=true)
     */
    private $dolRarIzq;

    /**
     * @var boolean
     *
     * @ORM\Column(name="dol_lder", type="boolean", nullable=true)
     */
    private $dolLder;

    /**
     * @var boolean
     *
     * @ORM\Column(name="dol_pr_aider", type="boolean", nullable=true)
     */
    private $dolPrAider;

    /**
     * @var boolean
     *
     * @ORM\Column(name="dol_ain_der", type="boolean", nullable=true)
     */
    private $dolAinDer;

    /**
     * @var boolean
     *
     * @ORM\Column(name="clic_ar_der", type="boolean", nullable=true)
     */
    private $clicArDer;

    /**
     * @var boolean
     *
     * @ORM\Column(name="pop_ar_der", type="boolean", nullable=true)
     */
    private $popArDer;

    /**
     * @var boolean
     *
     * @ORM\Column(name="crep_ar_der", type="boolean", nullable=true)
     */
    private $crepArDer;

    /**
     * @var boolean
     *
     * @ORM\Column(name="dol_rar_der", type="boolean", nullable=true)
     */
    private $dolRarDer;

    /**
     * @var string
     *
     * @ORM\Column(name="ima_palp_aus", type="string", length=255, nullable=true)
     */
    private $imaPalpAus;

    /**
     * @var string
     *
     * @ORM\Column(name="descripcion", type="string", length=100, nullable=true)
     */
    private $descripcion;

    /**
     * @var \FDiagnostico
     *
     * @ORM\ManyToOne(targetEntity="FDiagnostico")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_f_dx", referencedColumnName="id_f_dx")
     * })
     */
    private $idFDx;



    /**
     * Get idPalpaAus
     *
     * @return integer 
     */
    public function getIdPalpaAus()
    {
        return $this->idPalpaAus;
    }

    /**
     * Set dolLizq
     *
     * @param boolean $dolLizq
     * @return PalpaAus
     */
    public function setDolLizq($dolLizq)
    {
        $this->dolLizq = $dolLizq;

        return $this;
    }

    /**
     * Get dolLizq
     *
     * @return boolean 
     */
    public function getDolLizq()
    {
        return $this->dolLizq;
    }

    /**
     * Set dolPrAizq
     *
     * @param boolean $dolPrAizq
     * @return PalpaAus
     */
    public function setDolPrAizq($dolPrAizq)
    {
        $this->dolPrAizq = $dolPrAizq;

        return $this;
    }

    /**
     * Get dolPrAizq
     *
     * @return boolean 
     */
    public function getDolPrAizq()
    {
        return $this->dolPrAizq;
    }

    /**
     * Set dolAinIzq
     *
     * @param boolean $dolAinIzq
     * @return PalpaAus
     */
    public function setDolAinIzq($dolAinIzq)
    {
        $this->dolAinIzq = $dolAinIzq;

        return $this;
    }

    /**
     * Get dolAinIzq
     *
     * @return boolean 
     */
    public function getDolAinIzq()
    {
        return $this->dolAinIzq;
    }

    /**
     * Set clicArIzq
     *
     * @param boolean $clicArIzq
     * @return PalpaAus
     */
    public function setClicArIzq($clicArIzq)
    {
        $this->clicArIzq = $clicArIzq;

        return $this;
    }

    /**
     * Get clicArIzq
     *
     * @return boolean 
     */
    public function getClicArIzq()
    {
        return $this->clicArIzq;
    }

    /**
     * Set popArIzq
     *
     * @param boolean $popArIzq
     * @return PalpaAus
     */
    public function setPopArIzq($popArIzq)
    {
        $this->popArIzq = $popArIzq;

        return $this;
    }

    /**
     * Get popArIzq
     *
     * @return boolean 
     */
    public function getPopArIzq()
    {
        return $this->popArIzq;
    }

    /**
     * Set crepArIzq
     *
     * @param boolean $crepArIzq
     * @return PalpaAus
     */
    public function setCrepArIzq($crepArIzq)
    {
        $this->crepArIzq = $crepArIzq;

        return $this;
    }

    /**
     * Get crepArIzq
     *
     * @return boolean 
     */
    public function getCrepArIzq()
    {
        return $this->crepArIzq;
    }

    /**
     * Set dolRarIzq
     *
     * @param boolean $dolRarIzq
     * @return PalpaAus
     */
    public function setDolRarIzq($dolRarIzq)
    {
        $this->dolRarIzq = $dolRarIzq;

        return $this;
    }

    /**
     * Get dolRarIzq
     *
     * @return boolean 
     */
    public function getDolRarIzq()
    {
        return $this->dolRarIzq;
    }

    /**
     * Set dolLder
     *
     * @param boolean $dolLder
     * @return PalpaAus
     */
    public function setDolLder($dolLder)
    {
        $this->dolLder = $dolLder;

        return $this;
    }

    /**
     * Get dolLder
     *
     * @return boolean 
     */
    public function getDolLder()
    {
        return $this->dolLder;
    }

    /**
     * Set dolPrAider
     *
     * @param boolean $dolPrAider
     * @return PalpaAus
     */
    public function setDolPrAider($dolPrAider)
    {
        $this->dolPrAider = $dolPrAider;

        return $this;
    }

    /**
     * Get dolPrAider
     *
     * @return boolean 
     */
    public function getDolPrAider()
    {
        return $this->dolPrAider;
    }

    /**
     * Set dolAinDer
     *
     * @param boolean $dolAinDer
     * @return PalpaAus
     */
    public function setDolAinDer($dolAinDer)
    {
        $this->dolAinDer = $dolAinDer;

        return $this;
    }

    /**
     * Get dolAinDer
     *
     * @return boolean 
     */
    public function getDolAinDer()
    {
        return $this->dolAinDer;
    }

    /**
     * Set clicArDer
     *
     * @param boolean $clicArDer
     * @return PalpaAus
     */
    public function setClicArDer($clicArDer)
    {
        $this->clicArDer = $clicArDer;

        return $this;
    }

    /**
     * Get clicArDer
     *
     * @return boolean 
     */
    public function getClicArDer()
    {
        return $this->clicArDer;
    }

    /**
     * Set popArDer
     *
     * @param boolean $popArDer
     * @return PalpaAus
     */
    public function setPopArDer($popArDer)
    {
        $this->popArDer = $popArDer;

        return $this;
    }

    /**
     * Get popArDer
     *
     * @return boolean 
     */
    public function getPopArDer()
    {
        return $this->popArDer;
    }

    /**
     * Set crepArDer
     *
     * @param boolean $crepArDer
     * @return PalpaAus
     */
    public function setCrepArDer($crepArDer)
    {
        $this->crepArDer = $crepArDer;

        return $this;
    }

    /**
     * Get crepArDer
     *
     * @return boolean 
     */
    public function getCrepArDer()
    {
        return $this->crepArDer;
    }

    /**
     * Set dolRarDer
     *
     * @param boolean $dolRarDer
     * @return PalpaAus
     */
    public function setDolRarDer($dolRarDer)
    {
        $this->dolRarDer = $dolRarDer;

        return $this;
    }

    /**
     * Get dolRarDer
     *
     * @return boolean 
     */
    public function getDolRarDer()
    {
        return $this->dolRarDer;
    }

    /**
     * Set imaPalpAus
     *
     * @param string $imaPalpAus
     * @return PalpaAus
     */
    public function setImaPalpAus($imaPalpAus)
    {
        $this->imaPalpAus = $imaPalpAus;

        return $this;
    }

    /**
     * Get imaPalpAus
     *
     * @return string 
     */
    public function getImaPalpAus()
    {
        return $this->imaPalpAus;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     * @return PalpaAus
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string 
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set idFDx
     *
     * @param \foues\FPBundle\Entity\FDiagnostico $idFDx
     * @return PalpaAus
     */
    public function setIdFDx(\foues\FPBundle\Entity\FDiagnostico $idFDx = null)
    {
        $this->idFDx = $idFDx;

        return $this;
    }

    /**
     * Get idFDx
     *
     * @return \foues\FPBundle\Entity\FDiagnostico 
     */
    public function getIdFDx()
    {
        return $this->idFDx;
    }
}
