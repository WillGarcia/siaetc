<?php

namespace foues\FPBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * BitacoraSist
 *
 * @ORM\Table(name="bitacora_sist", uniqueConstraints={@ORM\UniqueConstraint(name="bitacora_sist_pk", columns={"id_bitacora"})}, indexes={@ORM\Index(name="fk_bitacora_controlan_usuario_f", columns={"id_usuario"})})
 * @ORM\Entity
 */
class BitacoraSist
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_bitacora", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="bitacora_sist_id_bitacora_seq", allocationSize=1, initialValue=1)
     */
    private $idBitacora;

    /**
     * @var string
     *
     * @ORM\Column(name="nom_accion", type="string", length=15, nullable=true)
     */
    private $nomAccion;

    /**
     * @var string
     *
     * @ORM\Column(name="nom_tabla", type="string", length=30, nullable=true)
     */
    private $nomTabla;

    /**
     * @var string
     *
     * @ORM\Column(name="nuevo_valor", type="text", nullable=true)
     */
    private $nuevoValor;

    /**
     * @var string
     *
     * @ORM\Column(name="antiguo_valor", type="text", nullable=true)
     */
    private $antiguoValor;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha_cambio", type="datetime", nullable=true)
     */
    private $fechaCambio;

    /**
     * @var string
     *
     * @ORM\Column(name="ultimo_acceso", type="string", length=25, nullable=false)
     */
    private $ultimoAcceso;

    /**
     * @var string
     *
     * @ORM\Column(name="request_uri", type="string", length=255, nullable=false)
     */
    private $requestUri;

    /**
     * @var string
     *
     * @ORM\Column(name="estado", type="string", length=15, nullable=false)
     */
    private $estado;

    /**
     * @var \Usuario
     *
     * @ORM\ManyToOne(targetEntity="Usuario")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_usuario", referencedColumnName="id_usuario")
     * })
     */
    private $idUsuario;



    /**
     * Get idBitacora
     *
     * @return integer 
     */
    public function getIdBitacora()
    {
        return $this->idBitacora;
    }

    /**
     * Set nomAccion
     *
     * @param string $nomAccion
     * @return BitacoraSist
     */
    public function setNomAccion($nomAccion)
    {
        $this->nomAccion = $nomAccion;

        return $this;
    }

    /**
     * Get nomAccion
     *
     * @return string 
     */
    public function getNomAccion()
    {
        return $this->nomAccion;
    }

    /**
     * Set nomTabla
     *
     * @param string $nomTabla
     * @return BitacoraSist
     */
    public function setNomTabla($nomTabla)
    {
        $this->nomTabla = $nomTabla;

        return $this;
    }

    /**
     * Get nomTabla
     *
     * @return string 
     */
    public function getNomTabla()
    {
        return $this->nomTabla;
    }

    /**
     * Set nuevoValor
     *
     * @param string $nuevoValor
     * @return BitacoraSist
     */
    public function setNuevoValor($nuevoValor)
    {
        $this->nuevoValor = $nuevoValor;

        return $this;
    }

    /**
     * Get nuevoValor
     *
     * @return string 
     */
    public function getNuevoValor()
    {
        return $this->nuevoValor;
    }

    /**
     * Set antiguoValor
     *
     * @param string $antiguoValor
     * @return BitacoraSist
     */
    public function setAntiguoValor($antiguoValor)
    {
        $this->antiguoValor = $antiguoValor;

        return $this;
    }

    /**
     * Get antiguoValor
     *
     * @return string 
     */
    public function getAntiguoValor()
    {
        return $this->antiguoValor;
    }

    /**
     * Set fechaCambio
     *
     * @param \DateTime $fechaCambio
     * @return BitacoraSist
     */
    public function setFechaCambio($fechaCambio)
    {
        $this->fechaCambio = $fechaCambio;

        return $this;
    }

    /**
     * Get fechaCambio
     *
     * @return \DateTime 
     */
    public function getFechaCambio()
    {
        return $this->fechaCambio;
    }

    /**
     * Set ultimoAcceso
     *
     * @param string $ultimoAcceso
     * @return BitacoraSist
     */
    public function setUltimoAcceso($ultimoAcceso)
    {
        $this->ultimoAcceso = $ultimoAcceso;

        return $this;
    }

    /**
     * Get ultimoAcceso
     *
     * @return string 
     */
    public function getUltimoAcceso()
    {
        return $this->ultimoAcceso;
    }

    /**
     * Set requestUri
     *
     * @param string $requestUri
     * @return BitacoraSist
     */
    public function setRequestUri($requestUri)
    {
        $this->requestUri = $requestUri;

        return $this;
    }

    /**
     * Get requestUri
     *
     * @return string 
     */
    public function getRequestUri()
    {
        return $this->requestUri;
    }

    /**
     * Set estado
     *
     * @param string $estado
     * @return BitacoraSist
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;

        return $this;
    }

    /**
     * Get estado
     *
     * @return string 
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * Set idUsuario
     *
     * @param \foues\FPBundle\Entity\Usuario $idUsuario
     * @return BitacoraSist
     */
    public function setIdUsuario(\foues\FPBundle\Entity\Usuario $idUsuario = null)
    {
        $this->idUsuario = $idUsuario;

        return $this;
    }

    /**
     * Get idUsuario
     *
     * @return \foues\FPBundle\Entity\Usuario 
     */
    public function getIdUsuario()
    {
        return $this->idUsuario;
    }
}
