<?php

namespace foues\FPBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Prestamo
 *
 * @ORM\Table(name="prestamo", uniqueConstraints={@ORM\UniqueConstraint(name="prestamo_pk", columns={"id_prestamo"})}, indexes={@ORM\Index(name="fk_prestamo_realiza_estudian_fk", columns={"due"}), @ORM\Index(name="IDX_F4D874F25A8BE498", columns={"num_expediente"})})
 * @ORM\Entity
 */
class Prestamo
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_prestamo", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="prestamo_id_prestamo_seq", allocationSize=1, initialValue=1)
     */
    private $idPrestamo;

    /**
     * @var string
     *
     * @ORM\Column(name="area_clinica", type="string", length=15, nullable=false)
     */
    private $areaClinica;

    /**
     * @var string
     *
     * @ORM\Column(name="ciclo", type="string", length=10, nullable=false)
     */
    private $ciclo;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha_prest", type="date", nullable=false)
     */
    private $fechaPrest;

    /**
     * @var \Estudiante
     *
     * @ORM\ManyToOne(targetEntity="Estudiante")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="due", referencedColumnName="due")
     * })
     */
    private $due;

    /**
     * @var \Expediente
     *
     * @ORM\ManyToOne(targetEntity="Expediente")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="num_expediente", referencedColumnName="num_expediente")
     * })
     */
    private $numExpediente;



    /**
     * Get idPrestamo
     *
     * @return integer 
     */
    public function getIdPrestamo()
    {
        return $this->idPrestamo;
    }

    /**
     * Set areaClinica
     *
     * @param string $areaClinica
     * @return Prestamo
     */
    public function setAreaClinica($areaClinica)
    {
        $this->areaClinica = $areaClinica;

        return $this;
    }

    /**
     * Get areaClinica
     *
     * @return string 
     */
    public function getAreaClinica()
    {
        return $this->areaClinica;
    }

    /**
     * Set ciclo
     *
     * @param string $ciclo
     * @return Prestamo
     */
    public function setCiclo($ciclo)
    {
        $this->ciclo = $ciclo;

        return $this;
    }

    /**
     * Get ciclo
     *
     * @return string 
     */
    public function getCiclo()
    {
        return $this->ciclo;
    }

    /**
     * Set fechaPrest
     *
     * @param \DateTime $fechaPrest
     * @return Prestamo
     */
    public function setFechaPrest($fechaPrest)
    {
        $this->fechaPrest = $fechaPrest;

        return $this;
    }

    /**
     * Get fechaPrest
     *
     * @return \DateTime 
     */
    public function getFechaPrest()
    {
        return $this->fechaPrest;
    }

    /**
     * Set due
     *
     * @param \foues\FPBundle\Entity\Estudiante $due
     * @return Prestamo
     */
    public function setDue(\foues\FPBundle\Entity\Estudiante $due = null)
    {
        $this->due = $due;

        return $this;
    }

    /**
     * Get due
     *
     * @return \foues\FPBundle\Entity\Estudiante 
     */
    public function getDue()
    {
        return $this->due;
    }

    /**
     * Set numExpediente
     *
     * @param \foues\FPBundle\Entity\Expediente $numExpediente
     * @return Prestamo
     */
    public function setNumExpediente(\foues\FPBundle\Entity\Expediente $numExpediente = null)
    {
        $this->numExpediente = $numExpediente;

        return $this;
    }

    /**
     * Get numExpediente
     *
     * @return \foues\FPBundle\Entity\Expediente 
     */
    public function getNumExpediente()
    {
        return $this->numExpediente;
    }
}
