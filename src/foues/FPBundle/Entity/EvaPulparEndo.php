<?php

namespace foues\FPBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * EvaPulparEndo
 *
 * @ORM\Table(name="eva_pulpar_endo", uniqueConstraints={@ORM\UniqueConstraint(name="eva_pulpar_endo_pk", columns={"id_eva"})}, indexes={@ORM\Index(name="fk_eva_pulp_registra_f_endodo_f", columns={"id_f_endo"})})
 * @ORM\Entity
 */
class EvaPulparEndo
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_eva", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="eva_pulpar_endo_id_eva_seq", allocationSize=1, initialValue=1)
     */
    private $idEva;

    /**
     * @var string
     *
     * @ORM\Column(name="diente_evaluar", type="string", length=4, nullable=true)
     */
    private $dienteEvaluar;

    /**
     * @var string
     *
     * @ORM\Column(name="otro_hallazgoep", type="string", length=100, nullable=true)
     */
    private $otroHallazgoep;

    /**
     * @var \FEndodoncia
     *
     * @ORM\ManyToOne(targetEntity="FEndodoncia")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_f_endo", referencedColumnName="id_f_endo")
     * })
     */
    private $idFEndo;



    /**
     * Get idEva
     *
     * @return integer 
     */
    public function getIdEva()
    {
        return $this->idEva;
    }

    /**
     * Set dienteEvaluar
     *
     * @param string $dienteEvaluar
     * @return EvaPulparEndo
     */
    public function setDienteEvaluar($dienteEvaluar)
    {
        $this->dienteEvaluar = $dienteEvaluar;

        return $this;
    }

    /**
     * Get dienteEvaluar
     *
     * @return string 
     */
    public function getDienteEvaluar()
    {
        return $this->dienteEvaluar;
    }

    /**
     * Set otroHallazgoep
     *
     * @param string $otroHallazgoep
     * @return EvaPulparEndo
     */
    public function setOtroHallazgoep($otroHallazgoep)
    {
        $this->otroHallazgoep = $otroHallazgoep;

        return $this;
    }

    /**
     * Get otroHallazgoep
     *
     * @return string 
     */
    public function getOtroHallazgoep()
    {
        return $this->otroHallazgoep;
    }

    /**
     * Set idFEndo
     *
     * @param \foues\FPBundle\Entity\FEndodoncia $idFEndo
     * @return EvaPulparEndo
     */
    public function setIdFEndo(\foues\FPBundle\Entity\FEndodoncia $idFEndo = null)
    {
        $this->idFEndo = $idFEndo;

        return $this;
    }

    /**
     * Get idFEndo
     *
     * @return \foues\FPBundle\Entity\FEndodoncia 
     */
    public function getIdFEndo()
    {
        return $this->idFEndo;
    }
}
