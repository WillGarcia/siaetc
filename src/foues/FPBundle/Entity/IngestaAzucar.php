<?php

namespace foues\FPBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * IngestaAzucar
 *
 * @ORM\Table(name="ingesta_azucar", uniqueConstraints={@ORM\UniqueConstraint(name="ingesta_azucar_pk", columns={"id_ia"})}, indexes={@ORM\Index(name="fk_ingesta_azucar_f_dx_fk", columns={"id_f_dx"})})
 * @ORM\Entity
 */
class IngestaAzucar
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_ia", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="ingesta_azucar_id_ia_seq", allocationSize=1, initialValue=1)
     */
    private $idIa;

    /**
     * @var boolean
     *
     * @ORM\Column(name="desayuno", type="boolean", nullable=true)
     */
    private $desayuno;

    /**
     * @var boolean
     *
     * @ORM\Column(name="des_alm_8a10", type="boolean", nullable=false)
     */
    private $desAlm8a10;

    /**
     * @var boolean
     *
     * @ORM\Column(name="des_alm_10a12", type="boolean", nullable=false)
     */
    private $desAlm10a12;

    /**
     * @var boolean
     *
     * @ORM\Column(name="almuerzo", type="boolean", nullable=false)
     */
    private $almuerzo;

    /**
     * @var boolean
     *
     * @ORM\Column(name="alm_cen_2a6", type="boolean", nullable=false)
     */
    private $almCen2a6;

    /**
     * @var boolean
     *
     * @ORM\Column(name="cena", type="boolean", nullable=false)
     */
    private $cena;

    /**
     * @var boolean
     *
     * @ORM\Column(name="des_cena", type="boolean", nullable=false)
     */
    private $desCena;

    /**
     * @var boolean
     *
     * @ORM\Column(name="si_despierta", type="boolean", nullable=false)
     */
    private $siDespierta;

    /**
     * @var boolean
     *
     * @ORM\Column(name="jarabes_noche", type="boolean", nullable=false)
     */
    private $jarabesNoche;

    /**
     * @var integer
     *
     * @ORM\Column(name="total_si", type="integer", nullable=false)
     */
    private $totalSi;

    /**
     * @var integer
     *
     * @ORM\Column(name="total_no", type="integer", nullable=false)
     */
    private $totalNo;

    /**
     * @var \FDiagnostico
     *
     * @ORM\ManyToOne(targetEntity="FDiagnostico")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_f_dx", referencedColumnName="id_f_dx")
     * })
     */
    private $idFDx;



    /**
     * Get idIa
     *
     * @return integer 
     */
    public function getIdIa()
    {
        return $this->idIa;
    }

    /**
     * Set desayuno
     *
     * @param boolean $desayuno
     * @return IngestaAzucar
     */
    public function setDesayuno($desayuno)
    {
        $this->desayuno = $desayuno;

        return $this;
    }

    /**
     * Get desayuno
     *
     * @return boolean 
     */
    public function getDesayuno()
    {
        return $this->desayuno;
    }

    /**
     * Set desAlm8a10
     *
     * @param boolean $desAlm8a10
     * @return IngestaAzucar
     */
    public function setDesAlm8a10($desAlm8a10)
    {
        $this->desAlm8a10 = $desAlm8a10;

        return $this;
    }

    /**
     * Get desAlm8a10
     *
     * @return boolean 
     */
    public function getDesAlm8a10()
    {
        return $this->desAlm8a10;
    }

    /**
     * Set desAlm10a12
     *
     * @param boolean $desAlm10a12
     * @return IngestaAzucar
     */
    public function setDesAlm10a12($desAlm10a12)
    {
        $this->desAlm10a12 = $desAlm10a12;

        return $this;
    }

    /**
     * Get desAlm10a12
     *
     * @return boolean 
     */
    public function getDesAlm10a12()
    {
        return $this->desAlm10a12;
    }

    /**
     * Set almuerzo
     *
     * @param boolean $almuerzo
     * @return IngestaAzucar
     */
    public function setAlmuerzo($almuerzo)
    {
        $this->almuerzo = $almuerzo;

        return $this;
    }

    /**
     * Get almuerzo
     *
     * @return boolean 
     */
    public function getAlmuerzo()
    {
        return $this->almuerzo;
    }

    /**
     * Set almCen2a6
     *
     * @param boolean $almCen2a6
     * @return IngestaAzucar
     */
    public function setAlmCen2a6($almCen2a6)
    {
        $this->almCen2a6 = $almCen2a6;

        return $this;
    }

    /**
     * Get almCen2a6
     *
     * @return boolean 
     */
    public function getAlmCen2a6()
    {
        return $this->almCen2a6;
    }

    /**
     * Set cena
     *
     * @param boolean $cena
     * @return IngestaAzucar
     */
    public function setCena($cena)
    {
        $this->cena = $cena;

        return $this;
    }

    /**
     * Get cena
     *
     * @return boolean 
     */
    public function getCena()
    {
        return $this->cena;
    }

    /**
     * Set desCena
     *
     * @param boolean $desCena
     * @return IngestaAzucar
     */
    public function setDesCena($desCena)
    {
        $this->desCena = $desCena;

        return $this;
    }

    /**
     * Get desCena
     *
     * @return boolean 
     */
    public function getDesCena()
    {
        return $this->desCena;
    }

    /**
     * Set siDespierta
     *
     * @param boolean $siDespierta
     * @return IngestaAzucar
     */
    public function setSiDespierta($siDespierta)
    {
        $this->siDespierta = $siDespierta;

        return $this;
    }

    /**
     * Get siDespierta
     *
     * @return boolean 
     */
    public function getSiDespierta()
    {
        return $this->siDespierta;
    }

    /**
     * Set jarabesNoche
     *
     * @param boolean $jarabesNoche
     * @return IngestaAzucar
     */
    public function setJarabesNoche($jarabesNoche)
    {
        $this->jarabesNoche = $jarabesNoche;

        return $this;
    }

    /**
     * Get jarabesNoche
     *
     * @return boolean 
     */
    public function getJarabesNoche()
    {
        return $this->jarabesNoche;
    }

    /**
     * Set totalSi
     *
     * @param integer $totalSi
     * @return IngestaAzucar
     */
    public function setTotalSi($totalSi)
    {
        $this->totalSi = $totalSi;

        return $this;
    }

    /**
     * Get totalSi
     *
     * @return integer 
     */
    public function getTotalSi()
    {
        return $this->totalSi;
    }

    /**
     * Set totalNo
     *
     * @param integer $totalNo
     * @return IngestaAzucar
     */
    public function setTotalNo($totalNo)
    {
        $this->totalNo = $totalNo;

        return $this;
    }

    /**
     * Get totalNo
     *
     * @return integer 
     */
    public function getTotalNo()
    {
        return $this->totalNo;
    }

    /**
     * Set idFDx
     *
     * @param \foues\FPBundle\Entity\FDiagnostico $idFDx
     * @return IngestaAzucar
     */
    public function setIdFDx(\foues\FPBundle\Entity\FDiagnostico $idFDx = null)
    {
        $this->idFDx = $idFDx;

        return $this;
    }

    /**
     * Get idFDx
     *
     * @return \foues\FPBundle\Entity\FDiagnostico 
     */
    public function getIdFDx()
    {
        return $this->idFDx;
    }
}
