<?php

namespace foues\FPBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Expediente
 *
 * @ORM\Table(name="expediente", uniqueConstraints={@ORM\UniqueConstraint(name="expediente_pk", columns={"num_expediente"})}, indexes={@ORM\Index(name="fk_expedien_le_crean2_paciente_", columns={"id_pac"})})
 * @ORM\Entity
 */
class Expediente
{
    /**
     * @var string
     *
     * @ORM\Column(name="num_expediente", type="string", length=10, nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="expediente_num_expediente_seq", allocationSize=1, initialValue=1)
     */
    private $numExpediente;

    /**
     * @var boolean
     *
     * @ORM\Column(name="disponible", type="boolean", nullable=false)
     */
    private $disponible;

    /**
     * @var \Paciente
     *
     * @ORM\ManyToOne(targetEntity="Paciente")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_pac", referencedColumnName="id_pac")
     * })
     */
    private $idPac;



    /**
     * Get numExpediente
     *
     * @return string 
     */
    public function getNumExpediente()
    {
        return $this->numExpediente;
    }

    /**
     * Set disponible
     *
     * @param boolean $disponible
     * @return Expediente
     */
    public function setDisponible($disponible)
    {
        $this->disponible = $disponible;

        return $this;
    }

    /**
     * Get disponible
     *
     * @return boolean 
     */
    public function getDisponible()
    {
        return $this->disponible;
    }

    /**
     * Set idPac
     *
     * @param \foues\FPBundle\Entity\Paciente $idPac
     * @return Expediente
     */
    public function setIdPac(\foues\FPBundle\Entity\Paciente $idPac = null)
    {
        $this->idPac = $idPac;

        return $this;
    }

    /**
     * Get idPac
     *
     * @return \foues\FPBundle\Entity\Paciente 
     */
    public function getIdPac()
    {
        return $this->idPac;
    }

    /*public function __toString(){
        return $this->getNumExpediente();
    }*/

}
