<?php

namespace foues\FPBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PruebasTeEndo
 *
 * @ORM\Table(name="pruebas_te_endo", uniqueConstraints={@ORM\UniqueConstraint(name="pruebas_te_endo_pk", columns={"id_prueba"})}, indexes={@ORM\Index(name="fk_pruebas__necesita3_eva_pulp_", columns={"id_eva"})})
 * @ORM\Entity
 */
class PruebasTeEndo
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_prueba", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="pruebas_te_endo_id_prueba_seq", allocationSize=1, initialValue=1)
     */
    private $idPrueba;

    /**
     * @var string
     *
     * @ORM\Column(name="frio", type="string", length=2, nullable=true)
     */
    private $frio;

    /**
     * @var string
     *
     * @ORM\Column(name="dc_f_1", type="string", length=2, nullable=true)
     */
    private $dcF1;

    /**
     * @var string
     *
     * @ORM\Column(name="dc_f_2", type="string", length=2, nullable=true)
     */
    private $dcF2;

    /**
     * @var string
     *
     * @ORM\Column(name="dc_f_3", type="string", length=2, nullable=true)
     */
    private $dcF3;

    /**
     * @var string
     *
     * @ORM\Column(name="dc_f_4", type="string", length=2, nullable=true)
     */
    private $dcF4;

    /**
     * @var string
     *
     * @ORM\Column(name="calor", type="string", length=2, nullable=true)
     */
    private $calor;

    /**
     * @var string
     *
     * @ORM\Column(name="dc_c_1", type="string", length=2, nullable=true)
     */
    private $dcC1;

    /**
     * @var string
     *
     * @ORM\Column(name="dc_c_2", type="string", length=2, nullable=true)
     */
    private $dcC2;

    /**
     * @var string
     *
     * @ORM\Column(name="dc_c_3", type="string", length=2, nullable=true)
     */
    private $dcC3;

    /**
     * @var string
     *
     * @ORM\Column(name="dc_c_4", type="string", length=2, nullable=true)
     */
    private $dcC4;

    /**
     * @var string
     *
     * @ORM\Column(name="p_electricas", type="string", length=2, nullable=true)
     */
    private $pElectricas;

    /**
     * @var string
     *
     * @ORM\Column(name="dc_pe_1", type="string", length=2, nullable=true)
     */
    private $dcPe1;

    /**
     * @var string
     *
     * @ORM\Column(name="dc_pe_2", type="string", length=2, nullable=true)
     */
    private $dcPe2;

    /**
     * @var string
     *
     * @ORM\Column(name="dc_pe_3", type="string", length=2, nullable=true)
     */
    private $dcPe3;

    /**
     * @var string
     *
     * @ORM\Column(name="dc_pe_4", type="string", length=2, nullable=true)
     */
    private $dcPe4;

    /**
     * @var \EvaPulparEndo
     *
     * @ORM\ManyToOne(targetEntity="EvaPulparEndo")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_eva", referencedColumnName="id_eva")
     * })
     */
    private $idEva;



    /**
     * Get idPrueba
     *
     * @return integer 
     */
    public function getIdPrueba()
    {
        return $this->idPrueba;
    }

    /**
     * Set frio
     *
     * @param string $frio
     * @return PruebasTeEndo
     */
    public function setFrio($frio)
    {
        $this->frio = $frio;

        return $this;
    }

    /**
     * Get frio
     *
     * @return string 
     */
    public function getFrio()
    {
        return $this->frio;
    }

    /**
     * Set dcF1
     *
     * @param string $dcF1
     * @return PruebasTeEndo
     */
    public function setDcF1($dcF1)
    {
        $this->dcF1 = $dcF1;

        return $this;
    }

    /**
     * Get dcF1
     *
     * @return string 
     */
    public function getDcF1()
    {
        return $this->dcF1;
    }

    /**
     * Set dcF2
     *
     * @param string $dcF2
     * @return PruebasTeEndo
     */
    public function setDcF2($dcF2)
    {
        $this->dcF2 = $dcF2;

        return $this;
    }

    /**
     * Get dcF2
     *
     * @return string 
     */
    public function getDcF2()
    {
        return $this->dcF2;
    }

    /**
     * Set dcF3
     *
     * @param string $dcF3
     * @return PruebasTeEndo
     */
    public function setDcF3($dcF3)
    {
        $this->dcF3 = $dcF3;

        return $this;
    }

    /**
     * Get dcF3
     *
     * @return string 
     */
    public function getDcF3()
    {
        return $this->dcF3;
    }

    /**
     * Set dcF4
     *
     * @param string $dcF4
     * @return PruebasTeEndo
     */
    public function setDcF4($dcF4)
    {
        $this->dcF4 = $dcF4;

        return $this;
    }

    /**
     * Get dcF4
     *
     * @return string 
     */
    public function getDcF4()
    {
        return $this->dcF4;
    }

    /**
     * Set calor
     *
     * @param string $calor
     * @return PruebasTeEndo
     */
    public function setCalor($calor)
    {
        $this->calor = $calor;

        return $this;
    }

    /**
     * Get calor
     *
     * @return string 
     */
    public function getCalor()
    {
        return $this->calor;
    }

    /**
     * Set dcC1
     *
     * @param string $dcC1
     * @return PruebasTeEndo
     */
    public function setDcC1($dcC1)
    {
        $this->dcC1 = $dcC1;

        return $this;
    }

    /**
     * Get dcC1
     *
     * @return string 
     */
    public function getDcC1()
    {
        return $this->dcC1;
    }

    /**
     * Set dcC2
     *
     * @param string $dcC2
     * @return PruebasTeEndo
     */
    public function setDcC2($dcC2)
    {
        $this->dcC2 = $dcC2;

        return $this;
    }

    /**
     * Get dcC2
     *
     * @return string 
     */
    public function getDcC2()
    {
        return $this->dcC2;
    }

    /**
     * Set dcC3
     *
     * @param string $dcC3
     * @return PruebasTeEndo
     */
    public function setDcC3($dcC3)
    {
        $this->dcC3 = $dcC3;

        return $this;
    }

    /**
     * Get dcC3
     *
     * @return string 
     */
    public function getDcC3()
    {
        return $this->dcC3;
    }

    /**
     * Set dcC4
     *
     * @param string $dcC4
     * @return PruebasTeEndo
     */
    public function setDcC4($dcC4)
    {
        $this->dcC4 = $dcC4;

        return $this;
    }

    /**
     * Get dcC4
     *
     * @return string 
     */
    public function getDcC4()
    {
        return $this->dcC4;
    }

    /**
     * Set pElectricas
     *
     * @param string $pElectricas
     * @return PruebasTeEndo
     */
    public function setPElectricas($pElectricas)
    {
        $this->pElectricas = $pElectricas;

        return $this;
    }

    /**
     * Get pElectricas
     *
     * @return string 
     */
    public function getPElectricas()
    {
        return $this->pElectricas;
    }

    /**
     * Set dcPe1
     *
     * @param string $dcPe1
     * @return PruebasTeEndo
     */
    public function setDcPe1($dcPe1)
    {
        $this->dcPe1 = $dcPe1;

        return $this;
    }

    /**
     * Get dcPe1
     *
     * @return string 
     */
    public function getDcPe1()
    {
        return $this->dcPe1;
    }

    /**
     * Set dcPe2
     *
     * @param string $dcPe2
     * @return PruebasTeEndo
     */
    public function setDcPe2($dcPe2)
    {
        $this->dcPe2 = $dcPe2;

        return $this;
    }

    /**
     * Get dcPe2
     *
     * @return string 
     */
    public function getDcPe2()
    {
        return $this->dcPe2;
    }

    /**
     * Set dcPe3
     *
     * @param string $dcPe3
     * @return PruebasTeEndo
     */
    public function setDcPe3($dcPe3)
    {
        $this->dcPe3 = $dcPe3;

        return $this;
    }

    /**
     * Get dcPe3
     *
     * @return string 
     */
    public function getDcPe3()
    {
        return $this->dcPe3;
    }

    /**
     * Set dcPe4
     *
     * @param string $dcPe4
     * @return PruebasTeEndo
     */
    public function setDcPe4($dcPe4)
    {
        $this->dcPe4 = $dcPe4;

        return $this;
    }

    /**
     * Get dcPe4
     *
     * @return string 
     */
    public function getDcPe4()
    {
        return $this->dcPe4;
    }

    /**
     * Set idEva
     *
     * @param \foues\FPBundle\Entity\EvaPulparEndo $idEva
     * @return PruebasTeEndo
     */
    public function setIdEva(\foues\FPBundle\Entity\EvaPulparEndo $idEva = null)
    {
        $this->idEva = $idEva;

        return $this;
    }

    /**
     * Get idEva
     *
     * @return \foues\FPBundle\Entity\EvaPulparEndo 
     */
    public function getIdEva()
    {
        return $this->idEva;
    }
}
