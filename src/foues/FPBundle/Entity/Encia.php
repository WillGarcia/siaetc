<?php

namespace foues\FPBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Encia
 *
 * @ORM\Table(name="encia", uniqueConstraints={@ORM\UniqueConstraint(name="encia_pk", columns={"id_descripcion"})}, indexes={@ORM\Index(name="relationship_97_fk", columns={"id_f_perio"})})
 * @ORM\Entity
 */
class Encia
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_descripcion", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="encia_id_descripcion_seq", allocationSize=1, initialValue=1)
     */
    private $idDescripcion;

    /**
     * @var string
     *
     * @ORM\Column(name="color", type="string", length=20, nullable=true)
     */
    private $color;

    /**
     * @var string
     *
     * @ORM\Column(name="alt_contorno", type="string", length=50, nullable=true)
     */
    private $altContorno;

    /**
     * @var string
     *
     * @ORM\Column(name="textura", type="string", length=50, nullable=true)
     */
    private $textura;

    /**
     * @var string
     *
     * @ORM\Column(name="consistencia", type="string", length=20, nullable=true)
     */
    private $consistencia;

    /**
     * @var \FPeriodoncia
     *
     * @ORM\ManyToOne(targetEntity="FPeriodoncia")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_f_perio", referencedColumnName="id_f_perio")
     * })
     */
    private $idFPerio;



    /**
     * Get idDescripcion
     *
     * @return integer 
     */
    public function getIdDescripcion()
    {
        return $this->idDescripcion;
    }

    /**
     * Set color
     *
     * @param string $color
     * @return Encia
     */
    public function setColor($color)
    {
        $this->color = $color;

        return $this;
    }

    /**
     * Get color
     *
     * @return string 
     */
    public function getColor()
    {
        return $this->color;
    }

    /**
     * Set altContorno
     *
     * @param string $altContorno
     * @return Encia
     */
    public function setAltContorno($altContorno)
    {
        $this->altContorno = $altContorno;

        return $this;
    }

    /**
     * Get altContorno
     *
     * @return string 
     */
    public function getAltContorno()
    {
        return $this->altContorno;
    }

    /**
     * Set textura
     *
     * @param string $textura
     * @return Encia
     */
    public function setTextura($textura)
    {
        $this->textura = $textura;

        return $this;
    }

    /**
     * Get textura
     *
     * @return string 
     */
    public function getTextura()
    {
        return $this->textura;
    }

    /**
     * Set consistencia
     *
     * @param string $consistencia
     * @return Encia
     */
    public function setConsistencia($consistencia)
    {
        $this->consistencia = $consistencia;

        return $this;
    }

    /**
     * Get consistencia
     *
     * @return string 
     */
    public function getConsistencia()
    {
        return $this->consistencia;
    }

    /**
     * Set idFPerio
     *
     * @param \foues\FPBundle\Entity\FPeriodoncia $idFPerio
     * @return Encia
     */
    public function setIdFPerio(\foues\FPBundle\Entity\FPeriodoncia $idFPerio = null)
    {
        $this->idFPerio = $idFPerio;

        return $this;
    }

    /**
     * Get idFPerio
     *
     * @return \foues\FPBundle\Entity\FPeriodoncia 
     */
    public function getIdFPerio()
    {
        return $this->idFPerio;
    }
}
