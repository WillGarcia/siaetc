<?php

namespace foues\FPBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * RConductometriaTbm
 *
 * @ORM\Table(name="r_conductometria_tbm", uniqueConstraints={@ORM\UniqueConstraint(name="r_conductometria_tbm_pk", columns={"id_rc_tbm"})}, indexes={@ORM\Index(name="fk_r_conduc_captura_f_endodo_fk", columns={"id_f_endo"}), @ORM\Index(name="relationship_109_fk", columns={"id_cat_t_endo"})})
 * @ORM\Entity
 */
class RConductometriaTbm
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_rc_tbm", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="r_conductometria_tbm_id_rc_tbm_seq", allocationSize=1, initialValue=1)
     */
    private $idRcTbm;

    /**
     * @var string
     *
     * @ORM\Column(name="num_diente", type="string", length=4, nullable=true)
     */
    private $numDiente;

    /**
     * @var string
     *
     * @ORM\Column(name="dx_pulpar", type="string", length=50, nullable=true)
     */
    private $dxPulpar;

    /**
     * @var string
     *
     * @ORM\Column(name="dx_periapical", type="string", length=50, nullable=true)
     */
    private $dxPeriapical;

    /**
     * @var string
     *
     * @ORM\Column(name="conducto", type="string", length=20, nullable=true)
     */
    private $conducto;

    /**
     * @var string
     *
     * @ORM\Column(name="p_referencia", type="string", length=20, nullable=true)
     */
    private $pReferencia;

    /**
     * @var string
     *
     * @ORM\Column(name="m_provisional", type="string", length=20, nullable=true)
     */
    private $mProvisional;

    /**
     * @var string
     *
     * @ORM\Column(name="m_trabajo", type="string", length=20, nullable=true)
     */
    private $mTrabajo;

    /**
     * @var string
     *
     * @ORM\Column(name="lima_memoria", type="string", length=20, nullable=true)
     */
    private $limaMemoria;

    /**
     * @var string
     *
     * @ORM\Column(name="lima_final", type="string", length=20, nullable=true)
     */
    private $limaFinal;

    /**
     * @var string
     *
     * @ORM\Column(name="observacion", type="string", length=100, nullable=true)
     */
    private $observacion;

    /**
     * @var \FEndodoncia
     *
     * @ORM\ManyToOne(targetEntity="FEndodoncia")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_f_endo", referencedColumnName="id_f_endo")
     * })
     */
    private $idFEndo;

    /**
     * @var \CatTrataEndo
     *
     * @ORM\ManyToOne(targetEntity="CatTrataEndo")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_cat_t_endo", referencedColumnName="id_cat_t_endo")
     * })
     */
    private $idCatTEndo;



    /**
     * Get idRcTbm
     *
     * @return integer 
     */
    public function getIdRcTbm()
    {
        return $this->idRcTbm;
    }

    /**
     * Set numDiente
     *
     * @param string $numDiente
     * @return RConductometriaTbm
     */
    public function setNumDiente($numDiente)
    {
        $this->numDiente = $numDiente;

        return $this;
    }

    /**
     * Get numDiente
     *
     * @return string 
     */
    public function getNumDiente()
    {
        return $this->numDiente;
    }

    /**
     * Set dxPulpar
     *
     * @param string $dxPulpar
     * @return RConductometriaTbm
     */
    public function setDxPulpar($dxPulpar)
    {
        $this->dxPulpar = $dxPulpar;

        return $this;
    }

    /**
     * Get dxPulpar
     *
     * @return string 
     */
    public function getDxPulpar()
    {
        return $this->dxPulpar;
    }

    /**
     * Set dxPeriapical
     *
     * @param string $dxPeriapical
     * @return RConductometriaTbm
     */
    public function setDxPeriapical($dxPeriapical)
    {
        $this->dxPeriapical = $dxPeriapical;

        return $this;
    }

    /**
     * Get dxPeriapical
     *
     * @return string 
     */
    public function getDxPeriapical()
    {
        return $this->dxPeriapical;
    }

    /**
     * Set conducto
     *
     * @param string $conducto
     * @return RConductometriaTbm
     */
    public function setConducto($conducto)
    {
        $this->conducto = $conducto;

        return $this;
    }

    /**
     * Get conducto
     *
     * @return string 
     */
    public function getConducto()
    {
        return $this->conducto;
    }

    /**
     * Set pReferencia
     *
     * @param string $pReferencia
     * @return RConductometriaTbm
     */
    public function setPReferencia($pReferencia)
    {
        $this->pReferencia = $pReferencia;

        return $this;
    }

    /**
     * Get pReferencia
     *
     * @return string 
     */
    public function getPReferencia()
    {
        return $this->pReferencia;
    }

    /**
     * Set mProvisional
     *
     * @param string $mProvisional
     * @return RConductometriaTbm
     */
    public function setMProvisional($mProvisional)
    {
        $this->mProvisional = $mProvisional;

        return $this;
    }

    /**
     * Get mProvisional
     *
     * @return string 
     */
    public function getMProvisional()
    {
        return $this->mProvisional;
    }

    /**
     * Set mTrabajo
     *
     * @param string $mTrabajo
     * @return RConductometriaTbm
     */
    public function setMTrabajo($mTrabajo)
    {
        $this->mTrabajo = $mTrabajo;

        return $this;
    }

    /**
     * Get mTrabajo
     *
     * @return string 
     */
    public function getMTrabajo()
    {
        return $this->mTrabajo;
    }

    /**
     * Set limaMemoria
     *
     * @param string $limaMemoria
     * @return RConductometriaTbm
     */
    public function setLimaMemoria($limaMemoria)
    {
        $this->limaMemoria = $limaMemoria;

        return $this;
    }

    /**
     * Get limaMemoria
     *
     * @return string 
     */
    public function getLimaMemoria()
    {
        return $this->limaMemoria;
    }

    /**
     * Set limaFinal
     *
     * @param string $limaFinal
     * @return RConductometriaTbm
     */
    public function setLimaFinal($limaFinal)
    {
        $this->limaFinal = $limaFinal;

        return $this;
    }

    /**
     * Get limaFinal
     *
     * @return string 
     */
    public function getLimaFinal()
    {
        return $this->limaFinal;
    }

    /**
     * Set observacion
     *
     * @param string $observacion
     * @return RConductometriaTbm
     */
    public function setObservacion($observacion)
    {
        $this->observacion = $observacion;

        return $this;
    }

    /**
     * Get observacion
     *
     * @return string 
     */
    public function getObservacion()
    {
        return $this->observacion;
    }

    /**
     * Set idFEndo
     *
     * @param \foues\FPBundle\Entity\FEndodoncia $idFEndo
     * @return RConductometriaTbm
     */
    public function setIdFEndo(\foues\FPBundle\Entity\FEndodoncia $idFEndo = null)
    {
        $this->idFEndo = $idFEndo;

        return $this;
    }

    /**
     * Get idFEndo
     *
     * @return \foues\FPBundle\Entity\FEndodoncia 
     */
    public function getIdFEndo()
    {
        return $this->idFEndo;
    }

    /**
     * Set idCatTEndo
     *
     * @param \foues\FPBundle\Entity\CatTrataEndo $idCatTEndo
     * @return RConductometriaTbm
     */
    public function setIdCatTEndo(\foues\FPBundle\Entity\CatTrataEndo $idCatTEndo = null)
    {
        $this->idCatTEndo = $idCatTEndo;

        return $this;
    }

    /**
     * Get idCatTEndo
     *
     * @return \foues\FPBundle\Entity\CatTrataEndo 
     */
    public function getIdCatTEndo()
    {
        return $this->idCatTEndo;
    }
}
