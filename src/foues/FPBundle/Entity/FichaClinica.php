<?php

namespace foues\FPBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * FichaClinica
 *
 * @ORM\Table(name="ficha_clinica", uniqueConstraints={@ORM\UniqueConstraint(name="ficha_clinica_pk", columns={"num_expediente"})})
 * @ORM\Entity
 */
class FichaClinica
{
    /**
     * @var integer
     *
     * @ORM\Column(name="num_ficha", type="integer", nullable=true)
     */
    private $numFicha;

    /**
     * @var \Expediente
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\OneToOne(targetEntity="Expediente")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="num_expediente", referencedColumnName="num_expediente")
     * })
     */
    private $numExpediente;



    /**
     * Set numFicha
     *
     * @param integer $numFicha
     * @return FichaClinica
     */
    public function setNumFicha($numFicha)
    {
        $this->numFicha = $numFicha;

        return $this;
    }

    /**
     * Get numFicha
     *
     * @return integer 
     */
    public function getNumFicha()
    {
        return $this->numFicha;
    }

    /**
     * Set numExpediente
     *
     * @param \foues\FPBundle\Entity\Expediente $numExpediente
     * @return FichaClinica
     */
    public function setNumExpediente(\foues\FPBundle\Entity\Expediente $numExpediente)
    {
        $this->numExpediente = $numExpediente;

        return $this;
    }

    /**
     * Get numExpediente
     *
     * @return \foues\FPBundle\Entity\Expediente 
     */
    public function getNumExpediente()
    {
        return $this->numExpediente;
    }

    /*public function __toString(){
        return (String)$this->getNumFicha();
    }*/
}
