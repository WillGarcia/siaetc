<?php

namespace foues\FPBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AnsDisPpr
 *
 * @ORM\Table(name="ans_dis_ppr", uniqueConstraints={@ORM\UniqueConstraint(name="ans_dis_ppr_pk", columns={"id_ad_ppr"})}, indexes={@ORM\Index(name="fk_clasific_tiene_cla_f_restau_", columns={"id_f_resta"})})
 * @ORM\Entity
 */
class AnsDisPpr
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_ad_ppr", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="ans_dis_ppr_id_ad_ppr_seq", allocationSize=1, initialValue=1)
     */
    private $idAdPpr;

    /**
     * @var string
     *
     * @ORM\Column(name="tipo_ppr", type="string", length=60, nullable=true)
     */
    private $tipoPpr;

    /**
     * @var string
     *
     * @ORM\Column(name="selec_dientes_pi", type="string", length=100, nullable=true)
     */
    private $selecDientesPi;

    /**
     * @var string
     *
     * @ORM\Column(name="areas_soca_pi", type="string", length=150, nullable=true)
     */
    private $areasSocaPi;

    /**
     * @var string
     *
     * @ORM\Column(name="areas_soca_tb", type="string", length=150, nullable=true)
     */
    private $areasSocaTb;

    /**
     * @var string
     *
     * @ORM\Column(name="aplicacion_med", type="string", length=150, nullable=true)
     */
    private $aplicacionMed;

    /**
     * @var string
     *
     * @ORM\Column(name="deter_planos", type="string", length=150, nullable=true)
     */
    private $deterPlanos;

    /**
     * @var string
     *
     * @ORM\Column(name="desgaste_selec", type="string", length=150, nullable=true)
     */
    private $desgasteSelec;

    /**
     * @var string
     *
     * @ORM\Column(name="descrip_pasos", type="string", length=150, nullable=true)
     */
    private $descripPasos;

    /**
     * @var string
     *
     * @ORM\Column(name="tripodizacion", type="string", length=150, nullable=true)
     */
    private $tripodizacion;

    /**
     * @var \FRestaurativa
     *
     * @ORM\ManyToOne(targetEntity="FRestaurativa")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_f_resta", referencedColumnName="id_f_resta")
     * })
     */
    private $idFResta;



    /**
     * Get idAdPpr
     *
     * @return integer 
     */
    public function getIdAdPpr()
    {
        return $this->idAdPpr;
    }

    /**
     * Set tipoPpr
     *
     * @param string $tipoPpr
     * @return AnsDisPpr
     */
    public function setTipoPpr($tipoPpr)
    {
        $this->tipoPpr = $tipoPpr;

        return $this;
    }

    /**
     * Get tipoPpr
     *
     * @return string 
     */
    public function getTipoPpr()
    {
        return $this->tipoPpr;
    }

    /**
     * Set selecDientesPi
     *
     * @param string $selecDientesPi
     * @return AnsDisPpr
     */
    public function setSelecDientesPi($selecDientesPi)
    {
        $this->selecDientesPi = $selecDientesPi;

        return $this;
    }

    /**
     * Get selecDientesPi
     *
     * @return string 
     */
    public function getSelecDientesPi()
    {
        return $this->selecDientesPi;
    }

    /**
     * Set areasSocaPi
     *
     * @param string $areasSocaPi
     * @return AnsDisPpr
     */
    public function setAreasSocaPi($areasSocaPi)
    {
        $this->areasSocaPi = $areasSocaPi;

        return $this;
    }

    /**
     * Get areasSocaPi
     *
     * @return string 
     */
    public function getAreasSocaPi()
    {
        return $this->areasSocaPi;
    }

    /**
     * Set areasSocaTb
     *
     * @param string $areasSocaTb
     * @return AnsDisPpr
     */
    public function setAreasSocaTb($areasSocaTb)
    {
        $this->areasSocaTb = $areasSocaTb;

        return $this;
    }

    /**
     * Get areasSocaTb
     *
     * @return string 
     */
    public function getAreasSocaTb()
    {
        return $this->areasSocaTb;
    }

    /**
     * Set aplicacionMed
     *
     * @param string $aplicacionMed
     * @return AnsDisPpr
     */
    public function setAplicacionMed($aplicacionMed)
    {
        $this->aplicacionMed = $aplicacionMed;

        return $this;
    }

    /**
     * Get aplicacionMed
     *
     * @return string 
     */
    public function getAplicacionMed()
    {
        return $this->aplicacionMed;
    }

    /**
     * Set deterPlanos
     *
     * @param string $deterPlanos
     * @return AnsDisPpr
     */
    public function setDeterPlanos($deterPlanos)
    {
        $this->deterPlanos = $deterPlanos;

        return $this;
    }

    /**
     * Get deterPlanos
     *
     * @return string 
     */
    public function getDeterPlanos()
    {
        return $this->deterPlanos;
    }

    /**
     * Set desgasteSelec
     *
     * @param string $desgasteSelec
     * @return AnsDisPpr
     */
    public function setDesgasteSelec($desgasteSelec)
    {
        $this->desgasteSelec = $desgasteSelec;

        return $this;
    }

    /**
     * Get desgasteSelec
     *
     * @return string 
     */
    public function getDesgasteSelec()
    {
        return $this->desgasteSelec;
    }

    /**
     * Set descripPasos
     *
     * @param string $descripPasos
     * @return AnsDisPpr
     */
    public function setDescripPasos($descripPasos)
    {
        $this->descripPasos = $descripPasos;

        return $this;
    }

    /**
     * Get descripPasos
     *
     * @return string 
     */
    public function getDescripPasos()
    {
        return $this->descripPasos;
    }

    /**
     * Set tripodizacion
     *
     * @param string $tripodizacion
     * @return AnsDisPpr
     */
    public function setTripodizacion($tripodizacion)
    {
        $this->tripodizacion = $tripodizacion;

        return $this;
    }

    /**
     * Get tripodizacion
     *
     * @return string 
     */
    public function getTripodizacion()
    {
        return $this->tripodizacion;
    }

    /**
     * Set idFResta
     *
     * @param \foues\FPBundle\Entity\FRestaurativa $idFResta
     * @return AnsDisPpr
     */
    public function setIdFResta(\foues\FPBundle\Entity\FRestaurativa $idFResta = null)
    {
        $this->idFResta = $idFResta;

        return $this;
    }

    /**
     * Get idFResta
     *
     * @return \foues\FPBundle\Entity\FRestaurativa 
     */
    public function getIdFResta()
    {
        return $this->idFResta;
    }
}
