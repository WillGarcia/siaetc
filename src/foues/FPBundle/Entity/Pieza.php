<?php

namespace foues\FPBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Pieza
 *
 * @ORM\Table(name="pieza")
 * @ORM\Entity
 */
class Pieza
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_pieza", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="pieza_id_pieza_seq", allocationSize=1, initialValue=1)
     */
    private $idPieza;

    /**
     * @var string
     *
     * @ORM\Column(name="pieza", type="string", length=4, nullable=false)
     */
    private $pieza;



    /**
     * Get idPieza
     *
     * @return integer 
     */
    public function getIdPieza()
    {
        return $this->idPieza;
    }

    /**
     * Set pieza
     *
     * @param string $pieza
     * @return Pieza
     */
    public function setPieza($pieza)
    {
        $this->pieza = $pieza;

        return $this;
    }

    /**
     * Get pieza
     *
     * @return string 
     */
    public function getPieza()
    {
        return $this->pieza;
    }
}
