<?php

namespace foues\FPBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ExamenClinicoEndo
 *
 * @ORM\Table(name="examen_clinico_endo", uniqueConstraints={@ORM\UniqueConstraint(name="examen_clinico_endo_pk", columns={"id_obs"})}, indexes={@ORM\Index(name="fk_examen_c_requiere3_eva_pulp_", columns={"id_eva"})})
 * @ORM\Entity
 */
class ExamenClinicoEndo
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_obs", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="examen_clinico_endo_id_obs_seq", allocationSize=1, initialValue=1)
     */
    private $idObs;

    /**
     * @var string
     *
     * @ORM\Column(name="caries", type="string", length=2, nullable=true)
     */
    private $caries;

    /**
     * @var string
     *
     * @ORM\Column(name="restauracion", type="string", length=2, nullable=true)
     */
    private $restauracion;

    /**
     * @var string
     *
     * @ORM\Column(name="frac_den", type="string", length=2, nullable=true)
     */
    private $fracDen;

    /**
     * @var string
     *
     * @ORM\Column(name="pul_exp", type="string", length=2, nullable=true)
     */
    private $pulExp;

    /**
     * @var string
     *
     * @ORM\Column(name="decoloracion", type="string", length=2, nullable=true)
     */
    private $decoloracion;

    /**
     * @var string
     *
     * @ORM\Column(name="fistula", type="string", length=2, nullable=true)
     */
    private $fistula;

    /**
     * @var string
     *
     * @ORM\Column(name="inflamacion", type="string", length=2, nullable=true)
     */
    private $inflamacion;

    /**
     * @var string
     *
     * @ORM\Column(name="bolsa_perio", type="string", length=2, nullable=true)
     */
    private $bolsaPerio;

    /**
     * @var string
     *
     * @ORM\Column(name="mov_grado", type="string", length=2, nullable=true)
     */
    private $movGrado;

    /**
     * @var string
     *
     * @ORM\Column(name="mal_pos_den", type="string", length=2, nullable=true)
     */
    private $malPosDen;

    /**
     * @var \EvaPulparEndo
     *
     * @ORM\ManyToOne(targetEntity="EvaPulparEndo")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_eva", referencedColumnName="id_eva")
     * })
     */
    private $idEva;



    /**
     * Get idObs
     *
     * @return integer 
     */
    public function getIdObs()
    {
        return $this->idObs;
    }

    /**
     * Set caries
     *
     * @param string $caries
     * @return ExamenClinicoEndo
     */
    public function setCaries($caries)
    {
        $this->caries = $caries;

        return $this;
    }

    /**
     * Get caries
     *
     * @return string 
     */
    public function getCaries()
    {
        return $this->caries;
    }

    /**
     * Set restauracion
     *
     * @param string $restauracion
     * @return ExamenClinicoEndo
     */
    public function setRestauracion($restauracion)
    {
        $this->restauracion = $restauracion;

        return $this;
    }

    /**
     * Get restauracion
     *
     * @return string 
     */
    public function getRestauracion()
    {
        return $this->restauracion;
    }

    /**
     * Set fracDen
     *
     * @param string $fracDen
     * @return ExamenClinicoEndo
     */
    public function setFracDen($fracDen)
    {
        $this->fracDen = $fracDen;

        return $this;
    }

    /**
     * Get fracDen
     *
     * @return string 
     */
    public function getFracDen()
    {
        return $this->fracDen;
    }

    /**
     * Set pulExp
     *
     * @param string $pulExp
     * @return ExamenClinicoEndo
     */
    public function setPulExp($pulExp)
    {
        $this->pulExp = $pulExp;

        return $this;
    }

    /**
     * Get pulExp
     *
     * @return string 
     */
    public function getPulExp()
    {
        return $this->pulExp;
    }

    /**
     * Set decoloracion
     *
     * @param string $decoloracion
     * @return ExamenClinicoEndo
     */
    public function setDecoloracion($decoloracion)
    {
        $this->decoloracion = $decoloracion;

        return $this;
    }

    /**
     * Get decoloracion
     *
     * @return string 
     */
    public function getDecoloracion()
    {
        return $this->decoloracion;
    }

    /**
     * Set fistula
     *
     * @param string $fistula
     * @return ExamenClinicoEndo
     */
    public function setFistula($fistula)
    {
        $this->fistula = $fistula;

        return $this;
    }

    /**
     * Get fistula
     *
     * @return string 
     */
    public function getFistula()
    {
        return $this->fistula;
    }

    /**
     * Set inflamacion
     *
     * @param string $inflamacion
     * @return ExamenClinicoEndo
     */
    public function setInflamacion($inflamacion)
    {
        $this->inflamacion = $inflamacion;

        return $this;
    }

    /**
     * Get inflamacion
     *
     * @return string 
     */
    public function getInflamacion()
    {
        return $this->inflamacion;
    }

    /**
     * Set bolsaPerio
     *
     * @param string $bolsaPerio
     * @return ExamenClinicoEndo
     */
    public function setBolsaPerio($bolsaPerio)
    {
        $this->bolsaPerio = $bolsaPerio;

        return $this;
    }

    /**
     * Get bolsaPerio
     *
     * @return string 
     */
    public function getBolsaPerio()
    {
        return $this->bolsaPerio;
    }

    /**
     * Set movGrado
     *
     * @param string $movGrado
     * @return ExamenClinicoEndo
     */
    public function setMovGrado($movGrado)
    {
        $this->movGrado = $movGrado;

        return $this;
    }

    /**
     * Get movGrado
     *
     * @return string 
     */
    public function getMovGrado()
    {
        return $this->movGrado;
    }

    /**
     * Set malPosDen
     *
     * @param string $malPosDen
     * @return ExamenClinicoEndo
     */
    public function setMalPosDen($malPosDen)
    {
        $this->malPosDen = $malPosDen;

        return $this;
    }

    /**
     * Get malPosDen
     *
     * @return string 
     */
    public function getMalPosDen()
    {
        return $this->malPosDen;
    }

    /**
     * Set idEva
     *
     * @param \foues\FPBundle\Entity\EvaPulparEndo $idEva
     * @return ExamenClinicoEndo
     */
    public function setIdEva(\foues\FPBundle\Entity\EvaPulparEndo $idEva = null)
    {
        $this->idEva = $idEva;

        return $this;
    }

    /**
     * Get idEva
     *
     * @return \foues\FPBundle\Entity\EvaPulparEndo 
     */
    public function getIdEva()
    {
        return $this->idEva;
    }
}
