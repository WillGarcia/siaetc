<?php

namespace foues\FPBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ClinicaAsignatura
 *
 * @ORM\Table(name="clinica_asignatura", uniqueConstraints={@ORM\UniqueConstraint(name="clinica_asignatura_pk", columns={"id_clinica"})}, indexes={@ORM\Index(name="fk_clinica__nivel_cur_nivel_cu_", columns={"id_nivel"}), @ORM\Index(name="fk_clinica__se_encuen_area_cli_", columns={"id_area"}), @ORM\Index(name="fk_clinica__represent_docente_f", columns={"id_docente"})})
 * @ORM\Entity
 */
class ClinicaAsignatura
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_clinica", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="clinica_asignatura_id_clinica_seq", allocationSize=1, initialValue=1)
     */
    private $idClinica;

    /**
     * @var \NivelCurso
     *
     * @ORM\ManyToOne(targetEntity="NivelCurso")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_nivel", referencedColumnName="id_nivel")
     * })
     */
    private $idNivel;

    /**
     * @var \Docente
     *
     * @ORM\ManyToOne(targetEntity="Docente")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_docente", referencedColumnName="id_docente")
     * })
     */
    private $idDocente;

    /**
     * @var \AreaClinica
     *
     * @ORM\ManyToOne(targetEntity="AreaClinica")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_area", referencedColumnName="id_area")
     * })
     */
    private $idArea;



    /**
     * Get idClinica
     *
     * @return integer 
     */
    public function getIdClinica()
    {
        return $this->idClinica;
    }

    /**
     * Set idNivel
     *
     * @param \foues\FPBundle\Entity\NivelCurso $idNivel
     * @return ClinicaAsignatura
     */
    public function setIdNivel(\foues\FPBundle\Entity\NivelCurso $idNivel = null)
    {
        $this->idNivel = $idNivel;

        return $this;
    }

    /**
     * Get idNivel
     *
     * @return \foues\FPBundle\Entity\NivelCurso 
     */
    public function getIdNivel()
    {
        return $this->idNivel;
    }

    /**
     * Set idDocente
     *
     * @param \foues\FPBundle\Entity\Docente $idDocente
     * @return ClinicaAsignatura
     */
    public function setIdDocente(\foues\FPBundle\Entity\Docente $idDocente = null)
    {
        $this->idDocente = $idDocente;

        return $this;
    }

    /**
     * Get idDocente
     *
     * @return \foues\FPBundle\Entity\Docente 
     */
    public function getIdDocente()
    {
        return $this->idDocente;
    }

    /**
     * Set idArea
     *
     * @param \foues\FPBundle\Entity\AreaClinica $idArea
     * @return ClinicaAsignatura
     */
    public function setIdArea(\foues\FPBundle\Entity\AreaClinica $idArea = null)
    {
        $this->idArea = $idArea;

        return $this;
    }

    /**
     * Get idArea
     *
     * @return \foues\FPBundle\Entity\AreaClinica 
     */
    public function getIdArea()
    {
        return $this->idArea;
    }
}
