<?php

namespace foues\FPBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * HistorialExp
 *
 * @ORM\Table(name="historial_exp", uniqueConstraints={@ORM\UniqueConstraint(name="id_historial_pk", columns={"id_historial"})}, indexes={@ORM\Index(name="fk_historial_genera_bi_expedien_", columns={"num_expediente"}), @ORM\Index(name="IDX_F0670BD8DF0FA28A", columns={"due"})})
 * @ORM\Entity
 */
class HistorialExp
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_historial", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="historial_exp_id_historial_seq", allocationSize=1, initialValue=1)
     */
    private $idHistorial;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha_h", type="datetime", nullable=false)
     */
    private $fechaH;

    /**
     * @var string
     *
     * @ORM\Column(name="accion", type="string", length=20, nullable=true)
     */
    private $accion;

    /**
     * @var \Expediente
     *
     * @ORM\ManyToOne(targetEntity="Expediente")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="num_expediente", referencedColumnName="num_expediente")
     * })
     */
    private $numExpediente;

    /**
     * @var \Estudiante
     *
     * @ORM\ManyToOne(targetEntity="Estudiante")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="due", referencedColumnName="due")
     * })
     */
    private $due;



    /**
     * Get idHistorial
     *
     * @return integer 
     */
    public function getIdHistorial()
    {
        return $this->idHistorial;
    }

    /**
     * Set fechaH
     *
     * @param \DateTime $fechaH
     * @return HistorialExp
     */
    public function setFechaH($fechaH)
    {
        $this->fechaH = $fechaH;

        return $this;
    }

    /**
     * Get fechaH
     *
     * @return \DateTime 
     */
    public function getFechaH()
    {
        return $this->fechaH;
    }

    /**
     * Set accion
     *
     * @param string $accion
     * @return HistorialExp
     */
    public function setAccion($accion)
    {
        $this->accion = $accion;

        return $this;
    }

    /**
     * Get accion
     *
     * @return string 
     */
    public function getAccion()
    {
        return $this->accion;
    }

    /**
     * Set numExpediente
     *
     * @param \foues\FPBundle\Entity\Expediente $numExpediente
     * @return HistorialExp
     */
    public function setNumExpediente(\foues\FPBundle\Entity\Expediente $numExpediente = null)
    {
        $this->numExpediente = $numExpediente;

        return $this;
    }

    /**
     * Get numExpediente
     *
     * @return \foues\FPBundle\Entity\Expediente 
     */
    public function getNumExpediente()
    {
        return $this->numExpediente;
    }

    /**
     * Set due
     *
     * @param \foues\FPBundle\Entity\Estudiante $due
     * @return HistorialExp
     */
    public function setDue(\foues\FPBundle\Entity\Estudiante $due = null)
    {
        $this->due = $due;

        return $this;
    }

    /**
     * Get due
     *
     * @return \foues\FPBundle\Entity\Estudiante 
     */
    public function getDue()
    {
        return $this->due;
    }
}
