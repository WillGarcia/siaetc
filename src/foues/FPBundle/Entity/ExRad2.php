<?php

namespace foues\FPBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ExRad2
 *
 * @ORM\Table(name="ex_rad_2", uniqueConstraints={@ORM\UniqueConstraint(name="ex_rad_2_pk", columns={"id_exr_2"})}, indexes={@ORM\Index(name="relationship_94_fk", columns={"id_f_dx"})})
 * @ORM\Entity
 */
class ExRad2
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_exr_2", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="ex_rad_2_id_exr_2_seq", allocationSize=1, initialValue=1)
     */
    private $idExr2;

    /**
     * @var string
     *
     * @ORM\Column(name="m2_1_m", type="string", length=2, nullable=true)
     */
    private $m21M;

    /**
     * @var string
     *
     * @ORM\Column(name="d2_1_d", type="string", length=2, nullable=true)
     */
    private $d21D;

    /**
     * @var string
     *
     * @ORM\Column(name="m2_2_m", type="string", length=2, nullable=true)
     */
    private $m22M;

    /**
     * @var string
     *
     * @ORM\Column(name="d2_2_d", type="string", length=2, nullable=true)
     */
    private $d22D;

    /**
     * @var string
     *
     * @ORM\Column(name="m2_3_m", type="string", length=2, nullable=true)
     */
    private $m23M;

    /**
     * @var string
     *
     * @ORM\Column(name="d2_3_d", type="string", length=2, nullable=true)
     */
    private $d23D;

    /**
     * @var string
     *
     * @ORM\Column(name="m2_4_m", type="string", length=2, nullable=true)
     */
    private $m24M;

    /**
     * @var string
     *
     * @ORM\Column(name="d2_4_d", type="string", length=2, nullable=true)
     */
    private $d24D;

    /**
     * @var string
     *
     * @ORM\Column(name="m2_5_m", type="string", length=2, nullable=true)
     */
    private $m25M;

    /**
     * @var string
     *
     * @ORM\Column(name="d2_5_d", type="string", length=2, nullable=true)
     */
    private $d25D;

    /**
     * @var string
     *
     * @ORM\Column(name="m2_6_m", type="string", length=2, nullable=true)
     */
    private $m26M;

    /**
     * @var string
     *
     * @ORM\Column(name="d2_6_d", type="string", length=2, nullable=true)
     */
    private $d26D;

    /**
     * @var string
     *
     * @ORM\Column(name="m2_7_m", type="string", length=2, nullable=true)
     */
    private $m27M;

    /**
     * @var string
     *
     * @ORM\Column(name="d2_7_d", type="string", length=2, nullable=true)
     */
    private $d27D;

    /**
     * @var string
     *
     * @ORM\Column(name="m2_8_m", type="string", length=2, nullable=true)
     */
    private $m28M;

    /**
     * @var string
     *
     * @ORM\Column(name="d_2_8", type="string", length=2, nullable=true)
     */
    private $d28;

    /**
     * @var \FDiagnostico
     *
     * @ORM\ManyToOne(targetEntity="FDiagnostico")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_f_dx", referencedColumnName="id_f_dx")
     * })
     */
    private $idFDx;



    /**
     * Get idExr2
     *
     * @return integer 
     */
    public function getIdExr2()
    {
        return $this->idExr2;
    }

    /**
     * Set m21M
     *
     * @param string $m21M
     * @return ExRad2
     */
    public function setM21M($m21M)
    {
        $this->m21M = $m21M;

        return $this;
    }

    /**
     * Get m21M
     *
     * @return string 
     */
    public function getM21M()
    {
        return $this->m21M;
    }

    /**
     * Set d21D
     *
     * @param string $d21D
     * @return ExRad2
     */
    public function setD21D($d21D)
    {
        $this->d21D = $d21D;

        return $this;
    }

    /**
     * Get d21D
     *
     * @return string 
     */
    public function getD21D()
    {
        return $this->d21D;
    }

    /**
     * Set m22M
     *
     * @param string $m22M
     * @return ExRad2
     */
    public function setM22M($m22M)
    {
        $this->m22M = $m22M;

        return $this;
    }

    /**
     * Get m22M
     *
     * @return string 
     */
    public function getM22M()
    {
        return $this->m22M;
    }

    /**
     * Set d22D
     *
     * @param string $d22D
     * @return ExRad2
     */
    public function setD22D($d22D)
    {
        $this->d22D = $d22D;

        return $this;
    }

    /**
     * Get d22D
     *
     * @return string 
     */
    public function getD22D()
    {
        return $this->d22D;
    }

    /**
     * Set m23M
     *
     * @param string $m23M
     * @return ExRad2
     */
    public function setM23M($m23M)
    {
        $this->m23M = $m23M;

        return $this;
    }

    /**
     * Get m23M
     *
     * @return string 
     */
    public function getM23M()
    {
        return $this->m23M;
    }

    /**
     * Set d23D
     *
     * @param string $d23D
     * @return ExRad2
     */
    public function setD23D($d23D)
    {
        $this->d23D = $d23D;

        return $this;
    }

    /**
     * Get d23D
     *
     * @return string 
     */
    public function getD23D()
    {
        return $this->d23D;
    }

    /**
     * Set m24M
     *
     * @param string $m24M
     * @return ExRad2
     */
    public function setM24M($m24M)
    {
        $this->m24M = $m24M;

        return $this;
    }

    /**
     * Get m24M
     *
     * @return string 
     */
    public function getM24M()
    {
        return $this->m24M;
    }

    /**
     * Set d24D
     *
     * @param string $d24D
     * @return ExRad2
     */
    public function setD24D($d24D)
    {
        $this->d24D = $d24D;

        return $this;
    }

    /**
     * Get d24D
     *
     * @return string 
     */
    public function getD24D()
    {
        return $this->d24D;
    }

    /**
     * Set m25M
     *
     * @param string $m25M
     * @return ExRad2
     */
    public function setM25M($m25M)
    {
        $this->m25M = $m25M;

        return $this;
    }

    /**
     * Get m25M
     *
     * @return string 
     */
    public function getM25M()
    {
        return $this->m25M;
    }

    /**
     * Set d25D
     *
     * @param string $d25D
     * @return ExRad2
     */
    public function setD25D($d25D)
    {
        $this->d25D = $d25D;

        return $this;
    }

    /**
     * Get d25D
     *
     * @return string 
     */
    public function getD25D()
    {
        return $this->d25D;
    }

    /**
     * Set m26M
     *
     * @param string $m26M
     * @return ExRad2
     */
    public function setM26M($m26M)
    {
        $this->m26M = $m26M;

        return $this;
    }

    /**
     * Get m26M
     *
     * @return string 
     */
    public function getM26M()
    {
        return $this->m26M;
    }

    /**
     * Set d26D
     *
     * @param string $d26D
     * @return ExRad2
     */
    public function setD26D($d26D)
    {
        $this->d26D = $d26D;

        return $this;
    }

    /**
     * Get d26D
     *
     * @return string 
     */
    public function getD26D()
    {
        return $this->d26D;
    }

    /**
     * Set m27M
     *
     * @param string $m27M
     * @return ExRad2
     */
    public function setM27M($m27M)
    {
        $this->m27M = $m27M;

        return $this;
    }

    /**
     * Get m27M
     *
     * @return string 
     */
    public function getM27M()
    {
        return $this->m27M;
    }

    /**
     * Set d27D
     *
     * @param string $d27D
     * @return ExRad2
     */
    public function setD27D($d27D)
    {
        $this->d27D = $d27D;

        return $this;
    }

    /**
     * Get d27D
     *
     * @return string 
     */
    public function getD27D()
    {
        return $this->d27D;
    }

    /**
     * Set m28M
     *
     * @param string $m28M
     * @return ExRad2
     */
    public function setM28M($m28M)
    {
        $this->m28M = $m28M;

        return $this;
    }

    /**
     * Get m28M
     *
     * @return string 
     */
    public function getM28M()
    {
        return $this->m28M;
    }

    /**
     * Set d28
     *
     * @param string $d28
     * @return ExRad2
     */
    public function setD28($d28)
    {
        $this->d28 = $d28;

        return $this;
    }

    /**
     * Get d28
     *
     * @return string 
     */
    public function getD28()
    {
        return $this->d28;
    }

    /**
     * Set idFDx
     *
     * @param \foues\FPBundle\Entity\FDiagnostico $idFDx
     * @return ExRad2
     */
    public function setIdFDx(\foues\FPBundle\Entity\FDiagnostico $idFDx = null)
    {
        $this->idFDx = $idFDx;

        return $this;
    }

    /**
     * Get idFDx
     *
     * @return \foues\FPBundle\Entity\FDiagnostico 
     */
    public function getIdFDx()
    {
        return $this->idFDx;
    }
}
