<?php

namespace foues\FPBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * FPeriodoncia
 *
 * @ORM\Table(name="f_periodoncia", uniqueConstraints={@ORM\UniqueConstraint(name="f_periodoncia_pk", columns={"id_f_perio"})}, indexes={@ORM\Index(name="fk_f_period_considera_ficha_cl_", columns={"num_expediente"})})
 * @ORM\Entity
 */
class FPeriodoncia
{
    //Atributo creado con el fin de mostrar datos en la ficha de periodoncia
    protected $paciente;
    /**
     * @var integer
     *
     * @ORM\Column(name="id_f_perio", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="f_periodoncia_id_f_perio_seq", allocationSize=1, initialValue=1)
     */
    private $idFPerio;

    /**
     * @var integer
     *
     * @ORM\Column(name="area_ficha", type="integer", nullable=false)
     */
    private $areaFicha;

    /**
     * @var string
     *
     * @ORM\Column(name="motivo_con_pe", type="string", length=60, nullable=true)
     */
    private $motivoConPe;

    /**
     * @var string
     *
     * @ORM\Column(name="his_med_pe", type="string", length=150, nullable=true)
     */
    private $hisMedPe;

    /**
     * @var string
     *
     * @ORM\Column(name="his_odonto_pe", type="string", length=150, nullable=true)
     */
    private $hisOdontoPe;

    /**
     * @var string
     *
     * @ORM\Column(name="habitos_or_pe", type="string", length=100, nullable=true)
     */
    private $habitosOrPe;

    /**
     * @var \FichaClinica
     *
     * @ORM\ManyToOne(targetEntity="FichaClinica")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="num_expediente", referencedColumnName="num_expediente")
     * })
     */
    private $numExpediente;



    /**
     * Get idFPerio
     *
     * @return integer 
     */
    public function getIdFPerio()
    {
        return $this->idFPerio;
    }

    /**
     * Set areaFicha
     *
     * @param integer $areaFicha
     * @return FPeriodoncia
     */
    public function setAreaFicha($areaFicha)
    {
        $this->areaFicha = $areaFicha;

        return $this;
    }

    /**
     * Get areaFicha
     *
     * @return integer 
     */
    public function getAreaFicha()
    {
        return $this->areaFicha;
    }

    /**
     * Set motivoConPe
     *
     * @param string $motivoConPe
     * @return FPeriodoncia
     */
    public function setMotivoConPe($motivoConPe)
    {
        $this->motivoConPe = $motivoConPe;

        return $this;
    }

    /**
     * Get motivoConPe
     *
     * @return string 
     */
    public function getMotivoConPe()
    {
        return $this->motivoConPe;
    }

    /**
     * Set hisMedPe
     *
     * @param string $hisMedPe
     * @return FPeriodoncia
     */
    public function setHisMedPe($hisMedPe)
    {
        $this->hisMedPe = $hisMedPe;

        return $this;
    }

    /**
     * Get hisMedPe
     *
     * @return string 
     */
    public function getHisMedPe()
    {
        return $this->hisMedPe;
    }

    /**
     * Set hisOdontoPe
     *
     * @param string $hisOdontoPe
     * @return FPeriodoncia
     */
    public function setHisOdontoPe($hisOdontoPe)
    {
        $this->hisOdontoPe = $hisOdontoPe;

        return $this;
    }

    /**
     * Get hisOdontoPe
     *
     * @return string 
     */
    public function getHisOdontoPe()
    {
        return $this->hisOdontoPe;
    }

    /**
     * Set habitosOrPe
     *
     * @param string $habitosOrPe
     * @return FPeriodoncia
     */
    public function setHabitosOrPe($habitosOrPe)
    {
        $this->habitosOrPe = $habitosOrPe;

        return $this;
    }

    /**
     * Get habitosOrPe
     *
     * @return string 
     */
    public function getHabitosOrPe()
    {
        return $this->habitosOrPe;
    }

    /**
     * Set numExpediente
     *
     * @param \foues\FPBundle\Entity\FichaClinica $numExpediente
     * @return FPeriodoncia
     */
    public function setNumExpediente(\foues\FPBundle\Entity\FichaClinica $numExpediente = null)
    {
        $this->numExpediente = $numExpediente;

        return $this;
    }

    /**
     * Get numExpediente
     *
     * @return \foues\FPBundle\Entity\FichaClinica 
     */
    public function getNumExpediente()
    {
        return $this->numExpediente;
    }


}
