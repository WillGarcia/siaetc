<?php

namespace foues\FPBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Cita
 *
 * @ORM\Table(name="cita", uniqueConstraints={@ORM\UniqueConstraint(name="cita_pk", columns={"id_cita"})}, indexes={@ORM\Index(name="fk_cita_asigna_estudian_fk", columns={"due"})})
 * @ORM\Entity
 */
class Cita
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_cita", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="cita_id_cita_seq", allocationSize=1, initialValue=1)
     */
    private $idCita;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="prox_cita", type="date", nullable=true)
     */
    private $proxCita;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="hora", type="time", nullable=true)
     */
    private $hora;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha_emi", type="date", nullable=true)
     */
    private $fechaEmi;

    /**
     * @var \Estudiante
     *
     * @ORM\ManyToOne(targetEntity="Estudiante")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="due", referencedColumnName="due")
     * })
     */
    private $due;



    /**
     * Get idCita
     *
     * @return integer 
     */
    public function getIdCita()
    {
        return $this->idCita;
    }

    /**
     * Set proxCita
     *
     * @param \DateTime $proxCita
     * @return Cita
     */
    public function setProxCita($proxCita)
    {
        $this->proxCita = $proxCita;

        return $this;
    }

    /**
     * Get proxCita
     *
     * @return \DateTime 
     */
    public function getProxCita()
    {
        return $this->proxCita;
    }

    /**
     * Set hora
     *
     * @param \DateTime $hora
     * @return Cita
     */
    public function setHora($hora)
    {
        $this->hora = $hora;

        return $this;
    }

    /**
     * Get hora
     *
     * @return \DateTime 
     */
    public function getHora()
    {
        return $this->hora;
    }

    /**
     * Set fechaEmi
     *
     * @param \DateTime $fechaEmi
     * @return Cita
     */
    public function setFechaEmi($fechaEmi)
    {
        $this->fechaEmi = $fechaEmi;

        return $this;
    }

    /**
     * Get fechaEmi
     *
     * @return \DateTime 
     */
    public function getFechaEmi()
    {
        return $this->fechaEmi;
    }

    /**
     * Set due
     *
     * @param \foues\FPBundle\Entity\Estudiante $due
     * @return Cita
     */
    public function setDue(\foues\FPBundle\Entity\Estudiante $due = null)
    {
        $this->due = $due;

        return $this;
    }

    /**
     * Get due
     *
     * @return \foues\FPBundle\Entity\Estudiante 
     */
    public function getDue()
    {
        return $this->due;
    }
}
