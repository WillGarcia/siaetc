<?php

namespace foues\FPBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ControlPeriodontograma
 *
 * @ORM\Table(name="control_periodontograma", indexes={@ORM\Index(name="IDX_85D0DFEB9D224214", columns={"id_f_perio"}), @ORM\Index(name="IDX_85D0DFEB5EE25C56", columns={"id_cuadrant"}), @ORM\Index(name="IDX_85D0DFEB36222712", columns={"id_pieza"}), @ORM\Index(name="IDX_85D0DFEB8ACF8DFB", columns={"id_imagenc"}), @ORM\Index(name="IDX_85D0DFEBB212090D", columns={"id_tcuadrante"})})
 * @ORM\Entity
 */
class ControlPeriodontograma
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_control", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="control_periodontograma_id_control_seq", allocationSize=1, initialValue=1)
     */
    private $idControl;

    /**
     * @var integer
     *
     * @ORM\Column(name="surco", type="integer", nullable=true)
     */
    private $surco;

    /**
     * @var integer
     *
     * @ORM\Column(name="recesion", type="integer", nullable=true)
     */
    private $recesion;

    /**
     * @var integer
     *
     * @ORM\Column(name="insercion", type="integer", nullable=true)
     */
    private $insercion;

    /**
     * @var integer
     *
     * @ORM\Column(name="margen", type="integer", nullable=true)
     */
    private $margen;

    /**
     * @var integer
     *
     * @ORM\Column(name="result", type="integer", nullable=true)
     */
    private $result;

    /**
     * @var \FPeriodoncia
     *
     * @ORM\ManyToOne(targetEntity="FPeriodoncia")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_f_perio", referencedColumnName="id_f_perio")
     * })
     */
    private $idFPerio;

    /**
     * @var \Cuadrante
     *
     * @ORM\ManyToOne(targetEntity="Cuadrante")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_cuadrant", referencedColumnName="id_cuadrant")
     * })
     */
    private $idCuadrant;

    /**
     * @var \Pieza
     *
     * @ORM\ManyToOne(targetEntity="Pieza")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_pieza", referencedColumnName="id_pieza")
     * })
     */
    private $idPieza;

    /**
     * @var \ImagenControlp
     *
     * @ORM\ManyToOne(targetEntity="ImagenControlp")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_imagenc", referencedColumnName="id_imagenc")
     * })
     */
    private $idImagenc;

    /**
     * @var \TipoCuadrante
     *
     * @ORM\ManyToOne(targetEntity="TipoCuadrante")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_tcuadrante", referencedColumnName="id_tcuadrante")
     * })
     */
    private $idTcuadrante;



    /**
     * Get idControl
     *
     * @return integer 
     */
    public function getIdControl()
    {
        return $this->idControl;
    }

    /**
     * Set surco
     *
     * @param integer $surco
     * @return ControlPeriodontograma
     */
    public function setSurco($surco)
    {
        $this->surco = $surco;

        return $this;
    }

    /**
     * Get surco
     *
     * @return integer 
     */
    public function getSurco()
    {
        return $this->surco;
    }

    /**
     * Set recesion
     *
     * @param integer $recesion
     * @return ControlPeriodontograma
     */
    public function setRecesion($recesion)
    {
        $this->recesion = $recesion;

        return $this;
    }

    /**
     * Get recesion
     *
     * @return integer 
     */
    public function getRecesion()
    {
        return $this->recesion;
    }

    /**
     * Set insercion
     *
     * @param integer $insercion
     * @return ControlPeriodontograma
     */
    public function setInsercion($insercion)
    {
        $this->insercion = $insercion;

        return $this;
    }

    /**
     * Get insercion
     *
     * @return integer 
     */
    public function getInsercion()
    {
        return $this->insercion;
    }

    /**
     * Set margen
     *
     * @param integer $margen
     * @return ControlPeriodontograma
     */
    public function setMargen($margen)
    {
        $this->margen = $margen;

        return $this;
    }

    /**
     * Get margen
     *
     * @return integer 
     */
    public function getMargen()
    {
        return $this->margen;
    }

    /**
     * Set result
     *
     * @param integer $result
     * @return ControlPeriodontograma
     */
    public function setResult($result)
    {
        $this->result = $result;

        return $this;
    }

    /**
     * Get result
     *
     * @return integer 
     */
    public function getResult()
    {
        return $this->result;
    }

    /**
     * Set idFPerio
     *
     * @param \foues\FPBundle\Entity\FPeriodoncia $idFPerio
     * @return ControlPeriodontograma
     */
    public function setIdFPerio(\foues\FPBundle\Entity\FPeriodoncia $idFPerio = null)
    {
        $this->idFPerio = $idFPerio;

        return $this;
    }

    /**
     * Get idFPerio
     *
     * @return \foues\FPBundle\Entity\FPeriodoncia 
     */
    public function getIdFPerio()
    {
        return $this->idFPerio;
    }

    /**
     * Set idCuadrant
     *
     * @param \foues\FPBundle\Entity\Cuadrante $idCuadrant
     * @return ControlPeriodontograma
     */
    public function setIdCuadrant(\foues\FPBundle\Entity\Cuadrante $idCuadrant = null)
    {
        $this->idCuadrant = $idCuadrant;

        return $this;
    }

    /**
     * Get idCuadrant
     *
     * @return \foues\FPBundle\Entity\Cuadrante 
     */
    public function getIdCuadrant()
    {
        return $this->idCuadrant;
    }

    /**
     * Set idPieza
     *
     * @param \foues\FPBundle\Entity\Pieza $idPieza
     * @return ControlPeriodontograma
     */
    public function setIdPieza(\foues\FPBundle\Entity\Pieza $idPieza = null)
    {
        $this->idPieza = $idPieza;

        return $this;
    }

    /**
     * Get idPieza
     *
     * @return \foues\FPBundle\Entity\Pieza 
     */
    public function getIdPieza()
    {
        return $this->idPieza;
    }

    /**
     * Set idImagenc
     *
     * @param \foues\FPBundle\Entity\ImagenControlp $idImagenc
     * @return ControlPeriodontograma
     */
    public function setIdImagenc(\foues\FPBundle\Entity\ImagenControlp $idImagenc = null)
    {
        $this->idImagenc = $idImagenc;

        return $this;
    }

    /**
     * Get idImagenc
     *
     * @return \foues\FPBundle\Entity\ImagenControlp 
     */
    public function getIdImagenc()
    {
        return $this->idImagenc;
    }

    /**
     * Set idTcuadrante
     *
     * @param \foues\FPBundle\Entity\TipoCuadrante $idTcuadrante
     * @return ControlPeriodontograma
     */
    public function setIdTcuadrante(\foues\FPBundle\Entity\TipoCuadrante $idTcuadrante = null)
    {
        $this->idTcuadrante = $idTcuadrante;

        return $this;
    }

    /**
     * Get idTcuadrante
     *
     * @return \foues\FPBundle\Entity\TipoCuadrante 
     */
    public function getIdTcuadrante()
    {
        return $this->idTcuadrante;
    }
}
