<?php

namespace foues\FPBundle\Form;

use foues\FPBundle\Entity\Expediente;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;

class FichaClinicaType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            /*->add('numExpediente',CollectionType::class,array(
                'entry_type'        =>Expediente::class,
                'data'              => new Expediente(),
                'by_reference'      =>false,
                'allow_delete'      =>false,    //Evita que elimine expedientes de forma dinamica
                'allow_add'         =>false     //Evita que inserte expedientes de forma dinamica
            ))*/
            ->add('numExpediente', new ExpedienteType(),array(
                'attr'=> array('class'=>'well'),
                'label'    =>false
            ))
        ;
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'foues\FPBundle\Entity\FichaClinica'
        ));
    }
}
