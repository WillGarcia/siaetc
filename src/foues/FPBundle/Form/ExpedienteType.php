<?php

namespace foues\FPBundle\Form;

use foues\FPBundle\Entity\Paciente;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ExpedienteType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            /*->add('idPac', CollectionType::class, array(
                'entry_type'    =>Paciente::class,
                'data'          => new Paciente(),
                'by_reference'  =>false,
                'allow_delete'  =>false,
                'allow_add'     =>false,
                'prototype'     =>true
            ))*/
            ->add('idPac', new PacienteType(), array(
                'attr'=> array('class'=>'well'),
                'label'     =>false
            ))
        ;
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'foues\FPBundle\Entity\Expediente'
        ));
    }
}
