<?php

namespace foues\FPBundle\Form;

use Doctrine\ORM\EntityRepository;
use foues\FPBundle\Entity\Expediente;
use foues\FPBundle\Entity\FichaClinica;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\TextType;



class FPeriodonciaType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            /*->add('numExpediente',CollectionType::class,array(
                'entry_type'        =>FichaClinicaType::class,
                'data'              =>new FichaClinica(),
                'by_reference'      =>true,
                'allow_delete'      =>false,    //Evita que borren fichasClinicas dinamicamente
                'allow_add'         =>true,      //Permite que agreguen fichasClinicas dinamicamente
                'prototype'         =>true
            ))*/
            ->add('numExpediente', new FichaClinicaType(), array(
                'attr'      => array('class'=>'well'),
                'label'     =>false
            ))

            //->add('areaFicha')
            ->add('motivoConPe',TextType::class,array(
                'label'=>'Motivo Consulta: '
            ))
            ->add('hisMedPe',TextType::class,array(
                'label'=>'Historia Medica Anterior: '
            ))
            ->add('hisOdontoPe',TextType::class,array(
                'label'=>'Historia Odontologica Anterior: '
            ))
            ->add('habitosOrPe',TextType::class,array(
                'label'=>'Habitos Orales: '
            ))
        ;
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'foues\FPBundle\Entity\FPeriodoncia'
        ));
    }
}
