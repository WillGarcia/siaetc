<?php

namespace foues\FPBundle\Form;

//use Doctrine\DBAL\Types\TextType;
use foues\FPBundle\Entity\Paciente;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;


class PacienteType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            //->add('dui')
            ->add('numExpediente',TextType::class,array(
                    'disabled'=>true, //Establece que los campos correspondientes al paciente no podran ser editados desde este formulario
                    'label'=>'No Expediente '
            ))
            //->add('fechaApertura', 'date')
            //->add('nom1Pac')
            //->add('nom2Pac')
            //->add('nom3Pac')
            //->add('ape1Pac')
            //->add('ape2Pac')
            //->add('ape3Pac')
            ->add('nompaciente',TextType::class,array(
                'data'=>new Paciente(), //Establezco que trabajare con un objeto de tipo paciente dentro de este formulario
                'disabled'=>true, //Establece que los campos correspondientes al paciente no podran ser editados desde este formulario
                'label'=>'Nombre del Paciente: '))
            //->add('fechaNac', 'date')
            ->add('edad',TextType::class,array(
                'disabled'=>true, //Establece que los campos correspondientes al paciente no podran ser editados desde este formulario
                'label'=>'Edad: '
            ))
            //->add('deptoNac')
            //->add('munNac')
            //->add('deptoRes')
            //->add('munRes')
            //->add('nivelEduc')
            ->add('idGenero',TextType::class,array(
                'disabled'=>true, //Establece que los campos correspondientes al paciente no podran ser editados desde este formulario
                'label'=>'Genero: '
            ))
            ->add('idEstadoC',TextType::class,array(
                'disabled'=>true, //Establece que los campos correspondientes al paciente no podran ser editados desde este formulario
                'label'=>'Estado Civil: '
            ))
            ->add('ocupacion',TextType::class,array(
                'disabled'=>true, //Establece que los campos correspondientes al paciente no podran ser editados desde este formulario
                'label'=>'Ocupacion: '
            ))
            ->add('domicilio',TextType::class,array(
                'disabled'=>true, //Establece que los campos correspondientes al paciente no podran ser editados desde este formulario
                'label'=>'Domicilio: '
            ))
            ->add('telefonoPx',TextType::class,array(
                'disabled'=>true, //Establece que los campos correspondientes al paciente no podran ser editados desde este formulario
                'label'=>'Telefono de Contacto: '
            ))
            //->add('movilPx')
            //->add('dirTrabajo')
            //->add('telTrabajo')
            //->add('responsable')
            //->add('telResponsable')

            //->add('activo')
            //->add('idGenero')

        ;
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'foues\FPBundle\Entity\Paciente'
        ));
    }


}
