<?php

namespace foues\FPBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('fouesFPBundle:Default:index.html.twig');
    }
}
