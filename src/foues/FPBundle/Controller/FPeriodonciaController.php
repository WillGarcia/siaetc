<?php

namespace foues\FPBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use foues\FPBundle\Entity\FPeriodoncia;
use foues\FPBundle\Form\FPeriodonciaType;

/**
 * FPeriodoncia controller.
 *
 */
class FPeriodonciaController extends Controller
{
    /**
     * Lists all FPeriodoncia entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $fPeriodoncias = $em->getRepository('fouesFPBundle:FPeriodoncia')->findAll();

        return $this->render('fperiodoncia/index.html.twig', array(
            'fPeriodoncias' => $fPeriodoncias,
        ));
    }

    /**
     * Creates a new FPeriodoncia entity.
     *
     */
    public function newAction(Request $request)
    {
        $fPeriodoncium = new FPeriodoncia();
        $form = $this->createForm('foues\FPBundle\Form\FPeriodonciaType', $fPeriodoncium);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($fPeriodoncium);
            $em->flush();

            return $this->redirectToRoute('fperiodoncia_show', array('id' => $fPeriodoncium->getId()));
        }

        return $this->render('fperiodoncia/new.html.twig', array(
            'fPeriodoncium' => $fPeriodoncium,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a FPeriodoncia entity.
     *
     */
    public function showAction(FPeriodoncia $fPeriodoncium)
    {
        $deleteForm = $this->createDeleteForm($fPeriodoncium);

        return $this->render('fperiodoncia/show.html.twig', array(
            'fPeriodoncium' => $fPeriodoncium,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing FPeriodoncia entity.
     *
     */
    public function editAction(Request $request, FPeriodoncia $fPeriodoncium)
    {
        $deleteForm = $this->createDeleteForm($fPeriodoncium);
        $editForm = $this->createForm('foues\FPBundle\Form\FPeriodonciaType', $fPeriodoncium);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($fPeriodoncium);
            $em->flush();

            return $this->redirectToRoute('fperiodoncia_edit', array('id' => $fPeriodoncium->getId()));
        }

        return $this->render('fperiodoncia/edit.html.twig', array(
            'fPeriodoncium' => $fPeriodoncium,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a FPeriodoncia entity.
     *
     */
    public function deleteAction(Request $request, FPeriodoncia $fPeriodoncium)
    {
        $form = $this->createDeleteForm($fPeriodoncium);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($fPeriodoncium);
            $em->flush();
        }

        return $this->redirectToRoute('fperiodoncia_index');
    }

    /**
     * Creates a form to delete a FPeriodoncia entity.
     *
     * @param FPeriodoncia $fPeriodoncium The FPeriodoncia entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(FPeriodoncia $fPeriodoncium)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('fperiodoncia_delete', array('id' => $fPeriodoncium->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }

    /*---------------INICIAN FUNCIONES DE CONTROLADOR PERSONALIZADAS -------------------------------------------------*/
    //NOTA: por cuestiones de prueba no recibe parametro, cuando este en produccion debera recibir el numero de expediente
    public function periodontoAction(){
        //return $this->render('FPBundle:periodonto:periodontograma.html.twig');
        return $this->render('fouesFPBundle:periodonto:periodontograma.html.twig');
    }
}
