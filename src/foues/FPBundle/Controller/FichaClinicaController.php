<?php

namespace foues\FPBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use foues\FPBundle\Entity\FichaClinica;
use foues\FPBundle\Form\FichaClinicaType;

/**
 * FichaClinica controller.
 *
 */
class FichaClinicaController extends Controller
{
    /**
     * Lists all FichaClinica entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $fichaClinicas = $em->getRepository('fouesFPBundle:FichaClinica')->findAll();

        return $this->render('fichaclinica/index.html.twig', array(
            'fichaClinicas' => $fichaClinicas,
        ));
    }

    /**
     * Creates a new FichaClinica entity.
     *
     */
    public function newAction(Request $request)
    {
        $fichaClinica = new FichaClinica();
        $form = $this->createForm('foues\FPBundle\Form\FichaClinicaType', $fichaClinica);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($fichaClinica);
            $em->flush();

            return $this->redirectToRoute('fichaclinica_show', array('id' => $fichaClinica->getId()));
        }

        return $this->render('fichaclinica/new.html.twig', array(
            'fichaClinica' => $fichaClinica,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a FichaClinica entity.
     *
     */
    public function showAction(FichaClinica $fichaClinica)
    {
        $deleteForm = $this->createDeleteForm($fichaClinica);

        return $this->render('fichaclinica/show.html.twig', array(
            'fichaClinica' => $fichaClinica,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing FichaClinica entity.
     *
     */
    public function editAction(Request $request, FichaClinica $fichaClinica)
    {
        $deleteForm = $this->createDeleteForm($fichaClinica);
        $editForm = $this->createForm('foues\FPBundle\Form\FichaClinicaType', $fichaClinica);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($fichaClinica);
            $em->flush();

            return $this->redirectToRoute('fichaclinica_edit', array('id' => $fichaClinica->getId()));
        }

        return $this->render('fichaclinica/edit.html.twig', array(
            'fichaClinica' => $fichaClinica,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a FichaClinica entity.
     *
     */
    public function deleteAction(Request $request, FichaClinica $fichaClinica)
    {
        $form = $this->createDeleteForm($fichaClinica);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($fichaClinica);
            $em->flush();
        }

        return $this->redirectToRoute('fichaclinica_index');
    }

    /**
     * Creates a form to delete a FichaClinica entity.
     *
     * @param FichaClinica $fichaClinica The FichaClinica entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(FichaClinica $fichaClinica)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('fichaclinica_delete', array('id' => $fichaClinica->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
