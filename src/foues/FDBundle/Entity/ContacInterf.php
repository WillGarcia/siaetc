<?php

namespace foues\FDBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ContacInterf
 *
 * @ORM\Table(name="contac_interf", uniqueConstraints={@ORM\UniqueConstraint(name="contac_interf_pk", columns={"id_cont_intrf"})}, indexes={@ORM\Index(name="relationship_98_fk", columns={"id_f_perio"})})
 * @ORM\Entity
 */
class ContacInterf
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_cont_intrf", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="contac_interf_id_cont_intrf_seq", allocationSize=1, initialValue=1)
     */
    private $idContIntrf;

    /**
     * @var string
     *
     * @ORM\Column(name="ima_con_rc_azul", type="string", length=255, nullable=true)
     */
    private $imaConRcAzul;

    /**
     * @var string
     *
     * @ORM\Column(name="ima_intr_lt_rojo", type="string", length=255, nullable=true)
     */
    private $imaIntrLtRojo;

    /**
     * @var string
     *
     * @ORM\Column(name="ima_intr_lb_verde", type="string", length=255, nullable=true)
     */
    private $imaIntrLbVerde;

    /**
     * @var string
     *
     * @ORM\Column(name="ima_intr_pro_negro", type="string", length=255, nullable=true)
     */
    private $imaIntrProNegro;

    /**
     * @var string
     *
     * @ORM\Column(name="dx_periodontal", type="string", length=100, nullable=true)
     */
    private $dxPeriodontal;

    /**
     * @var \FPeriodoncia
     *
     * @ORM\ManyToOne(targetEntity="FPeriodoncia")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_f_perio", referencedColumnName="id_f_perio")
     * })
     */
    private $idFPerio;



    /**
     * Get idContIntrf
     *
     * @return integer 
     */
    public function getIdContIntrf()
    {
        return $this->idContIntrf;
    }

    /**
     * Set imaConRcAzul
     *
     * @param string $imaConRcAzul
     * @return ContacInterf
     */
    public function setImaConRcAzul($imaConRcAzul)
    {
        $this->imaConRcAzul = $imaConRcAzul;

        return $this;
    }

    /**
     * Get imaConRcAzul
     *
     * @return string 
     */
    public function getImaConRcAzul()
    {
        return $this->imaConRcAzul;
    }

    /**
     * Set imaIntrLtRojo
     *
     * @param string $imaIntrLtRojo
     * @return ContacInterf
     */
    public function setImaIntrLtRojo($imaIntrLtRojo)
    {
        $this->imaIntrLtRojo = $imaIntrLtRojo;

        return $this;
    }

    /**
     * Get imaIntrLtRojo
     *
     * @return string 
     */
    public function getImaIntrLtRojo()
    {
        return $this->imaIntrLtRojo;
    }

    /**
     * Set imaIntrLbVerde
     *
     * @param string $imaIntrLbVerde
     * @return ContacInterf
     */
    public function setImaIntrLbVerde($imaIntrLbVerde)
    {
        $this->imaIntrLbVerde = $imaIntrLbVerde;

        return $this;
    }

    /**
     * Get imaIntrLbVerde
     *
     * @return string 
     */
    public function getImaIntrLbVerde()
    {
        return $this->imaIntrLbVerde;
    }

    /**
     * Set imaIntrProNegro
     *
     * @param string $imaIntrProNegro
     * @return ContacInterf
     */
    public function setImaIntrProNegro($imaIntrProNegro)
    {
        $this->imaIntrProNegro = $imaIntrProNegro;

        return $this;
    }

    /**
     * Get imaIntrProNegro
     *
     * @return string 
     */
    public function getImaIntrProNegro()
    {
        return $this->imaIntrProNegro;
    }

    /**
     * Set dxPeriodontal
     *
     * @param string $dxPeriodontal
     * @return ContacInterf
     */
    public function setDxPeriodontal($dxPeriodontal)
    {
        $this->dxPeriodontal = $dxPeriodontal;

        return $this;
    }

    /**
     * Get dxPeriodontal
     *
     * @return string 
     */
    public function getDxPeriodontal()
    {
        return $this->dxPeriodontal;
    }

    /**
     * Set idFPerio
     *
     * @param \foues\FDBundle\Entity\FPeriodoncia $idFPerio
     * @return ContacInterf
     */
    public function setIdFPerio(\foues\FDBundle\Entity\FPeriodoncia $idFPerio = null)
    {
        $this->idFPerio = $idFPerio;

        return $this;
    }

    /**
     * Get idFPerio
     *
     * @return \foues\FDBundle\Entity\FPeriodoncia 
     */
    public function getIdFPerio()
    {
        return $this->idFPerio;
    }
}
