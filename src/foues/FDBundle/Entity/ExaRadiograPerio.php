<?php

namespace foues\FDBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ExaRadiograPerio
 *
 * @ORM\Table(name="exa_radiogra_perio", uniqueConstraints={@ORM\UniqueConstraint(name="exa_radiogra_perio_pk", columns={"id_radiografico"})}, indexes={@ORM\Index(name="fk_exa_radi_contiene__f_period_", columns={"id_f_perio"})})
 * @ORM\Entity
 */
class ExaRadiograPerio
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_radiografico", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="exa_radiogra_perio_id_radiografico_seq", allocationSize=1, initialValue=1)
     */
    private $idRadiografico;

    /**
     * @var string
     *
     * @ORM\Column(name="ima_en_lig_perio", type="string", length=255, nullable=true)
     */
    private $imaEnLigPerio;

    /**
     * @var string
     *
     * @ORM\Column(name="ima_en_lig_dura", type="string", length=255, nullable=true)
     */
    private $imaEnLigDura;

    /**
     * @var string
     *
     * @ORM\Column(name="ima_poh", type="string", length=255, nullable=true)
     */
    private $imaPoh;

    /**
     * @var string
     *
     * @ORM\Column(name="ima_pov", type="string", length=255, nullable=true)
     */
    private $imaPov;

    /**
     * @var string
     *
     * @ORM\Column(name="forma_int_cre", type="string", length=50, nullable=true)
     */
    private $formaIntCre;

    /**
     * @var string
     *
     * @ORM\Column(name="extension", type="string", length=40, nullable=true)
     */
    private $extension;

    /**
     * @var string
     *
     * @ORM\Column(name="alteracion_tra", type="string", length=50, nullable=true)
     */
    private $alteracionTra;

    /**
     * @var string
     *
     * @ORM\Column(name="espesor_ce", type="string", length=20, nullable=true)
     */
    private $espesorCe;

    /**
     * @var string
     *
     * @ORM\Column(name="long_for_dis", type="string", length=60, nullable=true)
     */
    private $longForDis;

    /**
     * @var string
     *
     * @ORM\Column(name="proporcion_co", type="string", length=60, nullable=true)
     */
    private $proporcionCo;

    /**
     * @var string
     *
     * @ORM\Column(name="otros", type="string", length=100, nullable=true)
     */
    private $otros;

    /**
     * @var \FPeriodoncia
     *
     * @ORM\ManyToOne(targetEntity="FPeriodoncia")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_f_perio", referencedColumnName="id_f_perio")
     * })
     */
    private $idFPerio;



    /**
     * Get idRadiografico
     *
     * @return integer 
     */
    public function getIdRadiografico()
    {
        return $this->idRadiografico;
    }

    /**
     * Set imaEnLigPerio
     *
     * @param string $imaEnLigPerio
     * @return ExaRadiograPerio
     */
    public function setImaEnLigPerio($imaEnLigPerio)
    {
        $this->imaEnLigPerio = $imaEnLigPerio;

        return $this;
    }

    /**
     * Get imaEnLigPerio
     *
     * @return string 
     */
    public function getImaEnLigPerio()
    {
        return $this->imaEnLigPerio;
    }

    /**
     * Set imaEnLigDura
     *
     * @param string $imaEnLigDura
     * @return ExaRadiograPerio
     */
    public function setImaEnLigDura($imaEnLigDura)
    {
        $this->imaEnLigDura = $imaEnLigDura;

        return $this;
    }

    /**
     * Get imaEnLigDura
     *
     * @return string 
     */
    public function getImaEnLigDura()
    {
        return $this->imaEnLigDura;
    }

    /**
     * Set imaPoh
     *
     * @param string $imaPoh
     * @return ExaRadiograPerio
     */
    public function setImaPoh($imaPoh)
    {
        $this->imaPoh = $imaPoh;

        return $this;
    }

    /**
     * Get imaPoh
     *
     * @return string 
     */
    public function getImaPoh()
    {
        return $this->imaPoh;
    }

    /**
     * Set imaPov
     *
     * @param string $imaPov
     * @return ExaRadiograPerio
     */
    public function setImaPov($imaPov)
    {
        $this->imaPov = $imaPov;

        return $this;
    }

    /**
     * Get imaPov
     *
     * @return string 
     */
    public function getImaPov()
    {
        return $this->imaPov;
    }

    /**
     * Set formaIntCre
     *
     * @param string $formaIntCre
     * @return ExaRadiograPerio
     */
    public function setFormaIntCre($formaIntCre)
    {
        $this->formaIntCre = $formaIntCre;

        return $this;
    }

    /**
     * Get formaIntCre
     *
     * @return string 
     */
    public function getFormaIntCre()
    {
        return $this->formaIntCre;
    }

    /**
     * Set extension
     *
     * @param string $extension
     * @return ExaRadiograPerio
     */
    public function setExtension($extension)
    {
        $this->extension = $extension;

        return $this;
    }

    /**
     * Get extension
     *
     * @return string 
     */
    public function getExtension()
    {
        return $this->extension;
    }

    /**
     * Set alteracionTra
     *
     * @param string $alteracionTra
     * @return ExaRadiograPerio
     */
    public function setAlteracionTra($alteracionTra)
    {
        $this->alteracionTra = $alteracionTra;

        return $this;
    }

    /**
     * Get alteracionTra
     *
     * @return string 
     */
    public function getAlteracionTra()
    {
        return $this->alteracionTra;
    }

    /**
     * Set espesorCe
     *
     * @param string $espesorCe
     * @return ExaRadiograPerio
     */
    public function setEspesorCe($espesorCe)
    {
        $this->espesorCe = $espesorCe;

        return $this;
    }

    /**
     * Get espesorCe
     *
     * @return string 
     */
    public function getEspesorCe()
    {
        return $this->espesorCe;
    }

    /**
     * Set longForDis
     *
     * @param string $longForDis
     * @return ExaRadiograPerio
     */
    public function setLongForDis($longForDis)
    {
        $this->longForDis = $longForDis;

        return $this;
    }

    /**
     * Get longForDis
     *
     * @return string 
     */
    public function getLongForDis()
    {
        return $this->longForDis;
    }

    /**
     * Set proporcionCo
     *
     * @param string $proporcionCo
     * @return ExaRadiograPerio
     */
    public function setProporcionCo($proporcionCo)
    {
        $this->proporcionCo = $proporcionCo;

        return $this;
    }

    /**
     * Get proporcionCo
     *
     * @return string 
     */
    public function getProporcionCo()
    {
        return $this->proporcionCo;
    }

    /**
     * Set otros
     *
     * @param string $otros
     * @return ExaRadiograPerio
     */
    public function setOtros($otros)
    {
        $this->otros = $otros;

        return $this;
    }

    /**
     * Get otros
     *
     * @return string 
     */
    public function getOtros()
    {
        return $this->otros;
    }

    /**
     * Set idFPerio
     *
     * @param \foues\FDBundle\Entity\FPeriodoncia $idFPerio
     * @return ExaRadiograPerio
     */
    public function setIdFPerio(\foues\FDBundle\Entity\FPeriodoncia $idFPerio = null)
    {
        $this->idFPerio = $idFPerio;

        return $this;
    }

    /**
     * Get idFPerio
     *
     * @return \foues\FDBundle\Entity\FPeriodoncia 
     */
    public function getIdFPerio()
    {
        return $this->idFPerio;
    }
}
