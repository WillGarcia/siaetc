<?php

namespace foues\FDBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ExRad3
 *
 * @ORM\Table(name="ex_rad_3", uniqueConstraints={@ORM\UniqueConstraint(name="ex_rad_3_pk", columns={"id_exr_3"})}, indexes={@ORM\Index(name="relationship_96_fk", columns={"id_f_dx"})})
 * @ORM\Entity
 */
class ExRad3
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_exr_3", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="ex_rad_3_id_exr_3_seq", allocationSize=1, initialValue=1)
     */
    private $idExr3;

    /**
     * @var string
     *
     * @ORM\Column(name="m3_1_m", type="string", length=2, nullable=true)
     */
    private $m31M;

    /**
     * @var string
     *
     * @ORM\Column(name="d3_1_d", type="string", length=2, nullable=true)
     */
    private $d31D;

    /**
     * @var string
     *
     * @ORM\Column(name="m3_2_m", type="string", length=2, nullable=true)
     */
    private $m32M;

    /**
     * @var string
     *
     * @ORM\Column(name="d3_2_d", type="string", length=2, nullable=true)
     */
    private $d32D;

    /**
     * @var string
     *
     * @ORM\Column(name="m3_3_m", type="string", length=2, nullable=true)
     */
    private $m33M;

    /**
     * @var string
     *
     * @ORM\Column(name="d3_3_d", type="string", length=2, nullable=true)
     */
    private $d33D;

    /**
     * @var string
     *
     * @ORM\Column(name="m3_4_m", type="string", length=2, nullable=true)
     */
    private $m34M;

    /**
     * @var string
     *
     * @ORM\Column(name="d3_4_d", type="string", length=2, nullable=true)
     */
    private $d34D;

    /**
     * @var string
     *
     * @ORM\Column(name="m3_5_m", type="string", length=2, nullable=true)
     */
    private $m35M;

    /**
     * @var string
     *
     * @ORM\Column(name="d3_5_d", type="string", length=2, nullable=true)
     */
    private $d35D;

    /**
     * @var string
     *
     * @ORM\Column(name="m3_6_m", type="string", length=2, nullable=true)
     */
    private $m36M;

    /**
     * @var string
     *
     * @ORM\Column(name="d3_6_d", type="string", length=2, nullable=true)
     */
    private $d36D;

    /**
     * @var string
     *
     * @ORM\Column(name="m3_7_m", type="string", length=2, nullable=true)
     */
    private $m37M;

    /**
     * @var string
     *
     * @ORM\Column(name="d3_7_d", type="string", length=2, nullable=true)
     */
    private $d37D;

    /**
     * @var string
     *
     * @ORM\Column(name="m3_8_m", type="string", length=2, nullable=true)
     */
    private $m38M;

    /**
     * @var string
     *
     * @ORM\Column(name="d3_8_d", type="string", length=2, nullable=true)
     */
    private $d38D;

    /**
     * @var \FDiagnostico
     *
     * @ORM\ManyToOne(targetEntity="FDiagnostico")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_f_dx", referencedColumnName="id_f_dx")
     * })
     */
    private $idFDx;



    /**
     * Get idExr3
     *
     * @return integer 
     */
    public function getIdExr3()
    {
        return $this->idExr3;
    }

    /**
     * Set m31M
     *
     * @param string $m31M
     * @return ExRad3
     */
    public function setM31M($m31M)
    {
        $this->m31M = $m31M;

        return $this;
    }

    /**
     * Get m31M
     *
     * @return string 
     */
    public function getM31M()
    {
        return $this->m31M;
    }

    /**
     * Set d31D
     *
     * @param string $d31D
     * @return ExRad3
     */
    public function setD31D($d31D)
    {
        $this->d31D = $d31D;

        return $this;
    }

    /**
     * Get d31D
     *
     * @return string 
     */
    public function getD31D()
    {
        return $this->d31D;
    }

    /**
     * Set m32M
     *
     * @param string $m32M
     * @return ExRad3
     */
    public function setM32M($m32M)
    {
        $this->m32M = $m32M;

        return $this;
    }

    /**
     * Get m32M
     *
     * @return string 
     */
    public function getM32M()
    {
        return $this->m32M;
    }

    /**
     * Set d32D
     *
     * @param string $d32D
     * @return ExRad3
     */
    public function setD32D($d32D)
    {
        $this->d32D = $d32D;

        return $this;
    }

    /**
     * Get d32D
     *
     * @return string 
     */
    public function getD32D()
    {
        return $this->d32D;
    }

    /**
     * Set m33M
     *
     * @param string $m33M
     * @return ExRad3
     */
    public function setM33M($m33M)
    {
        $this->m33M = $m33M;

        return $this;
    }

    /**
     * Get m33M
     *
     * @return string 
     */
    public function getM33M()
    {
        return $this->m33M;
    }

    /**
     * Set d33D
     *
     * @param string $d33D
     * @return ExRad3
     */
    public function setD33D($d33D)
    {
        $this->d33D = $d33D;

        return $this;
    }

    /**
     * Get d33D
     *
     * @return string 
     */
    public function getD33D()
    {
        return $this->d33D;
    }

    /**
     * Set m34M
     *
     * @param string $m34M
     * @return ExRad3
     */
    public function setM34M($m34M)
    {
        $this->m34M = $m34M;

        return $this;
    }

    /**
     * Get m34M
     *
     * @return string 
     */
    public function getM34M()
    {
        return $this->m34M;
    }

    /**
     * Set d34D
     *
     * @param string $d34D
     * @return ExRad3
     */
    public function setD34D($d34D)
    {
        $this->d34D = $d34D;

        return $this;
    }

    /**
     * Get d34D
     *
     * @return string 
     */
    public function getD34D()
    {
        return $this->d34D;
    }

    /**
     * Set m35M
     *
     * @param string $m35M
     * @return ExRad3
     */
    public function setM35M($m35M)
    {
        $this->m35M = $m35M;

        return $this;
    }

    /**
     * Get m35M
     *
     * @return string 
     */
    public function getM35M()
    {
        return $this->m35M;
    }

    /**
     * Set d35D
     *
     * @param string $d35D
     * @return ExRad3
     */
    public function setD35D($d35D)
    {
        $this->d35D = $d35D;

        return $this;
    }

    /**
     * Get d35D
     *
     * @return string 
     */
    public function getD35D()
    {
        return $this->d35D;
    }

    /**
     * Set m36M
     *
     * @param string $m36M
     * @return ExRad3
     */
    public function setM36M($m36M)
    {
        $this->m36M = $m36M;

        return $this;
    }

    /**
     * Get m36M
     *
     * @return string 
     */
    public function getM36M()
    {
        return $this->m36M;
    }

    /**
     * Set d36D
     *
     * @param string $d36D
     * @return ExRad3
     */
    public function setD36D($d36D)
    {
        $this->d36D = $d36D;

        return $this;
    }

    /**
     * Get d36D
     *
     * @return string 
     */
    public function getD36D()
    {
        return $this->d36D;
    }

    /**
     * Set m37M
     *
     * @param string $m37M
     * @return ExRad3
     */
    public function setM37M($m37M)
    {
        $this->m37M = $m37M;

        return $this;
    }

    /**
     * Get m37M
     *
     * @return string 
     */
    public function getM37M()
    {
        return $this->m37M;
    }

    /**
     * Set d37D
     *
     * @param string $d37D
     * @return ExRad3
     */
    public function setD37D($d37D)
    {
        $this->d37D = $d37D;

        return $this;
    }

    /**
     * Get d37D
     *
     * @return string 
     */
    public function getD37D()
    {
        return $this->d37D;
    }

    /**
     * Set m38M
     *
     * @param string $m38M
     * @return ExRad3
     */
    public function setM38M($m38M)
    {
        $this->m38M = $m38M;

        return $this;
    }

    /**
     * Get m38M
     *
     * @return string 
     */
    public function getM38M()
    {
        return $this->m38M;
    }

    /**
     * Set d38D
     *
     * @param string $d38D
     * @return ExRad3
     */
    public function setD38D($d38D)
    {
        $this->d38D = $d38D;

        return $this;
    }

    /**
     * Get d38D
     *
     * @return string 
     */
    public function getD38D()
    {
        return $this->d38D;
    }

    /**
     * Set idFDx
     *
     * @param \foues\FDBundle\Entity\FDiagnostico $idFDx
     * @return ExRad3
     */
    public function setIdFDx(\foues\FDBundle\Entity\FDiagnostico $idFDx = null)
    {
        $this->idFDx = $idFDx;

        return $this;
    }

    /**
     * Get idFDx
     *
     * @return \foues\FDBundle\Entity\FDiagnostico 
     */
    public function getIdFDx()
    {
        return $this->idFDx;
    }
}
