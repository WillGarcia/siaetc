<?php

namespace foues\FDBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ConsideraRxDx
 *
 * @ORM\Table(name="considera_rx_dx", uniqueConstraints={@ORM\UniqueConstraint(name="considera_rx_dx_pk", columns={"id_consid"})}, indexes={@ORM\Index(name="fk_consider_toma_eva_pulp_fk", columns={"id_eva_pul"})})
 * @ORM\Entity
 */
class ConsideraRxDx
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_consid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="considera_rx_dx_id_consid_seq", allocationSize=1, initialValue=1)
     */
    private $idConsid;

    /**
     * @var string
     *
     * @ORM\Column(name="les_periapical", type="string", length=2, nullable=true)
     */
    private $lesPeriapical;

    /**
     * @var string
     *
     * @ORM\Column(name="rela_carie_pul", type="string", length=2, nullable=true)
     */
    private $relaCariePul;

    /**
     * @var string
     *
     * @ORM\Column(name="cor_raiz1_2", type="string", length=2, nullable=true)
     */
    private $corRaiz12;

    /**
     * @var string
     *
     * @ORM\Column(name="cor_raiz1_1", type="string", length=2, nullable=true)
     */
    private $corRaiz11;

    /**
     * @var string
     *
     * @ORM\Column(name="cor_raiz2_1", type="string", length=2, nullable=true)
     */
    private $corRaiz21;

    /**
     * @var string
     *
     * @ORM\Column(name="tcr", type="string", length=2, nullable=true)
     */
    private $tcr;

    /**
     * @var \EvaPulparDx
     *
     * @ORM\ManyToOne(targetEntity="EvaPulparDx")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_eva_pul", referencedColumnName="id_eva_pul")
     * })
     */
    private $idEvaPul;



    /**
     * Get idConsid
     *
     * @return integer 
     */
    public function getIdConsid()
    {
        return $this->idConsid;
    }

    /**
     * Set lesPeriapical
     *
     * @param string $lesPeriapical
     * @return ConsideraRxDx
     */
    public function setLesPeriapical($lesPeriapical)
    {
        $this->lesPeriapical = $lesPeriapical;

        return $this;
    }

    /**
     * Get lesPeriapical
     *
     * @return string 
     */
    public function getLesPeriapical()
    {
        return $this->lesPeriapical;
    }

    /**
     * Set relaCariePul
     *
     * @param string $relaCariePul
     * @return ConsideraRxDx
     */
    public function setRelaCariePul($relaCariePul)
    {
        $this->relaCariePul = $relaCariePul;

        return $this;
    }

    /**
     * Get relaCariePul
     *
     * @return string 
     */
    public function getRelaCariePul()
    {
        return $this->relaCariePul;
    }

    /**
     * Set corRaiz12
     *
     * @param string $corRaiz12
     * @return ConsideraRxDx
     */
    public function setCorRaiz12($corRaiz12)
    {
        $this->corRaiz12 = $corRaiz12;

        return $this;
    }

    /**
     * Get corRaiz12
     *
     * @return string 
     */
    public function getCorRaiz12()
    {
        return $this->corRaiz12;
    }

    /**
     * Set corRaiz11
     *
     * @param string $corRaiz11
     * @return ConsideraRxDx
     */
    public function setCorRaiz11($corRaiz11)
    {
        $this->corRaiz11 = $corRaiz11;

        return $this;
    }

    /**
     * Get corRaiz11
     *
     * @return string 
     */
    public function getCorRaiz11()
    {
        return $this->corRaiz11;
    }

    /**
     * Set corRaiz21
     *
     * @param string $corRaiz21
     * @return ConsideraRxDx
     */
    public function setCorRaiz21($corRaiz21)
    {
        $this->corRaiz21 = $corRaiz21;

        return $this;
    }

    /**
     * Get corRaiz21
     *
     * @return string 
     */
    public function getCorRaiz21()
    {
        return $this->corRaiz21;
    }

    /**
     * Set tcr
     *
     * @param string $tcr
     * @return ConsideraRxDx
     */
    public function setTcr($tcr)
    {
        $this->tcr = $tcr;

        return $this;
    }

    /**
     * Get tcr
     *
     * @return string 
     */
    public function getTcr()
    {
        return $this->tcr;
    }

    /**
     * Set idEvaPul
     *
     * @param \foues\FDBundle\Entity\EvaPulparDx $idEvaPul
     * @return ConsideraRxDx
     */
    public function setIdEvaPul(\foues\FDBundle\Entity\EvaPulparDx $idEvaPul = null)
    {
        $this->idEvaPul = $idEvaPul;

        return $this;
    }

    /**
     * Get idEvaPul
     *
     * @return \foues\FDBundle\Entity\EvaPulparDx 
     */
    public function getIdEvaPul()
    {
        return $this->idEvaPul;
    }
}
