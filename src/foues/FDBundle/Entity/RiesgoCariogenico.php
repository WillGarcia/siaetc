<?php

namespace foues\FDBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * RiesgoCariogenico
 *
 * @ORM\Table(name="riesgo_cariogenico", uniqueConstraints={@ORM\UniqueConstraint(name="riesgo_cariogenico_pk", columns={"id_rie_ca"})}, indexes={@ORM\Index(name="fk_riesgo_c_calcula_f_diagno_fk", columns={"id_f_dx"})})
 * @ORM\Entity
 */
class RiesgoCariogenico
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_rie_ca", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="riesgo_cariogenico_id_rie_ca_seq", allocationSize=1, initialValue=1)
     */
    private $idRieCa;

    /**
     * @var string
     *
     * @ORM\Column(name="cariado", type="decimal", precision=2, scale=0, nullable=true)
     */
    private $cariado;

    /**
     * @var string
     *
     * @ORM\Column(name="perdido", type="decimal", precision=2, scale=0, nullable=true)
     */
    private $perdido;

    /**
     * @var string
     *
     * @ORM\Column(name="opturado", type="decimal", precision=2, scale=0, nullable=true)
     */
    private $opturado;

    /**
     * @var string
     *
     * @ORM\Column(name="total_cpo", type="decimal", precision=2, scale=0, nullable=true)
     */
    private $totalCpo;

    /**
     * @var float
     *
     * @ORM\Column(name="placa_bac", type="float", precision=10, scale=0, nullable=true)
     */
    private $placaBac;

    /**
     * @var \FDiagnostico
     *
     * @ORM\ManyToOne(targetEntity="FDiagnostico")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_f_dx", referencedColumnName="id_f_dx")
     * })
     */
    private $idFDx;



    /**
     * Get idRieCa
     *
     * @return integer 
     */
    public function getIdRieCa()
    {
        return $this->idRieCa;
    }

    /**
     * Set cariado
     *
     * @param string $cariado
     * @return RiesgoCariogenico
     */
    public function setCariado($cariado)
    {
        $this->cariado = $cariado;

        return $this;
    }

    /**
     * Get cariado
     *
     * @return string 
     */
    public function getCariado()
    {
        return $this->cariado;
    }

    /**
     * Set perdido
     *
     * @param string $perdido
     * @return RiesgoCariogenico
     */
    public function setPerdido($perdido)
    {
        $this->perdido = $perdido;

        return $this;
    }

    /**
     * Get perdido
     *
     * @return string 
     */
    public function getPerdido()
    {
        return $this->perdido;
    }

    /**
     * Set opturado
     *
     * @param string $opturado
     * @return RiesgoCariogenico
     */
    public function setOpturado($opturado)
    {
        $this->opturado = $opturado;

        return $this;
    }

    /**
     * Get opturado
     *
     * @return string 
     */
    public function getOpturado()
    {
        return $this->opturado;
    }

    /**
     * Set totalCpo
     *
     * @param string $totalCpo
     * @return RiesgoCariogenico
     */
    public function setTotalCpo($totalCpo)
    {
        $this->totalCpo = $totalCpo;

        return $this;
    }

    /**
     * Get totalCpo
     *
     * @return string 
     */
    public function getTotalCpo()
    {
        return $this->totalCpo;
    }

    /**
     * Set placaBac
     *
     * @param float $placaBac
     * @return RiesgoCariogenico
     */
    public function setPlacaBac($placaBac)
    {
        $this->placaBac = $placaBac;

        return $this;
    }

    /**
     * Get placaBac
     *
     * @return float 
     */
    public function getPlacaBac()
    {
        return $this->placaBac;
    }

    /**
     * Set idFDx
     *
     * @param \foues\FDBundle\Entity\FDiagnostico $idFDx
     * @return RiesgoCariogenico
     */
    public function setIdFDx(\foues\FDBundle\Entity\FDiagnostico $idFDx = null)
    {
        $this->idFDx = $idFDx;

        return $this;
    }

    /**
     * Get idFDx
     *
     * @return \foues\FDBundle\Entity\FDiagnostico 
     */
    public function getIdFDx()
    {
        return $this->idFDx;
    }
}
