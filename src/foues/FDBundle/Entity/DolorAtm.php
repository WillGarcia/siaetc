<?php

namespace foues\FDBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * DolorAtm
 *
 * @ORM\Table(name="dolor_atm", uniqueConstraints={@ORM\UniqueConstraint(name="dolor_atm_pk", columns={"id_atm"})}, indexes={@ORM\Index(name="relationship_90_fk", columns={"id_f_dx"})})
 * @ORM\Entity
 */
class DolorAtm
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_atm", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="dolor_atm_id_atm_seq", allocationSize=1, initialValue=1)
     */
    private $idAtm;

    /**
     * @var string
     *
     * @ORM\Column(name="al_masticar", type="string", length=2, nullable=true)
     */
    private $alMasticar;

    /**
     * @var string
     *
     * @ORM\Column(name="al_abrir_bo", type="string", length=2, nullable=true)
     */
    private $alAbrirBo;

    /**
     * @var string
     *
     * @ORM\Column(name="al_bostezar", type="string", length=2, nullable=true)
     */
    private $alBostezar;

    /**
     * @var string
     *
     * @ORM\Column(name="al_hablar", type="string", length=2, nullable=true)
     */
    private $alHablar;

    /**
     * @var string
     *
     * @ORM\Column(name="al_girar_cab", type="string", length=2, nullable=true)
     */
    private $alGirarCab;

    /**
     * @var string
     *
     * @ORM\Column(name="al_tragar", type="string", length=2, nullable=true)
     */
    private $alTragar;

    /**
     * @var string
     *
     * @ORM\Column(name="otra_alterna", type="string", length=2, nullable=true)
     */
    private $otraAlterna;

    /**
     * @var \FDiagnostico
     *
     * @ORM\ManyToOne(targetEntity="FDiagnostico")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_f_dx", referencedColumnName="id_f_dx")
     * })
     */
    private $idFDx;



    /**
     * Get idAtm
     *
     * @return integer 
     */
    public function getIdAtm()
    {
        return $this->idAtm;
    }

    /**
     * Set alMasticar
     *
     * @param string $alMasticar
     * @return DolorAtm
     */
    public function setAlMasticar($alMasticar)
    {
        $this->alMasticar = $alMasticar;

        return $this;
    }

    /**
     * Get alMasticar
     *
     * @return string 
     */
    public function getAlMasticar()
    {
        return $this->alMasticar;
    }

    /**
     * Set alAbrirBo
     *
     * @param string $alAbrirBo
     * @return DolorAtm
     */
    public function setAlAbrirBo($alAbrirBo)
    {
        $this->alAbrirBo = $alAbrirBo;

        return $this;
    }

    /**
     * Get alAbrirBo
     *
     * @return string 
     */
    public function getAlAbrirBo()
    {
        return $this->alAbrirBo;
    }

    /**
     * Set alBostezar
     *
     * @param string $alBostezar
     * @return DolorAtm
     */
    public function setAlBostezar($alBostezar)
    {
        $this->alBostezar = $alBostezar;

        return $this;
    }

    /**
     * Get alBostezar
     *
     * @return string 
     */
    public function getAlBostezar()
    {
        return $this->alBostezar;
    }

    /**
     * Set alHablar
     *
     * @param string $alHablar
     * @return DolorAtm
     */
    public function setAlHablar($alHablar)
    {
        $this->alHablar = $alHablar;

        return $this;
    }

    /**
     * Get alHablar
     *
     * @return string 
     */
    public function getAlHablar()
    {
        return $this->alHablar;
    }

    /**
     * Set alGirarCab
     *
     * @param string $alGirarCab
     * @return DolorAtm
     */
    public function setAlGirarCab($alGirarCab)
    {
        $this->alGirarCab = $alGirarCab;

        return $this;
    }

    /**
     * Get alGirarCab
     *
     * @return string 
     */
    public function getAlGirarCab()
    {
        return $this->alGirarCab;
    }

    /**
     * Set alTragar
     *
     * @param string $alTragar
     * @return DolorAtm
     */
    public function setAlTragar($alTragar)
    {
        $this->alTragar = $alTragar;

        return $this;
    }

    /**
     * Get alTragar
     *
     * @return string 
     */
    public function getAlTragar()
    {
        return $this->alTragar;
    }

    /**
     * Set otraAlterna
     *
     * @param string $otraAlterna
     * @return DolorAtm
     */
    public function setOtraAlterna($otraAlterna)
    {
        $this->otraAlterna = $otraAlterna;

        return $this;
    }

    /**
     * Get otraAlterna
     *
     * @return string 
     */
    public function getOtraAlterna()
    {
        return $this->otraAlterna;
    }

    /**
     * Set idFDx
     *
     * @param \foues\FDBundle\Entity\FDiagnostico $idFDx
     * @return DolorAtm
     */
    public function setIdFDx(\foues\FDBundle\Entity\FDiagnostico $idFDx = null)
    {
        $this->idFDx = $idFDx;

        return $this;
    }

    /**
     * Get idFDx
     *
     * @return \foues\FDBundle\Entity\FDiagnostico 
     */
    public function getIdFDx()
    {
        return $this->idFDx;
    }
}
