<?php

namespace foues\FDBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Genero
 *
 * @ORM\Table(name="genero", uniqueConstraints={@ORM\UniqueConstraint(name="genero_pk", columns={"id_genero"})})
 * @ORM\Entity
 */
class Genero
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_genero", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="genero_id_genero_seq", allocationSize=1, initialValue=1)
     */
    private $idGenero;

    /**
     * @var string
     *
     * @ORM\Column(name="nom_genero", type="string", length=10, nullable=false)
     */
    private $nomGenero;



    /**
     * Get idGenero
     *
     * @return integer 
     */
    public function getIdGenero()
    {
        return $this->idGenero;
    }

    /**
     * Set nomGenero
     *
     * @param string $nomGenero
     * @return Genero
     */
    public function setNomGenero($nomGenero)
    {
        $this->nomGenero = $nomGenero;

        return $this;
    }

    /**
     * Get nomGenero
     *
     * @return string 
     */
    public function getNomGenero()
    {
        return $this->nomGenero;
    }

    public function __toString(){
        return $this->getNomGenero();
    }
}
