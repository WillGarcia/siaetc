<?php

namespace foues\FDBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * FRestaurativa
 *
 * @ORM\Table(name="f_restaurativa", uniqueConstraints={@ORM\UniqueConstraint(name="f_restaurativa_pk", columns={"id_f_resta"})}, indexes={@ORM\Index(name="fk_f_restau_es_parte__ficha_cl_", columns={"id_ficha"})})
 * @ORM\Entity
 */
class FRestaurativa
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_f_resta", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="f_restaurativa_id_f_resta_seq", allocationSize=1, initialValue=1)
     */
    private $idFResta;

    /**
     * @var integer
     *
     * @ORM\Column(name="area_ficha", type="integer", nullable=false)
     */
    private $areaFicha;

    /**
     * @var boolean
     *
     * @ORM\Column(name="ppr", type="boolean", nullable=true)
     */
    private $ppr;

    /**
     * @var boolean
     *
     * @ORM\Column(name="ppf", type="boolean", nullable=true)
     */
    private $ppf;

    /**
     * @var string
     *
     * @ORM\Column(name="his_medica", type="string", length=100, nullable=true)
     */
    private $hisMedica;

    /**
     * @var string
     *
     * @ORM\Column(name="exa_clinico_in_ex", type="string", length=100, nullable=true)
     */
    private $exaClinicoInEx;

    /**
     * @var string
     *
     * @ORM\Column(name="plan_trat_cr", type="string", length=1000, nullable=true)
     */
    private $planTratCr;

    /**
     * @var string
     *
     * @ORM\Column(name="cronograma", type="string", length=1000, nullable=true)
     */
    private $cronograma;

    /**
     * @var boolean
     *
     * @ORM\Column(name="alta", type="boolean", nullable=true)
     */
    private $alta;

    /**
     * @var \FichaClinica
     *
     * @ORM\ManyToOne(targetEntity="FichaClinica")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_ficha", referencedColumnName="id_ficha")
     * })
     */
    private $idFicha;



    /**
     * Get idFResta
     *
     * @return integer 
     */
    public function getIdFResta()
    {
        return $this->idFResta;
    }

    /**
     * Set areaFicha
     *
     * @param integer $areaFicha
     * @return FRestaurativa
     */
    public function setAreaFicha($areaFicha)
    {
        $this->areaFicha = $areaFicha;

        return $this;
    }

    /**
     * Get areaFicha
     *
     * @return integer 
     */
    public function getAreaFicha()
    {
        return $this->areaFicha;
    }

    /**
     * Set ppr
     *
     * @param boolean $ppr
     * @return FRestaurativa
     */
    public function setPpr($ppr)
    {
        $this->ppr = $ppr;

        return $this;
    }

    /**
     * Get ppr
     *
     * @return boolean 
     */
    public function getPpr()
    {
        return $this->ppr;
    }

    /**
     * Set ppf
     *
     * @param boolean $ppf
     * @return FRestaurativa
     */
    public function setPpf($ppf)
    {
        $this->ppf = $ppf;

        return $this;
    }

    /**
     * Get ppf
     *
     * @return boolean 
     */
    public function getPpf()
    {
        return $this->ppf;
    }

    /**
     * Set hisMedica
     *
     * @param string $hisMedica
     * @return FRestaurativa
     */
    public function setHisMedica($hisMedica)
    {
        $this->hisMedica = $hisMedica;

        return $this;
    }

    /**
     * Get hisMedica
     *
     * @return string 
     */
    public function getHisMedica()
    {
        return $this->hisMedica;
    }

    /**
     * Set exaClinicoInEx
     *
     * @param string $exaClinicoInEx
     * @return FRestaurativa
     */
    public function setExaClinicoInEx($exaClinicoInEx)
    {
        $this->exaClinicoInEx = $exaClinicoInEx;

        return $this;
    }

    /**
     * Get exaClinicoInEx
     *
     * @return string 
     */
    public function getExaClinicoInEx()
    {
        return $this->exaClinicoInEx;
    }

    /**
     * Set planTratCr
     *
     * @param string $planTratCr
     * @return FRestaurativa
     */
    public function setPlanTratCr($planTratCr)
    {
        $this->planTratCr = $planTratCr;

        return $this;
    }

    /**
     * Get planTratCr
     *
     * @return string 
     */
    public function getPlanTratCr()
    {
        return $this->planTratCr;
    }

    /**
     * Set cronograma
     *
     * @param string $cronograma
     * @return FRestaurativa
     */
    public function setCronograma($cronograma)
    {
        $this->cronograma = $cronograma;

        return $this;
    }

    /**
     * Get cronograma
     *
     * @return string 
     */
    public function getCronograma()
    {
        return $this->cronograma;
    }

    /**
     * Set alta
     *
     * @param boolean $alta
     * @return FRestaurativa
     */
    public function setAlta($alta)
    {
        $this->alta = $alta;

        return $this;
    }

    /**
     * Get alta
     *
     * @return boolean 
     */
    public function getAlta()
    {
        return $this->alta;
    }

    /**
     * Set idFicha
     *
     * @param \foues\FDBundle\Entity\FichaClinica $idFicha
     * @return FRestaurativa
     */
    public function setIdFicha(\foues\FDBundle\Entity\FichaClinica $idFicha = null)
    {
        $this->idFicha = $idFicha;

        return $this;
    }

    /**
     * Get idFicha
     *
     * @return \foues\FDBundle\Entity\FichaClinica 
     */
    public function getIdFicha()
    {
        return $this->idFicha;
    }
}
