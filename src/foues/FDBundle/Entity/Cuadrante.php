<?php

namespace foues\FDBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Cuadrante
 *
 * @ORM\Table(name="cuadrante")
 * @ORM\Entity
 */
class Cuadrante
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_cuadrant", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="cuadrante_id_cuadrant_seq", allocationSize=1, initialValue=1)
     */
    private $idCuadrant;

    /**
     * @var string
     *
     * @ORM\Column(name="cuadrante", type="string", length=14, nullable=true)
     */
    private $cuadrante;



    /**
     * Get idCuadrant
     *
     * @return integer 
     */
    public function getIdCuadrant()
    {
        return $this->idCuadrant;
    }

    /**
     * Set cuadrante
     *
     * @param string $cuadrante
     * @return Cuadrante
     */
    public function setCuadrante($cuadrante)
    {
        $this->cuadrante = $cuadrante;

        return $this;
    }

    /**
     * Get cuadrante
     *
     * @return string 
     */
    public function getCuadrante()
    {
        return $this->cuadrante;
    }
}
