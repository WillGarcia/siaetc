<?php

namespace foues\FDBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Paciente
 *
 * @ORM\Table(name="paciente", uniqueConstraints={@ORM\UniqueConstraint(name="paciente_pk", columns={"id_pac"})}, indexes={@ORM\Index(name="fk_paciente_pertenece_genero_fk", columns={"id_genero"}), @ORM\Index(name="fk_paciente_poseen_un_estado_c_", columns={"id_estado_c"})})
 * @ORM\Entity
 */
class Paciente
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_pac", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="paciente_id_pac_seq", allocationSize=1, initialValue=1)
     */
    private $idPac;

    /**
     * @var string
     *
     * @ORM\Column(name="dui", type="string", length=10, nullable=false)
     */
    private $dui;

    /**
     * @var string
     *
     * @ORM\Column(name="num_expediente", type="string", length=9, nullable=false)
     */
    private $numExpediente;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha_apertura", type="date", nullable=true)
     */
    private $fechaApertura;

    /**
     * @var string
     *
     * @ORM\Column(name="nom1_pac", type="string", length=15, nullable=false)
     */
    private $nom1Pac;

    /**
     * @var string
     *
     * @ORM\Column(name="nom2_pac", type="string", length=15, nullable=true)
     */
    private $nom2Pac;

    /**
     * @var string
     *
     * @ORM\Column(name="nom3_pac", type="string", length=15, nullable=true)
     */
    private $nom3Pac;

    /**
     * @var string
     *
     * @ORM\Column(name="ape1_pac", type="string", length=15, nullable=false)
     */
    private $ape1Pac;

    /**
     * @var string
     *
     * @ORM\Column(name="ape2_pac", type="string", length=15, nullable=true)
     */
    private $ape2Pac;

    /**
     * @var string
     *
     * @ORM\Column(name="ape3_pac", type="string", length=15, nullable=true)
     */
    private $ape3Pac;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha_nac", type="date", nullable=false)
     */
    private $fechaNac;

    /**
     * @var integer
     *
     * @ORM\Column(name="edad", type="integer", nullable=true)
     */
    private $edad;

    /**
     * @var integer
     *
     * @ORM\Column(name="depto_nac", type="integer", nullable=true)
     */
    private $deptoNac;

    /**
     * @var integer
     *
     * @ORM\Column(name="mun_nac", type="integer", nullable=true)
     */
    private $munNac;

    /**
     * @var integer
     *
     * @ORM\Column(name="depto_res", type="integer", nullable=true)
     */
    private $deptoRes;

    /**
     * @var integer
     *
     * @ORM\Column(name="mun_res", type="integer", nullable=true)
     */
    private $munRes;

    /**
     * @var string
     *
     * @ORM\Column(name="nivel_educ", type="string", length=60, nullable=true)
     */
    private $nivelEduc;

    /**
     * @var string
     *
     * @ORM\Column(name="domicilio", type="string", length=70, nullable=true)
     */
    private $domicilio;

    /**
     * @var string
     *
     * @ORM\Column(name="telefono_px", type="string", length=9, nullable=true)
     */
    private $telefonoPx;

    /**
     * @var string
     *
     * @ORM\Column(name="movil_px", type="string", length=9, nullable=true)
     */
    private $movilPx;

    /**
     * @var string
     *
     * @ORM\Column(name="dir_trabajo", type="string", length=70, nullable=true)
     */
    private $dirTrabajo;

    /**
     * @var string
     *
     * @ORM\Column(name="tel_trabajo", type="string", length=9, nullable=true)
     */
    private $telTrabajo;

    /**
     * @var string
     *
     * @ORM\Column(name="responsable", type="string", length=80, nullable=true)
     */
    private $responsable;

    /**
     * @var string
     *
     * @ORM\Column(name="tel_responsable", type="string", length=9, nullable=true)
     */
    private $telResponsable;

    /**
     * @var string
     *
     * @ORM\Column(name="ocupacion", type="string", length=15, nullable=true)
     */
    private $ocupacion;

    /**
     * @var boolean
     *
     * @ORM\Column(name="activo", type="boolean", nullable=true)
     */
    private $activo;

    /**
     * @var \Genero
     *
     * @ORM\ManyToOne(targetEntity="Genero")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_genero", referencedColumnName="id_genero")
     * })
     */
    private $idGenero;

    /**
     * @var \EstadoCivil
     *
     * @ORM\ManyToOne(targetEntity="EstadoCivil")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_estado_c", referencedColumnName="id_estado_c")
     * })
     */
    private $idEstadoC;



    /**
     * Get idPac
     *
     * @return integer 
     */
    public function getIdPac()
    {
        return $this->idPac;
    }

    /**
     * Set dui
     *
     * @param string $dui
     * @return Paciente
     */
    public function setDui($dui)
    {
        $this->dui = $dui;

        return $this;
    }

    /**
     * Get dui
     *
     * @return string 
     */
    public function getDui()
    {
        return $this->dui;
    }

    /**
     * Set numExpediente
     *
     * @param string $numExpediente
     * @return Paciente
     */
    public function setNumExpediente($numExpediente)
    {
        $this->numExpediente = $numExpediente;

        return $this;
    }

    /**
     * Get numExpediente
     *
     * @return string 
     */
    public function getNumExpediente()
    {
        return $this->numExpediente;
    }

    /**
     * Set fechaApertura
     *
     * @param \DateTime $fechaApertura
     * @return Paciente
     */
    public function setFechaApertura($fechaApertura)
    {
        $this->fechaApertura = $fechaApertura;

        return $this;
    }

    /**
     * Get fechaApertura
     *
     * @return \DateTime 
     */
    public function getFechaApertura()
    {
        return $this->fechaApertura;
    }

    /**
     * Set nom1Pac
     *
     * @param string $nom1Pac
     * @return Paciente
     */
    public function setNom1Pac($nom1Pac)
    {
        $this->nom1Pac = $nom1Pac;

        return $this;
    }

    /**
     * Get nom1Pac
     *
     * @return string 
     */
    public function getNom1Pac()
    {
        return $this->nom1Pac;
    }

    /**
     * Set nom2Pac
     *
     * @param string $nom2Pac
     * @return Paciente
     */
    public function setNom2Pac($nom2Pac)
    {
        $this->nom2Pac = $nom2Pac;

        return $this;
    }

    /**
     * Get nom2Pac
     *
     * @return string 
     */
    public function getNom2Pac()
    {
        return $this->nom2Pac;
    }

    /**
     * Set nom3Pac
     *
     * @param string $nom3Pac
     * @return Paciente
     */
    public function setNom3Pac($nom3Pac)
    {
        $this->nom3Pac = $nom3Pac;

        return $this;
    }

    /**
     * Get nom3Pac
     *
     * @return string 
     */
    public function getNom3Pac()
    {
        return $this->nom3Pac;
    }

    /**
     * Set ape1Pac
     *
     * @param string $ape1Pac
     * @return Paciente
     */
    public function setApe1Pac($ape1Pac)
    {
        $this->ape1Pac = $ape1Pac;

        return $this;
    }

    /**
     * Get ape1Pac
     *
     * @return string 
     */
    public function getApe1Pac()
    {
        return $this->ape1Pac;
    }

    /**
     * Set ape2Pac
     *
     * @param string $ape2Pac
     * @return Paciente
     */
    public function setApe2Pac($ape2Pac)
    {
        $this->ape2Pac = $ape2Pac;

        return $this;
    }

    /**
     * Get ape2Pac
     *
     * @return string 
     */
    public function getApe2Pac()
    {
        return $this->ape2Pac;
    }

    /**
     * Set ape3Pac
     *
     * @param string $ape3Pac
     * @return Paciente
     */
    public function setApe3Pac($ape3Pac)
    {
        $this->ape3Pac = $ape3Pac;

        return $this;
    }

    /**
     * Get ape3Pac
     *
     * @return string 
     */
    public function getApe3Pac()
    {
        return $this->ape3Pac;
    }

    /**
     * Set fechaNac
     *
     * @param \DateTime $fechaNac
     * @return Paciente
     */
    public function setFechaNac($fechaNac)
    {
        $this->fechaNac = $fechaNac;

        return $this;
    }

    /**
     * Get fechaNac
     *
     * @return \DateTime 
     */
    public function getFechaNac()
    {
        return $this->fechaNac;
    }

    /**
     * Set edad
     *
     * @param integer $edad
     * @return Paciente
     */
    public function setEdad($edad)
    {
        $this->edad = $edad;

        return $this;
    }

    /**
     * Get edad
     *
     * @return integer 
     */
    public function getEdad()
    {
        return $this->edad;
    }

    /**
     * Set deptoNac
     *
     * @param integer $deptoNac
     * @return Paciente
     */
    public function setDeptoNac($deptoNac)
    {
        $this->deptoNac = $deptoNac;

        return $this;
    }

    /**
     * Get deptoNac
     *
     * @return integer 
     */
    public function getDeptoNac()
    {
        return $this->deptoNac;
    }

    /**
     * Set munNac
     *
     * @param integer $munNac
     * @return Paciente
     */
    public function setMunNac($munNac)
    {
        $this->munNac = $munNac;

        return $this;
    }

    /**
     * Get munNac
     *
     * @return integer 
     */
    public function getMunNac()
    {
        return $this->munNac;
    }

    /**
     * Set deptoRes
     *
     * @param integer $deptoRes
     * @return Paciente
     */
    public function setDeptoRes($deptoRes)
    {
        $this->deptoRes = $deptoRes;

        return $this;
    }

    /**
     * Get deptoRes
     *
     * @return integer 
     */
    public function getDeptoRes()
    {
        return $this->deptoRes;
    }

    /**
     * Set munRes
     *
     * @param integer $munRes
     * @return Paciente
     */
    public function setMunRes($munRes)
    {
        $this->munRes = $munRes;

        return $this;
    }

    /**
     * Get munRes
     *
     * @return integer 
     */
    public function getMunRes()
    {
        return $this->munRes;
    }

    /**
     * Set nivelEduc
     *
     * @param string $nivelEduc
     * @return Paciente
     */
    public function setNivelEduc($nivelEduc)
    {
        $this->nivelEduc = $nivelEduc;

        return $this;
    }

    /**
     * Get nivelEduc
     *
     * @return string 
     */
    public function getNivelEduc()
    {
        return $this->nivelEduc;
    }

    /**
     * Set domicilio
     *
     * @param string $domicilio
     * @return Paciente
     */
    public function setDomicilio($domicilio)
    {
        $this->domicilio = $domicilio;

        return $this;
    }

    /**
     * Get domicilio
     *
     * @return string 
     */
    public function getDomicilio()
    {
        return $this->domicilio;
    }

    /**
     * Set telefonoPx
     *
     * @param string $telefonoPx
     * @return Paciente
     */
    public function setTelefonoPx($telefonoPx)
    {
        $this->telefonoPx = $telefonoPx;

        return $this;
    }

    /**
     * Get telefonoPx
     *
     * @return string 
     */
    public function getTelefonoPx()
    {
        return $this->telefonoPx;
    }

    /**
     * Set movilPx
     *
     * @param string $movilPx
     * @return Paciente
     */
    public function setMovilPx($movilPx)
    {
        $this->movilPx = $movilPx;

        return $this;
    }

    /**
     * Get movilPx
     *
     * @return string 
     */
    public function getMovilPx()
    {
        return $this->movilPx;
    }

    /**
     * Set dirTrabajo
     *
     * @param string $dirTrabajo
     * @return Paciente
     */
    public function setDirTrabajo($dirTrabajo)
    {
        $this->dirTrabajo = $dirTrabajo;

        return $this;
    }

    /**
     * Get dirTrabajo
     *
     * @return string 
     */
    public function getDirTrabajo()
    {
        return $this->dirTrabajo;
    }

    /**
     * Set telTrabajo
     *
     * @param string $telTrabajo
     * @return Paciente
     */
    public function setTelTrabajo($telTrabajo)
    {
        $this->telTrabajo = $telTrabajo;

        return $this;
    }

    /**
     * Get telTrabajo
     *
     * @return string 
     */
    public function getTelTrabajo()
    {
        return $this->telTrabajo;
    }

    /**
     * Set responsable
     *
     * @param string $responsable
     * @return Paciente
     */
    public function setResponsable($responsable)
    {
        $this->responsable = $responsable;

        return $this;
    }

    /**
     * Get responsable
     *
     * @return string 
     */
    public function getResponsable()
    {
        return $this->responsable;
    }

    /**
     * Set telResponsable
     *
     * @param string $telResponsable
     * @return Paciente
     */
    public function setTelResponsable($telResponsable)
    {
        $this->telResponsable = $telResponsable;

        return $this;
    }

    /**
     * Get telResponsable
     *
     * @return string 
     */
    public function getTelResponsable()
    {
        return $this->telResponsable;
    }

    /**
     * Set ocupacion
     *
     * @param string $ocupacion
     * @return Paciente
     */
    public function setOcupacion($ocupacion)
    {
        $this->ocupacion = $ocupacion;

        return $this;
    }

    /**
     * Get ocupacion
     *
     * @return string 
     */
    public function getOcupacion()
    {
        return $this->ocupacion;
    }

    /**
     * Set activo
     *
     * @param boolean $activo
     * @return Paciente
     */
    public function setActivo($activo)
    {
        $this->activo = $activo;

        return $this;
    }

    /**
     * Get activo
     *
     * @return boolean 
     */
    public function getActivo()
    {
        return $this->activo;
    }

    /**
     * Set idGenero
     *
     * @param \foues\FDBundle\Entity\Genero $idGenero
     * @return Paciente
     */
    public function setIdGenero(\foues\FDBundle\Entity\Genero $idGenero = null)
    {
        $this->idGenero = $idGenero;

        return $this;
    }

    /**
     * Get idGenero
     *
     * @return \foues\FDBundle\Entity\Genero 
     */
    public function getIdGenero()
    {
        return $this->idGenero;
    }

    /**
     * Set idEstadoC
     *
     * @param \foues\FDBundle\Entity\EstadoCivil $idEstadoC
     * @return Paciente
     */
    public function setIdEstadoC(\foues\FDBundle\Entity\EstadoCivil $idEstadoC = null)
    {
        $this->idEstadoC = $idEstadoC;

        return $this;
    }

    /**
     * Get idEstadoC
     *
     * @return \foues\FDBundle\Entity\EstadoCivil 
     */
    public function getIdEstadoC()
    {
        return $this->idEstadoC;
    }
}
