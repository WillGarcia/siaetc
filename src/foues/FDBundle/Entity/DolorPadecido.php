<?php

namespace foues\FDBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * DolorPadecido
 *
 * @ORM\Table(name="dolor_padecido", uniqueConstraints={@ORM\UniqueConstraint(name="dolor_padecido_pk", columns={"id_dolorpad"})}, indexes={@ORM\Index(name="relationship_87_fk", columns={"id_f_dx"})})
 * @ORM\Entity
 */
class DolorPadecido
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_dolorpad", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="dolor_padecido_id_dolorpad_seq", allocationSize=1, initialValue=1)
     */
    private $idDolorpad;

    /**
     * @var boolean
     *
     * @ORM\Column(name="dp_cabeza", type="boolean", nullable=true)
     */
    private $dpCabeza;

    /**
     * @var boolean
     *
     * @ORM\Column(name="dp_cara", type="boolean", nullable=true)
     */
    private $dpCara;

    /**
     * @var boolean
     *
     * @ORM\Column(name="dp_oidos", type="boolean", nullable=true)
     */
    private $dpOidos;

    /**
     * @var boolean
     *
     * @ORM\Column(name="dp_cuello", type="boolean", nullable=true)
     */
    private $dpCuello;

    /**
     * @var boolean
     *
     * @ORM\Column(name="dp_hombros", type="boolean", nullable=true)
     */
    private $dpHombros;

    /**
     * @var boolean
     *
     * @ORM\Column(name="dp_espalda", type="boolean", nullable=true)
     */
    private $dpEspalda;

    /**
     * @var boolean
     *
     * @ORM\Column(name="dp_atm", type="boolean", nullable=true)
     */
    private $dpAtm;

    /**
     * @var \FDiagnostico
     *
     * @ORM\ManyToOne(targetEntity="FDiagnostico")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_f_dx", referencedColumnName="id_f_dx")
     * })
     */
    private $idFDx;



    /**
     * Get idDolorpad
     *
     * @return integer 
     */
    public function getIdDolorpad()
    {
        return $this->idDolorpad;
    }

    /**
     * Set dpCabeza
     *
     * @param boolean $dpCabeza
     * @return DolorPadecido
     */
    public function setDpCabeza($dpCabeza)
    {
        $this->dpCabeza = $dpCabeza;

        return $this;
    }

    /**
     * Get dpCabeza
     *
     * @return boolean 
     */
    public function getDpCabeza()
    {
        return $this->dpCabeza;
    }

    /**
     * Set dpCara
     *
     * @param boolean $dpCara
     * @return DolorPadecido
     */
    public function setDpCara($dpCara)
    {
        $this->dpCara = $dpCara;

        return $this;
    }

    /**
     * Get dpCara
     *
     * @return boolean 
     */
    public function getDpCara()
    {
        return $this->dpCara;
    }

    /**
     * Set dpOidos
     *
     * @param boolean $dpOidos
     * @return DolorPadecido
     */
    public function setDpOidos($dpOidos)
    {
        $this->dpOidos = $dpOidos;

        return $this;
    }

    /**
     * Get dpOidos
     *
     * @return boolean 
     */
    public function getDpOidos()
    {
        return $this->dpOidos;
    }

    /**
     * Set dpCuello
     *
     * @param boolean $dpCuello
     * @return DolorPadecido
     */
    public function setDpCuello($dpCuello)
    {
        $this->dpCuello = $dpCuello;

        return $this;
    }

    /**
     * Get dpCuello
     *
     * @return boolean 
     */
    public function getDpCuello()
    {
        return $this->dpCuello;
    }

    /**
     * Set dpHombros
     *
     * @param boolean $dpHombros
     * @return DolorPadecido
     */
    public function setDpHombros($dpHombros)
    {
        $this->dpHombros = $dpHombros;

        return $this;
    }

    /**
     * Get dpHombros
     *
     * @return boolean 
     */
    public function getDpHombros()
    {
        return $this->dpHombros;
    }

    /**
     * Set dpEspalda
     *
     * @param boolean $dpEspalda
     * @return DolorPadecido
     */
    public function setDpEspalda($dpEspalda)
    {
        $this->dpEspalda = $dpEspalda;

        return $this;
    }

    /**
     * Get dpEspalda
     *
     * @return boolean 
     */
    public function getDpEspalda()
    {
        return $this->dpEspalda;
    }

    /**
     * Set dpAtm
     *
     * @param boolean $dpAtm
     * @return DolorPadecido
     */
    public function setDpAtm($dpAtm)
    {
        $this->dpAtm = $dpAtm;

        return $this;
    }

    /**
     * Get dpAtm
     *
     * @return boolean 
     */
    public function getDpAtm()
    {
        return $this->dpAtm;
    }

    /**
     * Set idFDx
     *
     * @param \foues\FDBundle\Entity\FDiagnostico $idFDx
     * @return DolorPadecido
     */
    public function setIdFDx(\foues\FDBundle\Entity\FDiagnostico $idFDx = null)
    {
        $this->idFDx = $idFDx;

        return $this;
    }

    /**
     * Get idFDx
     *
     * @return \foues\FDBundle\Entity\FDiagnostico 
     */
    public function getIdFDx()
    {
        return $this->idFDx;
    }
}
