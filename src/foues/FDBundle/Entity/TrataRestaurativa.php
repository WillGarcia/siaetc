<?php

namespace foues\FDBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TrataRestaurativa
 *
 * @ORM\Table(name="trata_restaurativa", uniqueConstraints={@ORM\UniqueConstraint(name="trata_restaurativa_pk", columns={"id_tratamiento"})}, indexes={@ORM\Index(name="fk_trata_re_seleccion_cat_trat_", columns={"id_cat_resta"}), @ORM\Index(name="fk_trata_re_expone_pl_f_restau_", columns={"id_f_resta"})})
 * @ORM\Entity
 */
class TrataRestaurativa
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_tratamiento", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="trata_restaurativa_id_tratamiento_seq", allocationSize=1, initialValue=1)
     */
    private $idTratamiento;

    /**
     * @var string
     *
     * @ORM\Column(name="nom_trata", type="string", length=100, nullable=true)
     */
    private $nomTrata;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha_trata", type="date", nullable=true)
     */
    private $fechaTrata;

    /**
     * @var \FRestaurativa
     *
     * @ORM\ManyToOne(targetEntity="FRestaurativa")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_f_resta", referencedColumnName="id_f_resta")
     * })
     */
    private $idFResta;

    /**
     * @var \CatTrataResta
     *
     * @ORM\ManyToOne(targetEntity="CatTrataResta")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_cat_resta", referencedColumnName="id_cat_resta")
     * })
     */
    private $idCatResta;



    /**
     * Get idTratamiento
     *
     * @return integer 
     */
    public function getIdTratamiento()
    {
        return $this->idTratamiento;
    }

    /**
     * Set nomTrata
     *
     * @param string $nomTrata
     * @return TrataRestaurativa
     */
    public function setNomTrata($nomTrata)
    {
        $this->nomTrata = $nomTrata;

        return $this;
    }

    /**
     * Get nomTrata
     *
     * @return string 
     */
    public function getNomTrata()
    {
        return $this->nomTrata;
    }

    /**
     * Set fechaTrata
     *
     * @param \DateTime $fechaTrata
     * @return TrataRestaurativa
     */
    public function setFechaTrata($fechaTrata)
    {
        $this->fechaTrata = $fechaTrata;

        return $this;
    }

    /**
     * Get fechaTrata
     *
     * @return \DateTime 
     */
    public function getFechaTrata()
    {
        return $this->fechaTrata;
    }

    /**
     * Set idFResta
     *
     * @param \foues\FDBundle\Entity\FRestaurativa $idFResta
     * @return TrataRestaurativa
     */
    public function setIdFResta(\foues\FDBundle\Entity\FRestaurativa $idFResta = null)
    {
        $this->idFResta = $idFResta;

        return $this;
    }

    /**
     * Get idFResta
     *
     * @return \foues\FDBundle\Entity\FRestaurativa 
     */
    public function getIdFResta()
    {
        return $this->idFResta;
    }

    /**
     * Set idCatResta
     *
     * @param \foues\FDBundle\Entity\CatTrataResta $idCatResta
     * @return TrataRestaurativa
     */
    public function setIdCatResta(\foues\FDBundle\Entity\CatTrataResta $idCatResta = null)
    {
        $this->idCatResta = $idCatResta;

        return $this;
    }

    /**
     * Get idCatResta
     *
     * @return \foues\FDBundle\Entity\CatTrataResta 
     */
    public function getIdCatResta()
    {
        return $this->idCatResta;
    }
}
