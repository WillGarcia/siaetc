<?php

namespace foues\FDBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * GrupoClinica
 *
 * @ORM\Table(name="grupo_clinica", uniqueConstraints={@ORM\UniqueConstraint(name="grupo_clinica_pk", columns={"id_grupoc"})}, indexes={@ORM\Index(name="fk_grupo_cl_dispone_clinica__fk", columns={"id_clinica"}), @ORM\Index(name="fk_grupo_cl_relations_estudian_", columns={"due"}), @ORM\Index(name="relationship_107_fk", columns={"id_turno"})})
 * @ORM\Entity
 */
class GrupoClinica
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_grupoc", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="grupo_clinica_id_grupoc_seq", allocationSize=1, initialValue=1)
     */
    private $idGrupoc;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre_grupo", type="string", length=40, nullable=true)
     */
    private $nombreGrupo;

    /**
     * @var \ClinicaAsignatura
     *
     * @ORM\ManyToOne(targetEntity="ClinicaAsignatura")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_clinica", referencedColumnName="id_clinica")
     * })
     */
    private $idClinica;

    /**
     * @var \Estudiante
     *
     * @ORM\ManyToOne(targetEntity="Estudiante")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="due", referencedColumnName="due")
     * })
     */
    private $due;

    /**
     * @var \TurnoClinico
     *
     * @ORM\ManyToOne(targetEntity="TurnoClinico")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_turno", referencedColumnName="id_turno")
     * })
     */
    private $idTurno;



    /**
     * Get idGrupoc
     *
     * @return integer 
     */
    public function getIdGrupoc()
    {
        return $this->idGrupoc;
    }

    /**
     * Set nombreGrupo
     *
     * @param string $nombreGrupo
     * @return GrupoClinica
     */
    public function setNombreGrupo($nombreGrupo)
    {
        $this->nombreGrupo = $nombreGrupo;

        return $this;
    }

    /**
     * Get nombreGrupo
     *
     * @return string 
     */
    public function getNombreGrupo()
    {
        return $this->nombreGrupo;
    }

    /**
     * Set idClinica
     *
     * @param \foues\FDBundle\Entity\ClinicaAsignatura $idClinica
     * @return GrupoClinica
     */
    public function setIdClinica(\foues\FDBundle\Entity\ClinicaAsignatura $idClinica = null)
    {
        $this->idClinica = $idClinica;

        return $this;
    }

    /**
     * Get idClinica
     *
     * @return \foues\FDBundle\Entity\ClinicaAsignatura 
     */
    public function getIdClinica()
    {
        return $this->idClinica;
    }

    /**
     * Set due
     *
     * @param \foues\FDBundle\Entity\Estudiante $due
     * @return GrupoClinica
     */
    public function setDue(\foues\FDBundle\Entity\Estudiante $due = null)
    {
        $this->due = $due;

        return $this;
    }

    /**
     * Get due
     *
     * @return \foues\FDBundle\Entity\Estudiante 
     */
    public function getDue()
    {
        return $this->due;
    }

    /**
     * Set idTurno
     *
     * @param \foues\FDBundle\Entity\TurnoClinico $idTurno
     * @return GrupoClinica
     */
    public function setIdTurno(\foues\FDBundle\Entity\TurnoClinico $idTurno = null)
    {
        $this->idTurno = $idTurno;

        return $this;
    }

    /**
     * Get idTurno
     *
     * @return \foues\FDBundle\Entity\TurnoClinico 
     */
    public function getIdTurno()
    {
        return $this->idTurno;
    }
}
