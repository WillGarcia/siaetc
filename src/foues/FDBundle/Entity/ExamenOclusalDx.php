<?php

namespace foues\FDBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ExamenOclusalDx
 *
 * @ORM\Table(name="examen_oclusal_dx", uniqueConstraints={@ORM\UniqueConstraint(name="examen_oclusal_dx_pk", columns={"id_exa_oclu"})}, indexes={@ORM\Index(name="fk_examen_o_registra__f_diagno_", columns={"id_f_dx"})})
 * @ORM\Entity
 */
class ExamenOclusalDx
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_exa_oclu", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="examen_oclusal_dx_id_exa_oclu_seq", allocationSize=1, initialValue=1)
     */
    private $idExaOclu;

    /**
     * @var string
     *
     * @ORM\Column(name="inesta_mic", type="string", length=2, nullable=true)
     */
    private $inestaMic;

    /**
     * @var string
     *
     * @ORM\Column(name="desg_oclu", type="string", length=2, nullable=true)
     */
    private $desgOclu;

    /**
     * @var string
     *
     * @ORM\Column(name="abfrac_cervi", type="string", length=2, nullable=true)
     */
    private $abfracCervi;

    /**
     * @var string
     *
     * @ORM\Column(name="mov_fre", type="string", length=2, nullable=true)
     */
    private $movFre;

    /**
     * @var string
     *
     * @ORM\Column(name="frac_resta", type="string", length=2, nullable=true)
     */
    private $fracResta;

    /**
     * @var string
     *
     * @ORM\Column(name="migracion_inc", type="string", length=2, nullable=true)
     */
    private $migracionInc;

    /**
     * @var string
     *
     * @ORM\Column(name="image_tbd", type="string", length=255, nullable=true)
     */
    private $imageTbd;

    /**
     * @var string
     *
     * @ORM\Column(name="image_tbi", type="string", length=255, nullable=true)
     */
    private $imageTbi;

    /**
     * @var string
     *
     * @ORM\Column(name="image_tbp", type="string", length=255, nullable=true)
     */
    private $imageTbp;

    /**
     * @var \FDiagnostico
     *
     * @ORM\ManyToOne(targetEntity="FDiagnostico")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_f_dx", referencedColumnName="id_f_dx")
     * })
     */
    private $idFDx;



    /**
     * Get idExaOclu
     *
     * @return integer 
     */
    public function getIdExaOclu()
    {
        return $this->idExaOclu;
    }

    /**
     * Set inestaMic
     *
     * @param string $inestaMic
     * @return ExamenOclusalDx
     */
    public function setInestaMic($inestaMic)
    {
        $this->inestaMic = $inestaMic;

        return $this;
    }

    /**
     * Get inestaMic
     *
     * @return string 
     */
    public function getInestaMic()
    {
        return $this->inestaMic;
    }

    /**
     * Set desgOclu
     *
     * @param string $desgOclu
     * @return ExamenOclusalDx
     */
    public function setDesgOclu($desgOclu)
    {
        $this->desgOclu = $desgOclu;

        return $this;
    }

    /**
     * Get desgOclu
     *
     * @return string 
     */
    public function getDesgOclu()
    {
        return $this->desgOclu;
    }

    /**
     * Set abfracCervi
     *
     * @param string $abfracCervi
     * @return ExamenOclusalDx
     */
    public function setAbfracCervi($abfracCervi)
    {
        $this->abfracCervi = $abfracCervi;

        return $this;
    }

    /**
     * Get abfracCervi
     *
     * @return string 
     */
    public function getAbfracCervi()
    {
        return $this->abfracCervi;
    }

    /**
     * Set movFre
     *
     * @param string $movFre
     * @return ExamenOclusalDx
     */
    public function setMovFre($movFre)
    {
        $this->movFre = $movFre;

        return $this;
    }

    /**
     * Get movFre
     *
     * @return string 
     */
    public function getMovFre()
    {
        return $this->movFre;
    }

    /**
     * Set fracResta
     *
     * @param string $fracResta
     * @return ExamenOclusalDx
     */
    public function setFracResta($fracResta)
    {
        $this->fracResta = $fracResta;

        return $this;
    }

    /**
     * Get fracResta
     *
     * @return string 
     */
    public function getFracResta()
    {
        return $this->fracResta;
    }

    /**
     * Set migracionInc
     *
     * @param string $migracionInc
     * @return ExamenOclusalDx
     */
    public function setMigracionInc($migracionInc)
    {
        $this->migracionInc = $migracionInc;

        return $this;
    }

    /**
     * Get migracionInc
     *
     * @return string 
     */
    public function getMigracionInc()
    {
        return $this->migracionInc;
    }

    /**
     * Set imageTbd
     *
     * @param string $imageTbd
     * @return ExamenOclusalDx
     */
    public function setImageTbd($imageTbd)
    {
        $this->imageTbd = $imageTbd;

        return $this;
    }

    /**
     * Get imageTbd
     *
     * @return string 
     */
    public function getImageTbd()
    {
        return $this->imageTbd;
    }

    /**
     * Set imageTbi
     *
     * @param string $imageTbi
     * @return ExamenOclusalDx
     */
    public function setImageTbi($imageTbi)
    {
        $this->imageTbi = $imageTbi;

        return $this;
    }

    /**
     * Get imageTbi
     *
     * @return string 
     */
    public function getImageTbi()
    {
        return $this->imageTbi;
    }

    /**
     * Set imageTbp
     *
     * @param string $imageTbp
     * @return ExamenOclusalDx
     */
    public function setImageTbp($imageTbp)
    {
        $this->imageTbp = $imageTbp;

        return $this;
    }

    /**
     * Get imageTbp
     *
     * @return string 
     */
    public function getImageTbp()
    {
        return $this->imageTbp;
    }

    /**
     * Set idFDx
     *
     * @param \foues\FDBundle\Entity\FDiagnostico $idFDx
     * @return ExamenOclusalDx
     */
    public function setIdFDx(\foues\FDBundle\Entity\FDiagnostico $idFDx = null)
    {
        $this->idFDx = $idFDx;

        return $this;
    }

    /**
     * Get idFDx
     *
     * @return \foues\FDBundle\Entity\FDiagnostico 
     */
    public function getIdFDx()
    {
        return $this->idFDx;
    }
}
