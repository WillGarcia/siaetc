<?php

namespace foues\FDBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * EvaPulparDx
 *
 * @ORM\Table(name="eva_pulpar_dx", uniqueConstraints={@ORM\UniqueConstraint(name="eva_pulpar_dx_pk", columns={"id_eva_pul"})}, indexes={@ORM\Index(name="fk_eva_pulp_presenta2_f_diagno_", columns={"id_f_dx"})})
 * @ORM\Entity
 */
class EvaPulparDx
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_eva_pul", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="eva_pulpar_dx_id_eva_pul_seq", allocationSize=1, initialValue=1)
     */
    private $idEvaPul;

    /**
     * @var string
     *
     * @ORM\Column(name="diente_eva", type="string", length=4, nullable=true)
     */
    private $dienteEva;

    /**
     * @var string
     *
     * @ORM\Column(name="otro_hallazgoep", type="string", length=100, nullable=true)
     */
    private $otroHallazgoep;

    /**
     * @var \FDiagnostico
     *
     * @ORM\ManyToOne(targetEntity="FDiagnostico")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_f_dx", referencedColumnName="id_f_dx")
     * })
     */
    private $idFDx;



    /**
     * Get idEvaPul
     *
     * @return integer 
     */
    public function getIdEvaPul()
    {
        return $this->idEvaPul;
    }

    /**
     * Set dienteEva
     *
     * @param string $dienteEva
     * @return EvaPulparDx
     */
    public function setDienteEva($dienteEva)
    {
        $this->dienteEva = $dienteEva;

        return $this;
    }

    /**
     * Get dienteEva
     *
     * @return string 
     */
    public function getDienteEva()
    {
        return $this->dienteEva;
    }

    /**
     * Set otroHallazgoep
     *
     * @param string $otroHallazgoep
     * @return EvaPulparDx
     */
    public function setOtroHallazgoep($otroHallazgoep)
    {
        $this->otroHallazgoep = $otroHallazgoep;

        return $this;
    }

    /**
     * Get otroHallazgoep
     *
     * @return string 
     */
    public function getOtroHallazgoep()
    {
        return $this->otroHallazgoep;
    }

    /**
     * Set idFDx
     *
     * @param \foues\FDBundle\Entity\FDiagnostico $idFDx
     * @return EvaPulparDx
     */
    public function setIdFDx(\foues\FDBundle\Entity\FDiagnostico $idFDx = null)
    {
        $this->idFDx = $idFDx;

        return $this;
    }

    /**
     * Get idFDx
     *
     * @return \foues\FDBundle\Entity\FDiagnostico 
     */
    public function getIdFDx()
    {
        return $this->idFDx;
    }
}
