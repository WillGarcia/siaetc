<?php

namespace foues\FDBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Odontograma
 *
 * @ORM\Table(name="odontograma", uniqueConstraints={@ORM\UniqueConstraint(name="odontograma_pk", columns={"id_odonto"})}, indexes={@ORM\Index(name="fk_odontogr_posee5_f_diagno_fk", columns={"id_f_dx"})})
 * @ORM\Entity
 */
class Odontograma
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_odonto", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="odontograma_id_odonto_seq", allocationSize=1, initialValue=1)
     */
    private $idOdonto;

    /**
     * @var string
     *
     * @ORM\Column(name="image_odonto", type="string", length=255, nullable=false)
     */
    private $imageOdonto;

    /**
     * @var \FDiagnostico
     *
     * @ORM\ManyToOne(targetEntity="FDiagnostico")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_f_dx", referencedColumnName="id_f_dx")
     * })
     */
    private $idFDx;



    /**
     * Get idOdonto
     *
     * @return integer 
     */
    public function getIdOdonto()
    {
        return $this->idOdonto;
    }

    /**
     * Set imageOdonto
     *
     * @param string $imageOdonto
     * @return Odontograma
     */
    public function setImageOdonto($imageOdonto)
    {
        $this->imageOdonto = $imageOdonto;

        return $this;
    }

    /**
     * Get imageOdonto
     *
     * @return string 
     */
    public function getImageOdonto()
    {
        return $this->imageOdonto;
    }

    /**
     * Set idFDx
     *
     * @param \foues\FDBundle\Entity\FDiagnostico $idFDx
     * @return Odontograma
     */
    public function setIdFDx(\foues\FDBundle\Entity\FDiagnostico $idFDx = null)
    {
        $this->idFDx = $idFDx;

        return $this;
    }

    /**
     * Get idFDx
     *
     * @return \foues\FDBundle\Entity\FDiagnostico 
     */
    public function getIdFDx()
    {
        return $this->idFDx;
    }
}
