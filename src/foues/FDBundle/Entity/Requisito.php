<?php

namespace foues\FDBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Requisito
 *
 * @ORM\Table(name="requisito", uniqueConstraints={@ORM\UniqueConstraint(name="requisito_pk", columns={"id_requisito"})}, indexes={@ORM\Index(name="fk_requisit_debe_cump_paciente_", columns={"id_pac"}), @ORM\Index(name="fk_requisit_seleciona_clinica__", columns={"id_clinica"})})
 * @ORM\Entity
 */
class Requisito
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_requisito", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="requisito_id_requisito_seq", allocationSize=1, initialValue=1)
     */
    private $idRequisito;

    /**
     * @var string
     *
     * @ORM\Column(name="nom_requisito", type="string", length=15, nullable=false)
     */
    private $nomRequisito;

    /**
     * @var \Paciente
     *
     * @ORM\ManyToOne(targetEntity="Paciente")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_pac", referencedColumnName="id_pac")
     * })
     */
    private $idPac;

    /**
     * @var \ClinicaAsignatura
     *
     * @ORM\ManyToOne(targetEntity="ClinicaAsignatura")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_clinica", referencedColumnName="id_clinica")
     * })
     */
    private $idClinica;



    /**
     * Get idRequisito
     *
     * @return integer 
     */
    public function getIdRequisito()
    {
        return $this->idRequisito;
    }

    /**
     * Set nomRequisito
     *
     * @param string $nomRequisito
     * @return Requisito
     */
    public function setNomRequisito($nomRequisito)
    {
        $this->nomRequisito = $nomRequisito;

        return $this;
    }

    /**
     * Get nomRequisito
     *
     * @return string 
     */
    public function getNomRequisito()
    {
        return $this->nomRequisito;
    }

    /**
     * Set idPac
     *
     * @param \foues\FDBundle\Entity\Paciente $idPac
     * @return Requisito
     */
    public function setIdPac(\foues\FDBundle\Entity\Paciente $idPac = null)
    {
        $this->idPac = $idPac;

        return $this;
    }

    /**
     * Get idPac
     *
     * @return \foues\FDBundle\Entity\Paciente 
     */
    public function getIdPac()
    {
        return $this->idPac;
    }

    /**
     * Set idClinica
     *
     * @param \foues\FDBundle\Entity\ClinicaAsignatura $idClinica
     * @return Requisito
     */
    public function setIdClinica(\foues\FDBundle\Entity\ClinicaAsignatura $idClinica = null)
    {
        $this->idClinica = $idClinica;

        return $this;
    }

    /**
     * Get idClinica
     *
     * @return \foues\FDBundle\Entity\ClinicaAsignatura 
     */
    public function getIdClinica()
    {
        return $this->idClinica;
    }
}
