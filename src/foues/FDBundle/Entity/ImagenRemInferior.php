<?php

namespace foues\FDBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ImagenRemInferior
 *
 * @ORM\Table(name="imagen_rem_inferior", uniqueConstraints={@ORM\UniqueConstraint(name="imagen_rem_inferior_pk", columns={"id_image_in"})}, indexes={@ORM\Index(name="fk_imagen_r_disenia_f_restau_fk", columns={"id_f_resta"})})
 * @ORM\Entity
 */
class ImagenRemInferior
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_image_in", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="imagen_rem_inferior_id_image_in_seq", allocationSize=1, initialValue=1)
     */
    private $idImageIn;

    /**
     * @var string
     *
     * @ORM\Column(name="nom_image_in", type="string", length=40, nullable=true)
     */
    private $nomImageIn;

    /**
     * @var \FRestaurativa
     *
     * @ORM\ManyToOne(targetEntity="FRestaurativa")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_f_resta", referencedColumnName="id_f_resta")
     * })
     */
    private $idFResta;



    /**
     * Get idImageIn
     *
     * @return integer 
     */
    public function getIdImageIn()
    {
        return $this->idImageIn;
    }

    /**
     * Set nomImageIn
     *
     * @param string $nomImageIn
     * @return ImagenRemInferior
     */
    public function setNomImageIn($nomImageIn)
    {
        $this->nomImageIn = $nomImageIn;

        return $this;
    }

    /**
     * Get nomImageIn
     *
     * @return string 
     */
    public function getNomImageIn()
    {
        return $this->nomImageIn;
    }

    /**
     * Set idFResta
     *
     * @param \foues\FDBundle\Entity\FRestaurativa $idFResta
     * @return ImagenRemInferior
     */
    public function setIdFResta(\foues\FDBundle\Entity\FRestaurativa $idFResta = null)
    {
        $this->idFResta = $idFResta;

        return $this;
    }

    /**
     * Get idFResta
     *
     * @return \foues\FDBundle\Entity\FRestaurativa 
     */
    public function getIdFResta()
    {
        return $this->idFResta;
    }
}
