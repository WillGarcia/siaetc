<?php

namespace foues\FDBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ExRad4
 *
 * @ORM\Table(name="ex_rad_4", uniqueConstraints={@ORM\UniqueConstraint(name="ex_rad_4_pk", columns={"id_exr_4"})}, indexes={@ORM\Index(name="relationship_95_fk", columns={"id_f_dx"})})
 * @ORM\Entity
 */
class ExRad4
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_exr_4", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="ex_rad_4_id_exr_4_seq", allocationSize=1, initialValue=1)
     */
    private $idExr4;

    /**
     * @var string
     *
     * @ORM\Column(name="d4_8_d", type="string", length=2, nullable=true)
     */
    private $d48D;

    /**
     * @var string
     *
     * @ORM\Column(name="m4_8_m", type="string", length=2, nullable=true)
     */
    private $m48M;

    /**
     * @var string
     *
     * @ORM\Column(name="d4_7_d", type="string", length=2, nullable=true)
     */
    private $d47D;

    /**
     * @var string
     *
     * @ORM\Column(name="m4_7_m", type="string", length=2, nullable=true)
     */
    private $m47M;

    /**
     * @var string
     *
     * @ORM\Column(name="d4_6_d", type="string", length=2, nullable=true)
     */
    private $d46D;

    /**
     * @var string
     *
     * @ORM\Column(name="m4_6_m", type="string", length=2, nullable=true)
     */
    private $m46M;

    /**
     * @var string
     *
     * @ORM\Column(name="d4_5_d", type="string", length=2, nullable=true)
     */
    private $d45D;

    /**
     * @var string
     *
     * @ORM\Column(name="m4_5_m", type="string", length=2, nullable=true)
     */
    private $m45M;

    /**
     * @var string
     *
     * @ORM\Column(name="d4_4_d", type="string", length=2, nullable=true)
     */
    private $d44D;

    /**
     * @var string
     *
     * @ORM\Column(name="m4_4_m", type="string", length=2, nullable=true)
     */
    private $m44M;

    /**
     * @var string
     *
     * @ORM\Column(name="d4_3_d", type="string", length=2, nullable=true)
     */
    private $d43D;

    /**
     * @var string
     *
     * @ORM\Column(name="m4_3_m", type="string", length=2, nullable=true)
     */
    private $m43M;

    /**
     * @var string
     *
     * @ORM\Column(name="d4_2_d", type="string", length=2, nullable=true)
     */
    private $d42D;

    /**
     * @var string
     *
     * @ORM\Column(name="m4_2_m", type="string", length=2, nullable=true)
     */
    private $m42M;

    /**
     * @var string
     *
     * @ORM\Column(name="d4_1_d", type="string", length=2, nullable=true)
     */
    private $d41D;

    /**
     * @var string
     *
     * @ORM\Column(name="m4_1_m", type="string", length=2, nullable=true)
     */
    private $m41M;

    /**
     * @var \FDiagnostico
     *
     * @ORM\ManyToOne(targetEntity="FDiagnostico")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_f_dx", referencedColumnName="id_f_dx")
     * })
     */
    private $idFDx;



    /**
     * Get idExr4
     *
     * @return integer 
     */
    public function getIdExr4()
    {
        return $this->idExr4;
    }

    /**
     * Set d48D
     *
     * @param string $d48D
     * @return ExRad4
     */
    public function setD48D($d48D)
    {
        $this->d48D = $d48D;

        return $this;
    }

    /**
     * Get d48D
     *
     * @return string 
     */
    public function getD48D()
    {
        return $this->d48D;
    }

    /**
     * Set m48M
     *
     * @param string $m48M
     * @return ExRad4
     */
    public function setM48M($m48M)
    {
        $this->m48M = $m48M;

        return $this;
    }

    /**
     * Get m48M
     *
     * @return string 
     */
    public function getM48M()
    {
        return $this->m48M;
    }

    /**
     * Set d47D
     *
     * @param string $d47D
     * @return ExRad4
     */
    public function setD47D($d47D)
    {
        $this->d47D = $d47D;

        return $this;
    }

    /**
     * Get d47D
     *
     * @return string 
     */
    public function getD47D()
    {
        return $this->d47D;
    }

    /**
     * Set m47M
     *
     * @param string $m47M
     * @return ExRad4
     */
    public function setM47M($m47M)
    {
        $this->m47M = $m47M;

        return $this;
    }

    /**
     * Get m47M
     *
     * @return string 
     */
    public function getM47M()
    {
        return $this->m47M;
    }

    /**
     * Set d46D
     *
     * @param string $d46D
     * @return ExRad4
     */
    public function setD46D($d46D)
    {
        $this->d46D = $d46D;

        return $this;
    }

    /**
     * Get d46D
     *
     * @return string 
     */
    public function getD46D()
    {
        return $this->d46D;
    }

    /**
     * Set m46M
     *
     * @param string $m46M
     * @return ExRad4
     */
    public function setM46M($m46M)
    {
        $this->m46M = $m46M;

        return $this;
    }

    /**
     * Get m46M
     *
     * @return string 
     */
    public function getM46M()
    {
        return $this->m46M;
    }

    /**
     * Set d45D
     *
     * @param string $d45D
     * @return ExRad4
     */
    public function setD45D($d45D)
    {
        $this->d45D = $d45D;

        return $this;
    }

    /**
     * Get d45D
     *
     * @return string 
     */
    public function getD45D()
    {
        return $this->d45D;
    }

    /**
     * Set m45M
     *
     * @param string $m45M
     * @return ExRad4
     */
    public function setM45M($m45M)
    {
        $this->m45M = $m45M;

        return $this;
    }

    /**
     * Get m45M
     *
     * @return string 
     */
    public function getM45M()
    {
        return $this->m45M;
    }

    /**
     * Set d44D
     *
     * @param string $d44D
     * @return ExRad4
     */
    public function setD44D($d44D)
    {
        $this->d44D = $d44D;

        return $this;
    }

    /**
     * Get d44D
     *
     * @return string 
     */
    public function getD44D()
    {
        return $this->d44D;
    }

    /**
     * Set m44M
     *
     * @param string $m44M
     * @return ExRad4
     */
    public function setM44M($m44M)
    {
        $this->m44M = $m44M;

        return $this;
    }

    /**
     * Get m44M
     *
     * @return string 
     */
    public function getM44M()
    {
        return $this->m44M;
    }

    /**
     * Set d43D
     *
     * @param string $d43D
     * @return ExRad4
     */
    public function setD43D($d43D)
    {
        $this->d43D = $d43D;

        return $this;
    }

    /**
     * Get d43D
     *
     * @return string 
     */
    public function getD43D()
    {
        return $this->d43D;
    }

    /**
     * Set m43M
     *
     * @param string $m43M
     * @return ExRad4
     */
    public function setM43M($m43M)
    {
        $this->m43M = $m43M;

        return $this;
    }

    /**
     * Get m43M
     *
     * @return string 
     */
    public function getM43M()
    {
        return $this->m43M;
    }

    /**
     * Set d42D
     *
     * @param string $d42D
     * @return ExRad4
     */
    public function setD42D($d42D)
    {
        $this->d42D = $d42D;

        return $this;
    }

    /**
     * Get d42D
     *
     * @return string 
     */
    public function getD42D()
    {
        return $this->d42D;
    }

    /**
     * Set m42M
     *
     * @param string $m42M
     * @return ExRad4
     */
    public function setM42M($m42M)
    {
        $this->m42M = $m42M;

        return $this;
    }

    /**
     * Get m42M
     *
     * @return string 
     */
    public function getM42M()
    {
        return $this->m42M;
    }

    /**
     * Set d41D
     *
     * @param string $d41D
     * @return ExRad4
     */
    public function setD41D($d41D)
    {
        $this->d41D = $d41D;

        return $this;
    }

    /**
     * Get d41D
     *
     * @return string 
     */
    public function getD41D()
    {
        return $this->d41D;
    }

    /**
     * Set m41M
     *
     * @param string $m41M
     * @return ExRad4
     */
    public function setM41M($m41M)
    {
        $this->m41M = $m41M;

        return $this;
    }

    /**
     * Get m41M
     *
     * @return string 
     */
    public function getM41M()
    {
        return $this->m41M;
    }

    /**
     * Set idFDx
     *
     * @param \foues\FDBundle\Entity\FDiagnostico $idFDx
     * @return ExRad4
     */
    public function setIdFDx(\foues\FDBundle\Entity\FDiagnostico $idFDx = null)
    {
        $this->idFDx = $idFDx;

        return $this;
    }

    /**
     * Get idFDx
     *
     * @return \foues\FDBundle\Entity\FDiagnostico 
     */
    public function getIdFDx()
    {
        return $this->idFDx;
    }
}
