<?php

namespace foues\FDBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TratamientoPerio
 *
 * @ORM\Table(name="tratamiento_perio", uniqueConstraints={@ORM\UniqueConstraint(name="tratamiento_perio_pk", columns={"id_plan_trat_p"})}, indexes={@ORM\Index(name="IDX_AC910F9D224214", columns={"id_f_perio"})})
 * @ORM\Entity
 */
class TratamientoPerio
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_plan_trat_p", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="tratamiento_perio_id_plan_trat_p_seq", allocationSize=1, initialValue=1)
     */
    private $idPlanTratP;

    /**
     * @var string
     *
     * @ORM\Column(name="fase_sist", type="string", length=200, nullable=true)
     */
    private $faseSist;

    /**
     * @var string
     *
     * @ORM\Column(name="fase_prel", type="string", length=200, nullable=true)
     */
    private $fasePrel;

    /**
     * @var string
     *
     * @ORM\Column(name="fase_hig", type="string", length=200, nullable=true)
     */
    private $faseHig;

    /**
     * @var string
     *
     * @ORM\Column(name="fase_quiru", type="string", length=200, nullable=true)
     */
    private $faseQuiru;

    /**
     * @var string
     *
     * @ORM\Column(name="fase_manto", type="string", length=200, nullable=true)
     */
    private $faseManto;

    /**
     * @var string
     *
     * @ORM\Column(name="recomend", type="string", length=100, nullable=true)
     */
    private $recomend;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha", type="date", nullable=true)
     */
    private $fecha;

    /**
     * @var \FPeriodoncia
     *
     * @ORM\ManyToOne(targetEntity="FPeriodoncia")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_f_perio", referencedColumnName="id_f_perio")
     * })
     */
    private $idFPerio;



    /**
     * Get idPlanTratP
     *
     * @return integer 
     */
    public function getIdPlanTratP()
    {
        return $this->idPlanTratP;
    }

    /**
     * Set faseSist
     *
     * @param string $faseSist
     * @return TratamientoPerio
     */
    public function setFaseSist($faseSist)
    {
        $this->faseSist = $faseSist;

        return $this;
    }

    /**
     * Get faseSist
     *
     * @return string 
     */
    public function getFaseSist()
    {
        return $this->faseSist;
    }

    /**
     * Set fasePrel
     *
     * @param string $fasePrel
     * @return TratamientoPerio
     */
    public function setFasePrel($fasePrel)
    {
        $this->fasePrel = $fasePrel;

        return $this;
    }

    /**
     * Get fasePrel
     *
     * @return string 
     */
    public function getFasePrel()
    {
        return $this->fasePrel;
    }

    /**
     * Set faseHig
     *
     * @param string $faseHig
     * @return TratamientoPerio
     */
    public function setFaseHig($faseHig)
    {
        $this->faseHig = $faseHig;

        return $this;
    }

    /**
     * Get faseHig
     *
     * @return string 
     */
    public function getFaseHig()
    {
        return $this->faseHig;
    }

    /**
     * Set faseQuiru
     *
     * @param string $faseQuiru
     * @return TratamientoPerio
     */
    public function setFaseQuiru($faseQuiru)
    {
        $this->faseQuiru = $faseQuiru;

        return $this;
    }

    /**
     * Get faseQuiru
     *
     * @return string 
     */
    public function getFaseQuiru()
    {
        return $this->faseQuiru;
    }

    /**
     * Set faseManto
     *
     * @param string $faseManto
     * @return TratamientoPerio
     */
    public function setFaseManto($faseManto)
    {
        $this->faseManto = $faseManto;

        return $this;
    }

    /**
     * Get faseManto
     *
     * @return string 
     */
    public function getFaseManto()
    {
        return $this->faseManto;
    }

    /**
     * Set recomend
     *
     * @param string $recomend
     * @return TratamientoPerio
     */
    public function setRecomend($recomend)
    {
        $this->recomend = $recomend;

        return $this;
    }

    /**
     * Get recomend
     *
     * @return string 
     */
    public function getRecomend()
    {
        return $this->recomend;
    }

    /**
     * Set fecha
     *
     * @param \DateTime $fecha
     * @return TratamientoPerio
     */
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;

        return $this;
    }

    /**
     * Get fecha
     *
     * @return \DateTime 
     */
    public function getFecha()
    {
        return $this->fecha;
    }

    /**
     * Set idFPerio
     *
     * @param \foues\FDBundle\Entity\FPeriodoncia $idFPerio
     * @return TratamientoPerio
     */
    public function setIdFPerio(\foues\FDBundle\Entity\FPeriodoncia $idFPerio = null)
    {
        $this->idFPerio = $idFPerio;

        return $this;
    }

    /**
     * Get idFPerio
     *
     * @return \foues\FDBundle\Entity\FPeriodoncia 
     */
    public function getIdFPerio()
    {
        return $this->idFPerio;
    }
}
