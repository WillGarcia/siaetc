<?php

namespace foues\FDBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * FichaClinica
 *
 * @ORM\Table(name="ficha_clinica", uniqueConstraints={@ORM\UniqueConstraint(name="ficha_clinica_pk", columns={"id_ficha"})}, indexes={@ORM\Index(name="IDX_615140F85A8BE498", columns={"num_expediente"})})
 * @ORM\Entity
 */
class FichaClinica
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_ficha", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="ficha_clinica_id_ficha_seq", allocationSize=1, initialValue=1)
     */
    private $idFicha;

    /**
     * @var \Expediente
     *
     * @ORM\ManyToOne(targetEntity="Expediente")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="num_expediente", referencedColumnName="num_expediente")
     * })
     */
    private $numExpediente;



    /**
     * Get idFicha
     *
     * @return integer 
     */
    public function getIdFicha()
    {
        return $this->idFicha;
    }

    /**
     * Set numExpediente
     *
     * @param \foues\FDBundle\Entity\Expediente $numExpediente
     * @return FichaClinica
     */
    public function setNumExpediente(\foues\FDBundle\Entity\Expediente $numExpediente = null)
    {
        $this->numExpediente = $numExpediente;

        return $this;
    }

    /**
     * Get numExpediente
     *
     * @return \foues\FDBundle\Entity\Expediente 
     */
    public function getNumExpediente()
    {
        return $this->numExpediente;
    }
}
