<?php

namespace foues\FDBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ExRad1
 *
 * @ORM\Table(name="ex_rad_1", uniqueConstraints={@ORM\UniqueConstraint(name="ex_rad_1_pk", columns={"id_exr_1"})}, indexes={@ORM\Index(name="relationship_99_fk", columns={"id_f_dx"})})
 * @ORM\Entity
 */
class ExRad1
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_exr_1", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="ex_rad_1_id_exr_1_seq", allocationSize=1, initialValue=1)
     */
    private $idExr1;

    /**
     * @var string
     *
     * @ORM\Column(name="d1_8", type="string", length=2, nullable=true)
     */
    private $d18;

    /**
     * @var string
     *
     * @ORM\Column(name="m1_8", type="string", length=2, nullable=true)
     */
    private $m18;

    /**
     * @var string
     *
     * @ORM\Column(name="d1_7", type="string", length=2, nullable=true)
     */
    private $d17;

    /**
     * @var string
     *
     * @ORM\Column(name="m1_7", type="string", length=2, nullable=true)
     */
    private $m17;

    /**
     * @var string
     *
     * @ORM\Column(name="d1_6", type="string", length=2, nullable=true)
     */
    private $d16;

    /**
     * @var string
     *
     * @ORM\Column(name="m1_6", type="string", length=2, nullable=true)
     */
    private $m16;

    /**
     * @var string
     *
     * @ORM\Column(name="d1_5", type="string", length=2, nullable=true)
     */
    private $d15;

    /**
     * @var string
     *
     * @ORM\Column(name="m1_5", type="string", length=2, nullable=true)
     */
    private $m15;

    /**
     * @var string
     *
     * @ORM\Column(name="d1_4", type="string", length=2, nullable=true)
     */
    private $d14;

    /**
     * @var string
     *
     * @ORM\Column(name="m1_4", type="string", length=2, nullable=true)
     */
    private $m14;

    /**
     * @var string
     *
     * @ORM\Column(name="d1_3", type="string", length=2, nullable=true)
     */
    private $d13;

    /**
     * @var string
     *
     * @ORM\Column(name="m1_3", type="string", length=2, nullable=true)
     */
    private $m13;

    /**
     * @var string
     *
     * @ORM\Column(name="d1_2", type="string", length=2, nullable=true)
     */
    private $d12;

    /**
     * @var string
     *
     * @ORM\Column(name="m1_2", type="string", length=2, nullable=true)
     */
    private $m12;

    /**
     * @var string
     *
     * @ORM\Column(name="d1_1", type="string", length=2, nullable=true)
     */
    private $d11;

    /**
     * @var string
     *
     * @ORM\Column(name="m1_1", type="string", length=2, nullable=true)
     */
    private $m11;

    /**
     * @var \FDiagnostico
     *
     * @ORM\ManyToOne(targetEntity="FDiagnostico")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_f_dx", referencedColumnName="id_f_dx")
     * })
     */
    private $idFDx;



    /**
     * Get idExr1
     *
     * @return integer 
     */
    public function getIdExr1()
    {
        return $this->idExr1;
    }

    /**
     * Set d18
     *
     * @param string $d18
     * @return ExRad1
     */
    public function setD18($d18)
    {
        $this->d18 = $d18;

        return $this;
    }

    /**
     * Get d18
     *
     * @return string 
     */
    public function getD18()
    {
        return $this->d18;
    }

    /**
     * Set m18
     *
     * @param string $m18
     * @return ExRad1
     */
    public function setM18($m18)
    {
        $this->m18 = $m18;

        return $this;
    }

    /**
     * Get m18
     *
     * @return string 
     */
    public function getM18()
    {
        return $this->m18;
    }

    /**
     * Set d17
     *
     * @param string $d17
     * @return ExRad1
     */
    public function setD17($d17)
    {
        $this->d17 = $d17;

        return $this;
    }

    /**
     * Get d17
     *
     * @return string 
     */
    public function getD17()
    {
        return $this->d17;
    }

    /**
     * Set m17
     *
     * @param string $m17
     * @return ExRad1
     */
    public function setM17($m17)
    {
        $this->m17 = $m17;

        return $this;
    }

    /**
     * Get m17
     *
     * @return string 
     */
    public function getM17()
    {
        return $this->m17;
    }

    /**
     * Set d16
     *
     * @param string $d16
     * @return ExRad1
     */
    public function setD16($d16)
    {
        $this->d16 = $d16;

        return $this;
    }

    /**
     * Get d16
     *
     * @return string 
     */
    public function getD16()
    {
        return $this->d16;
    }

    /**
     * Set m16
     *
     * @param string $m16
     * @return ExRad1
     */
    public function setM16($m16)
    {
        $this->m16 = $m16;

        return $this;
    }

    /**
     * Get m16
     *
     * @return string 
     */
    public function getM16()
    {
        return $this->m16;
    }

    /**
     * Set d15
     *
     * @param string $d15
     * @return ExRad1
     */
    public function setD15($d15)
    {
        $this->d15 = $d15;

        return $this;
    }

    /**
     * Get d15
     *
     * @return string 
     */
    public function getD15()
    {
        return $this->d15;
    }

    /**
     * Set m15
     *
     * @param string $m15
     * @return ExRad1
     */
    public function setM15($m15)
    {
        $this->m15 = $m15;

        return $this;
    }

    /**
     * Get m15
     *
     * @return string 
     */
    public function getM15()
    {
        return $this->m15;
    }

    /**
     * Set d14
     *
     * @param string $d14
     * @return ExRad1
     */
    public function setD14($d14)
    {
        $this->d14 = $d14;

        return $this;
    }

    /**
     * Get d14
     *
     * @return string 
     */
    public function getD14()
    {
        return $this->d14;
    }

    /**
     * Set m14
     *
     * @param string $m14
     * @return ExRad1
     */
    public function setM14($m14)
    {
        $this->m14 = $m14;

        return $this;
    }

    /**
     * Get m14
     *
     * @return string 
     */
    public function getM14()
    {
        return $this->m14;
    }

    /**
     * Set d13
     *
     * @param string $d13
     * @return ExRad1
     */
    public function setD13($d13)
    {
        $this->d13 = $d13;

        return $this;
    }

    /**
     * Get d13
     *
     * @return string 
     */
    public function getD13()
    {
        return $this->d13;
    }

    /**
     * Set m13
     *
     * @param string $m13
     * @return ExRad1
     */
    public function setM13($m13)
    {
        $this->m13 = $m13;

        return $this;
    }

    /**
     * Get m13
     *
     * @return string 
     */
    public function getM13()
    {
        return $this->m13;
    }

    /**
     * Set d12
     *
     * @param string $d12
     * @return ExRad1
     */
    public function setD12($d12)
    {
        $this->d12 = $d12;

        return $this;
    }

    /**
     * Get d12
     *
     * @return string 
     */
    public function getD12()
    {
        return $this->d12;
    }

    /**
     * Set m12
     *
     * @param string $m12
     * @return ExRad1
     */
    public function setM12($m12)
    {
        $this->m12 = $m12;

        return $this;
    }

    /**
     * Get m12
     *
     * @return string 
     */
    public function getM12()
    {
        return $this->m12;
    }

    /**
     * Set d11
     *
     * @param string $d11
     * @return ExRad1
     */
    public function setD11($d11)
    {
        $this->d11 = $d11;

        return $this;
    }

    /**
     * Get d11
     *
     * @return string 
     */
    public function getD11()
    {
        return $this->d11;
    }

    /**
     * Set m11
     *
     * @param string $m11
     * @return ExRad1
     */
    public function setM11($m11)
    {
        $this->m11 = $m11;

        return $this;
    }

    /**
     * Get m11
     *
     * @return string 
     */
    public function getM11()
    {
        return $this->m11;
    }

    /**
     * Set idFDx
     *
     * @param \foues\FDBundle\Entity\FDiagnostico $idFDx
     * @return ExRad1
     */
    public function setIdFDx(\foues\FDBundle\Entity\FDiagnostico $idFDx = null)
    {
        $this->idFDx = $idFDx;

        return $this;
    }

    /**
     * Get idFDx
     *
     * @return \foues\FDBundle\Entity\FDiagnostico 
     */
    public function getIdFDx()
    {
        return $this->idFDx;
    }
}
