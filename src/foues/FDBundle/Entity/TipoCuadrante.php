<?php

namespace foues\FDBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TipoCuadrante
 *
 * @ORM\Table(name="tipo_cuadrante")
 * @ORM\Entity
 */
class TipoCuadrante
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_tcuadrante", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="tipo_cuadrante_id_tcuadrante_seq", allocationSize=1, initialValue=1)
     */
    private $idTcuadrante;

    /**
     * @var string
     *
     * @ORM\Column(name="tipo_cu", type="string", length=14, nullable=false)
     */
    private $tipoCu;



    /**
     * Get idTcuadrante
     *
     * @return integer 
     */
    public function getIdTcuadrante()
    {
        return $this->idTcuadrante;
    }

    /**
     * Set tipoCu
     *
     * @param string $tipoCu
     * @return TipoCuadrante
     */
    public function setTipoCu($tipoCu)
    {
        $this->tipoCu = $tipoCu;

        return $this;
    }

    /**
     * Get tipoCu
     *
     * @return string 
     */
    public function getTipoCu()
    {
        return $this->tipoCu;
    }
}
