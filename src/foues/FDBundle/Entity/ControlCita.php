<?php

namespace foues\FDBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ControlCita
 *
 * @ORM\Table(name="control_cita", uniqueConstraints={@ORM\UniqueConstraint(name="control_cita_pk", columns={"id_control"})}, indexes={@ORM\Index(name="fk_control__requiere__cita_fk", columns={"id_cita"})})
 * @ORM\Entity
 */
class ControlCita
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_control", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="control_cita_id_control_seq", allocationSize=1, initialValue=1)
     */
    private $idControl;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha_inicio", type="date", nullable=false)
     */
    private $fechaInicio;

    /**
     * @var string
     *
     * @ORM\Column(name="autorizado", type="string", length=20, nullable=false)
     */
    private $autorizado;

    /**
     * @var \Cita
     *
     * @ORM\ManyToOne(targetEntity="Cita")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_cita", referencedColumnName="id_cita")
     * })
     */
    private $idCita;



    /**
     * Get idControl
     *
     * @return integer 
     */
    public function getIdControl()
    {
        return $this->idControl;
    }

    /**
     * Set fechaInicio
     *
     * @param \DateTime $fechaInicio
     * @return ControlCita
     */
    public function setFechaInicio($fechaInicio)
    {
        $this->fechaInicio = $fechaInicio;

        return $this;
    }

    /**
     * Get fechaInicio
     *
     * @return \DateTime 
     */
    public function getFechaInicio()
    {
        return $this->fechaInicio;
    }

    /**
     * Set autorizado
     *
     * @param string $autorizado
     * @return ControlCita
     */
    public function setAutorizado($autorizado)
    {
        $this->autorizado = $autorizado;

        return $this;
    }

    /**
     * Get autorizado
     *
     * @return string 
     */
    public function getAutorizado()
    {
        return $this->autorizado;
    }

    /**
     * Set idCita
     *
     * @param \foues\FDBundle\Entity\Cita $idCita
     * @return ControlCita
     */
    public function setIdCita(\foues\FDBundle\Entity\Cita $idCita = null)
    {
        $this->idCita = $idCita;

        return $this;
    }

    /**
     * Get idCita
     *
     * @return \foues\FDBundle\Entity\Cita 
     */
    public function getIdCita()
    {
        return $this->idCita;
    }
}
