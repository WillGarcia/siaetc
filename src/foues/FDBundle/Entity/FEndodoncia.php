<?php

namespace foues\FDBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * FEndodoncia
 *
 * @ORM\Table(name="f_endodoncia", uniqueConstraints={@ORM\UniqueConstraint(name="f_endodoncia_pk", columns={"id_f_endo"})}, indexes={@ORM\Index(name="fk_f_endodo_posee_una_ficha_cl_", columns={"id_ficha"})})
 * @ORM\Entity
 */
class FEndodoncia
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_f_endo", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="f_endodoncia_id_f_endo_seq", allocationSize=1, initialValue=1)
     */
    private $idFEndo;

    /**
     * @var integer
     *
     * @ORM\Column(name="area_ficha", type="integer", nullable=false)
     */
    private $areaFicha;

    /**
     * @var string
     *
     * @ORM\Column(name="mot_con_endo", type="string", length=60, nullable=true)
     */
    private $motConEndo;

    /**
     * @var string
     *
     * @ORM\Column(name="hist_med_endo", type="string", length=60, nullable=true)
     */
    private $histMedEndo;

    /**
     * @var string
     *
     * @ORM\Column(name="hist_previa", type="string", length=60, nullable=true)
     */
    private $histPrevia;

    /**
     * @var boolean
     *
     * @ORM\Column(name="alta", type="boolean", nullable=true)
     */
    private $alta;

    /**
     * @var \FichaClinica
     *
     * @ORM\ManyToOne(targetEntity="FichaClinica")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_ficha", referencedColumnName="id_ficha")
     * })
     */
    private $idFicha;



    /**
     * Get idFEndo
     *
     * @return integer 
     */
    public function getIdFEndo()
    {
        return $this->idFEndo;
    }

    /**
     * Set areaFicha
     *
     * @param integer $areaFicha
     * @return FEndodoncia
     */
    public function setAreaFicha($areaFicha)
    {
        $this->areaFicha = $areaFicha;

        return $this;
    }

    /**
     * Get areaFicha
     *
     * @return integer 
     */
    public function getAreaFicha()
    {
        return $this->areaFicha;
    }

    /**
     * Set motConEndo
     *
     * @param string $motConEndo
     * @return FEndodoncia
     */
    public function setMotConEndo($motConEndo)
    {
        $this->motConEndo = $motConEndo;

        return $this;
    }

    /**
     * Get motConEndo
     *
     * @return string 
     */
    public function getMotConEndo()
    {
        return $this->motConEndo;
    }

    /**
     * Set histMedEndo
     *
     * @param string $histMedEndo
     * @return FEndodoncia
     */
    public function setHistMedEndo($histMedEndo)
    {
        $this->histMedEndo = $histMedEndo;

        return $this;
    }

    /**
     * Get histMedEndo
     *
     * @return string 
     */
    public function getHistMedEndo()
    {
        return $this->histMedEndo;
    }

    /**
     * Set histPrevia
     *
     * @param string $histPrevia
     * @return FEndodoncia
     */
    public function setHistPrevia($histPrevia)
    {
        $this->histPrevia = $histPrevia;

        return $this;
    }

    /**
     * Get histPrevia
     *
     * @return string 
     */
    public function getHistPrevia()
    {
        return $this->histPrevia;
    }

    /**
     * Set alta
     *
     * @param boolean $alta
     * @return FEndodoncia
     */
    public function setAlta($alta)
    {
        $this->alta = $alta;

        return $this;
    }

    /**
     * Get alta
     *
     * @return boolean 
     */
    public function getAlta()
    {
        return $this->alta;
    }

    /**
     * Set idFicha
     *
     * @param \foues\FDBundle\Entity\FichaClinica $idFicha
     * @return FEndodoncia
     */
    public function setIdFicha(\foues\FDBundle\Entity\FichaClinica $idFicha = null)
    {
        $this->idFicha = $idFicha;

        return $this;
    }

    /**
     * Get idFicha
     *
     * @return \foues\FDBundle\Entity\FichaClinica 
     */
    public function getIdFicha()
    {
        return $this->idFicha;
    }
}
