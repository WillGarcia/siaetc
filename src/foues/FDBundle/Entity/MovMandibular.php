<?php

namespace foues\FDBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * MovMandibular
 *
 * @ORM\Table(name="mov_mandibular", uniqueConstraints={@ORM\UniqueConstraint(name="mov_mandibular_pk", columns={"id_mov"})}, indexes={@ORM\Index(name="fk_mov_mand_registra__f_diagno_", columns={"id_f_dx"})})
 * @ORM\Entity
 */
class MovMandibular
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_mov", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="mov_mandibular_id_mov_seq", allocationSize=1, initialValue=1)
     */
    private $idMov;

    /**
     * @var integer
     *
     * @ORM\Column(name="an_longitud", type="integer", nullable=true)
     */
    private $anLongitud;

    /**
     * @var string
     *
     * @ORM\Column(name="an_facilidad", type="string", length=10, nullable=true)
     */
    private $anFacilidad;

    /**
     * @var boolean
     *
     * @ORM\Column(name="an_dolor", type="boolean", nullable=true)
     */
    private $anDolor;

    /**
     * @var string
     *
     * @ORM\Column(name="an_direccion", type="string", length=10, nullable=true)
     */
    private $anDireccion;

    /**
     * @var integer
     *
     * @ORM\Column(name="am_longitud", type="integer", nullable=true)
     */
    private $amLongitud;

    /**
     * @var string
     *
     * @ORM\Column(name="am_facilidad", type="string", length=10, nullable=true)
     */
    private $amFacilidad;

    /**
     * @var boolean
     *
     * @ORM\Column(name="am_dolor", type="boolean", nullable=true)
     */
    private $amDolor;

    /**
     * @var string
     *
     * @ORM\Column(name="am_direccion", type="string", length=10, nullable=true)
     */
    private $amDireccion;

    /**
     * @var integer
     *
     * @ORM\Column(name="li_longitud", type="integer", nullable=true)
     */
    private $liLongitud;

    /**
     * @var string
     *
     * @ORM\Column(name="li_facilidad", type="string", length=10, nullable=true)
     */
    private $liFacilidad;

    /**
     * @var boolean
     *
     * @ORM\Column(name="li_dolor", type="boolean", nullable=true)
     */
    private $liDolor;

    /**
     * @var integer
     *
     * @ORM\Column(name="ld_longitud", type="integer", nullable=true)
     */
    private $ldLongitud;

    /**
     * @var string
     *
     * @ORM\Column(name="ld_facilidad", type="string", length=10, nullable=true)
     */
    private $ldFacilidad;

    /**
     * @var boolean
     *
     * @ORM\Column(name="ld_dolor", type="boolean", nullable=true)
     */
    private $ldDolor;

    /**
     * @var \FDiagnostico
     *
     * @ORM\ManyToOne(targetEntity="FDiagnostico")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_f_dx", referencedColumnName="id_f_dx")
     * })
     */
    private $idFDx;



    /**
     * Get idMov
     *
     * @return integer 
     */
    public function getIdMov()
    {
        return $this->idMov;
    }

    /**
     * Set anLongitud
     *
     * @param integer $anLongitud
     * @return MovMandibular
     */
    public function setAnLongitud($anLongitud)
    {
        $this->anLongitud = $anLongitud;

        return $this;
    }

    /**
     * Get anLongitud
     *
     * @return integer 
     */
    public function getAnLongitud()
    {
        return $this->anLongitud;
    }

    /**
     * Set anFacilidad
     *
     * @param string $anFacilidad
     * @return MovMandibular
     */
    public function setAnFacilidad($anFacilidad)
    {
        $this->anFacilidad = $anFacilidad;

        return $this;
    }

    /**
     * Get anFacilidad
     *
     * @return string 
     */
    public function getAnFacilidad()
    {
        return $this->anFacilidad;
    }

    /**
     * Set anDolor
     *
     * @param boolean $anDolor
     * @return MovMandibular
     */
    public function setAnDolor($anDolor)
    {
        $this->anDolor = $anDolor;

        return $this;
    }

    /**
     * Get anDolor
     *
     * @return boolean 
     */
    public function getAnDolor()
    {
        return $this->anDolor;
    }

    /**
     * Set anDireccion
     *
     * @param string $anDireccion
     * @return MovMandibular
     */
    public function setAnDireccion($anDireccion)
    {
        $this->anDireccion = $anDireccion;

        return $this;
    }

    /**
     * Get anDireccion
     *
     * @return string 
     */
    public function getAnDireccion()
    {
        return $this->anDireccion;
    }

    /**
     * Set amLongitud
     *
     * @param integer $amLongitud
     * @return MovMandibular
     */
    public function setAmLongitud($amLongitud)
    {
        $this->amLongitud = $amLongitud;

        return $this;
    }

    /**
     * Get amLongitud
     *
     * @return integer 
     */
    public function getAmLongitud()
    {
        return $this->amLongitud;
    }

    /**
     * Set amFacilidad
     *
     * @param string $amFacilidad
     * @return MovMandibular
     */
    public function setAmFacilidad($amFacilidad)
    {
        $this->amFacilidad = $amFacilidad;

        return $this;
    }

    /**
     * Get amFacilidad
     *
     * @return string 
     */
    public function getAmFacilidad()
    {
        return $this->amFacilidad;
    }

    /**
     * Set amDolor
     *
     * @param boolean $amDolor
     * @return MovMandibular
     */
    public function setAmDolor($amDolor)
    {
        $this->amDolor = $amDolor;

        return $this;
    }

    /**
     * Get amDolor
     *
     * @return boolean 
     */
    public function getAmDolor()
    {
        return $this->amDolor;
    }

    /**
     * Set amDireccion
     *
     * @param string $amDireccion
     * @return MovMandibular
     */
    public function setAmDireccion($amDireccion)
    {
        $this->amDireccion = $amDireccion;

        return $this;
    }

    /**
     * Get amDireccion
     *
     * @return string 
     */
    public function getAmDireccion()
    {
        return $this->amDireccion;
    }

    /**
     * Set liLongitud
     *
     * @param integer $liLongitud
     * @return MovMandibular
     */
    public function setLiLongitud($liLongitud)
    {
        $this->liLongitud = $liLongitud;

        return $this;
    }

    /**
     * Get liLongitud
     *
     * @return integer 
     */
    public function getLiLongitud()
    {
        return $this->liLongitud;
    }

    /**
     * Set liFacilidad
     *
     * @param string $liFacilidad
     * @return MovMandibular
     */
    public function setLiFacilidad($liFacilidad)
    {
        $this->liFacilidad = $liFacilidad;

        return $this;
    }

    /**
     * Get liFacilidad
     *
     * @return string 
     */
    public function getLiFacilidad()
    {
        return $this->liFacilidad;
    }

    /**
     * Set liDolor
     *
     * @param boolean $liDolor
     * @return MovMandibular
     */
    public function setLiDolor($liDolor)
    {
        $this->liDolor = $liDolor;

        return $this;
    }

    /**
     * Get liDolor
     *
     * @return boolean 
     */
    public function getLiDolor()
    {
        return $this->liDolor;
    }

    /**
     * Set ldLongitud
     *
     * @param integer $ldLongitud
     * @return MovMandibular
     */
    public function setLdLongitud($ldLongitud)
    {
        $this->ldLongitud = $ldLongitud;

        return $this;
    }

    /**
     * Get ldLongitud
     *
     * @return integer 
     */
    public function getLdLongitud()
    {
        return $this->ldLongitud;
    }

    /**
     * Set ldFacilidad
     *
     * @param string $ldFacilidad
     * @return MovMandibular
     */
    public function setLdFacilidad($ldFacilidad)
    {
        $this->ldFacilidad = $ldFacilidad;

        return $this;
    }

    /**
     * Get ldFacilidad
     *
     * @return string 
     */
    public function getLdFacilidad()
    {
        return $this->ldFacilidad;
    }

    /**
     * Set ldDolor
     *
     * @param boolean $ldDolor
     * @return MovMandibular
     */
    public function setLdDolor($ldDolor)
    {
        $this->ldDolor = $ldDolor;

        return $this;
    }

    /**
     * Get ldDolor
     *
     * @return boolean 
     */
    public function getLdDolor()
    {
        return $this->ldDolor;
    }

    /**
     * Set idFDx
     *
     * @param \foues\FDBundle\Entity\FDiagnostico $idFDx
     * @return MovMandibular
     */
    public function setIdFDx(\foues\FDBundle\Entity\FDiagnostico $idFDx = null)
    {
        $this->idFDx = $idFDx;

        return $this;
    }

    /**
     * Get idFDx
     *
     * @return \foues\FDBundle\Entity\FDiagnostico 
     */
    public function getIdFDx()
    {
        return $this->idFDx;
    }
}
