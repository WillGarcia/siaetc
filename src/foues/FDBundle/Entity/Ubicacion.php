<?php

namespace foues\FDBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Ubicacion
 *
 * @ORM\Table(name="ubicacion", uniqueConstraints={@ORM\UniqueConstraint(name="ubicacion_pk", columns={"id_ubicacion"})})
 * @ORM\Entity
 */
class Ubicacion
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_ubicacion", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="ubicacion_id_ubicacion_seq", allocationSize=1, initialValue=1)
     */
    private $idUbicacion;

    /**
     * @var string
     *
     * @ORM\Column(name="nom_ubicacion", type="string", length=20, nullable=false)
     */
    private $nomUbicacion;

    /**
     * @var integer
     *
     * @ORM\Column(name="capacidad", type="integer", nullable=false)
     */
    private $capacidad;



    /**
     * Get idUbicacion
     *
     * @return integer 
     */
    public function getIdUbicacion()
    {
        return $this->idUbicacion;
    }

    /**
     * Set nomUbicacion
     *
     * @param string $nomUbicacion
     * @return Ubicacion
     */
    public function setNomUbicacion($nomUbicacion)
    {
        $this->nomUbicacion = $nomUbicacion;

        return $this;
    }

    /**
     * Get nomUbicacion
     *
     * @return string 
     */
    public function getNomUbicacion()
    {
        return $this->nomUbicacion;
    }

    /**
     * Set capacidad
     *
     * @param integer $capacidad
     * @return Ubicacion
     */
    public function setCapacidad($capacidad)
    {
        $this->capacidad = $capacidad;

        return $this;
    }

    /**
     * Get capacidad
     *
     * @return integer 
     */
    public function getCapacidad()
    {
        return $this->capacidad;
    }
}
