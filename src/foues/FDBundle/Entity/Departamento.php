<?php

namespace foues\FDBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Departamento
 *
 * @ORM\Table(name="departamento")
 * @ORM\Entity
 */
class Departamento
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_departamento", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="departamento_id_departamento_seq", allocationSize=1, initialValue=1)
     */
    private $idDepartamento;

    /**
     * @var string
     *
     * @ORM\Column(name="nom_departamento", type="string", length=30, nullable=false)
     */
    private $nomDepartamento;



    /**
     * Get idDepartamento
     *
     * @return integer 
     */
    public function getIdDepartamento()
    {
        return $this->idDepartamento;
    }

    /**
     * Set nomDepartamento
     *
     * @param string $nomDepartamento
     * @return Departamento
     */
    public function setNomDepartamento($nomDepartamento)
    {
        $this->nomDepartamento = $nomDepartamento;

        return $this;
    }

    /**
     * Get nomDepartamento
     *
     * @return string 
     */
    public function getNomDepartamento()
    {
        return $this->nomDepartamento;
    }
}
