<?php

namespace foues\FDBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ImagenRemSuperior
 *
 * @ORM\Table(name="imagen_rem_superior", uniqueConstraints={@ORM\UniqueConstraint(name="imagen_rem_superior_pk", columns={"id_image_s"})}, indexes={@ORM\Index(name="fk_imagen_r_dibuja_f_restau_fk", columns={"id_f_resta"})})
 * @ORM\Entity
 */
class ImagenRemSuperior
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_image_s", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="imagen_rem_superior_id_image_s_seq", allocationSize=1, initialValue=1)
     */
    private $idImageS;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre_image", type="string", length=40, nullable=true)
     */
    private $nombreImage;

    /**
     * @var \FRestaurativa
     *
     * @ORM\ManyToOne(targetEntity="FRestaurativa")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_f_resta", referencedColumnName="id_f_resta")
     * })
     */
    private $idFResta;



    /**
     * Get idImageS
     *
     * @return integer 
     */
    public function getIdImageS()
    {
        return $this->idImageS;
    }

    /**
     * Set nombreImage
     *
     * @param string $nombreImage
     * @return ImagenRemSuperior
     */
    public function setNombreImage($nombreImage)
    {
        $this->nombreImage = $nombreImage;

        return $this;
    }

    /**
     * Get nombreImage
     *
     * @return string 
     */
    public function getNombreImage()
    {
        return $this->nombreImage;
    }

    /**
     * Set idFResta
     *
     * @param \foues\FDBundle\Entity\FRestaurativa $idFResta
     * @return ImagenRemSuperior
     */
    public function setIdFResta(\foues\FDBundle\Entity\FRestaurativa $idFResta = null)
    {
        $this->idFResta = $idFResta;

        return $this;
    }

    /**
     * Get idFResta
     *
     * @return \foues\FDBundle\Entity\FRestaurativa 
     */
    public function getIdFResta()
    {
        return $this->idFResta;
    }
}
