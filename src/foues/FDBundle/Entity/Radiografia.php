<?php

namespace foues\FDBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Radiografia
 *
 * @ORM\Table(name="radiografia", uniqueConstraints={@ORM\UniqueConstraint(name="radiografia_pk", columns={"id_radiografia"})}, indexes={@ORM\Index(name="fk_radiogra_demanda_f_diagno_fk", columns={"id_f_dx"})})
 * @ORM\Entity
 */
class Radiografia
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_radiografia", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="radiografia_id_radiografia_seq", allocationSize=1, initialValue=1)
     */
    private $idRadiografia;

    /**
     * @var string
     *
     * @ORM\Column(name="nom_image", type="string", length=100, nullable=true)
     */
    private $nomImage;

    /**
     * @var string
     *
     * @ORM\Column(name="hallazgo_rad", type="string", length=100, nullable=true)
     */
    private $hallazgoRad;

    /**
     * @var \FDiagnostico
     *
     * @ORM\ManyToOne(targetEntity="FDiagnostico")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_f_dx", referencedColumnName="id_f_dx")
     * })
     */
    private $idFDx;



    /**
     * Get idRadiografia
     *
     * @return integer 
     */
    public function getIdRadiografia()
    {
        return $this->idRadiografia;
    }

    /**
     * Set nomImage
     *
     * @param string $nomImage
     * @return Radiografia
     */
    public function setNomImage($nomImage)
    {
        $this->nomImage = $nomImage;

        return $this;
    }

    /**
     * Get nomImage
     *
     * @return string 
     */
    public function getNomImage()
    {
        return $this->nomImage;
    }

    /**
     * Set hallazgoRad
     *
     * @param string $hallazgoRad
     * @return Radiografia
     */
    public function setHallazgoRad($hallazgoRad)
    {
        $this->hallazgoRad = $hallazgoRad;

        return $this;
    }

    /**
     * Get hallazgoRad
     *
     * @return string 
     */
    public function getHallazgoRad()
    {
        return $this->hallazgoRad;
    }

    /**
     * Set idFDx
     *
     * @param \foues\FDBundle\Entity\FDiagnostico $idFDx
     * @return Radiografia
     */
    public function setIdFDx(\foues\FDBundle\Entity\FDiagnostico $idFDx = null)
    {
        $this->idFDx = $idFDx;

        return $this;
    }

    /**
     * Get idFDx
     *
     * @return \foues\FDBundle\Entity\FDiagnostico 
     */
    public function getIdFDx()
    {
        return $this->idFDx;
    }
}
