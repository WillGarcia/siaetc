<?php

namespace foues\FDBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Periodontograma
 *
 * @ORM\Table(name="periodontograma", indexes={@ORM\Index(name="IDX_436BF29F9D224214", columns={"id_f_perio"})})
 * @ORM\Entity
 */
class Periodontograma
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_perio", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="periodontograma_id_perio_seq", allocationSize=1, initialValue=1)
     */
    private $idPerio;

    /**
     * @var \FPeriodoncia
     *
     * @ORM\ManyToOne(targetEntity="FPeriodoncia")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_f_perio", referencedColumnName="id_f_perio")
     * })
     */
    private $idFPerio;



    /**
     * Get idPerio
     *
     * @return integer 
     */
    public function getIdPerio()
    {
        return $this->idPerio;
    }

    /**
     * Set idFPerio
     *
     * @param \foues\FDBundle\Entity\FPeriodoncia $idFPerio
     * @return Periodontograma
     */
    public function setIdFPerio(\foues\FDBundle\Entity\FPeriodoncia $idFPerio = null)
    {
        $this->idFPerio = $idFPerio;

        return $this;
    }

    /**
     * Get idFPerio
     *
     * @return \foues\FDBundle\Entity\FPeriodoncia 
     */
    public function getIdFPerio()
    {
        return $this->idFPerio;
    }
}
