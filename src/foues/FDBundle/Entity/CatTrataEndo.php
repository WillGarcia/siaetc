<?php

namespace foues\FDBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CatTrataEndo
 *
 * @ORM\Table(name="cat_trata_endo", uniqueConstraints={@ORM\UniqueConstraint(name="cat_trata_endo_pk", columns={"id_cat_t_endo"})})
 * @ORM\Entity
 */
class CatTrataEndo
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_cat_t_endo", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="cat_trata_endo_id_cat_t_endo_seq", allocationSize=1, initialValue=1)
     */
    private $idCatTEndo;

    /**
     * @var string
     *
     * @ORM\Column(name="nom_trata", type="string", length=60, nullable=true)
     */
    private $nomTrata;



    /**
     * Get idCatTEndo
     *
     * @return integer 
     */
    public function getIdCatTEndo()
    {
        return $this->idCatTEndo;
    }

    /**
     * Set nomTrata
     *
     * @param string $nomTrata
     * @return CatTrataEndo
     */
    public function setNomTrata($nomTrata)
    {
        $this->nomTrata = $nomTrata;

        return $this;
    }

    /**
     * Get nomTrata
     *
     * @return string 
     */
    public function getNomTrata()
    {
        return $this->nomTrata;
    }
}
