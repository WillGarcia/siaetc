<?php

namespace foues\FDBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ExaIntraoral
 *
 * @ORM\Table(name="exa_intraoral", uniqueConstraints={@ORM\UniqueConstraint(name="exa_intraoral_pk", columns={"id_intraoral"})}, indexes={@ORM\Index(name="fk_exa_intr_necesita4_f_diagno_", columns={"id_f_dx"})})
 * @ORM\Entity
 */
class ExaIntraoral
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_intraoral", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="exa_intraoral_id_intraoral_seq", allocationSize=1, initialValue=1)
     */
    private $idIntraoral;

    /**
     * @var string
     *
     * @ORM\Column(name="les_labios", type="string", length=150, nullable=true)
     */
    private $lesLabios;

    /**
     * @var string
     *
     * @ORM\Column(name="les_carri", type="string", length=150, nullable=true)
     */
    private $lesCarri;

    /**
     * @var string
     *
     * @ORM\Column(name="les_vevsti", type="string", length=150, nullable=true)
     */
    private $lesVevsti;

    /**
     * @var string
     *
     * @ORM\Column(name="les_freni", type="string", length=150, nullable=true)
     */
    private $lesFreni;

    /**
     * @var string
     *
     * @ORM\Column(name="les_leng", type="string", length=150, nullable=true)
     */
    private $lesLeng;

    /**
     * @var string
     *
     * @ORM\Column(name="les_pboca", type="string", length=150, nullable=true)
     */
    private $lesPboca;

    /**
     * @var string
     *
     * @ORM\Column(name="les_palduro", type="string", length=150, nullable=true)
     */
    private $lesPalduro;

    /**
     * @var string
     *
     * @ORM\Column(name="les_palblan", type="string", length=150, nullable=true)
     */
    private $lesPalblan;

    /**
     * @var string
     *
     * @ORM\Column(name="les_orof", type="string", length=150, nullable=true)
     */
    private $lesOrof;

    /**
     * @var string
     *
     * @ORM\Column(name="les_encia", type="string", length=150, nullable=true)
     */
    private $lesEncia;

    /**
     * @var string
     *
     * @ORM\Column(name="bolsas_perio", type="string", length=200, nullable=true)
     */
    private $bolsasPerio;

    /**
     * @var string
     *
     * @ORM\Column(name="exa_diente", type="string", length=500, nullable=true)
     */
    private $exaDiente;

    /**
     * @var \FDiagnostico
     *
     * @ORM\ManyToOne(targetEntity="FDiagnostico")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_f_dx", referencedColumnName="id_f_dx")
     * })
     */
    private $idFDx;



    /**
     * Get idIntraoral
     *
     * @return integer 
     */
    public function getIdIntraoral()
    {
        return $this->idIntraoral;
    }

    /**
     * Set lesLabios
     *
     * @param string $lesLabios
     * @return ExaIntraoral
     */
    public function setLesLabios($lesLabios)
    {
        $this->lesLabios = $lesLabios;

        return $this;
    }

    /**
     * Get lesLabios
     *
     * @return string 
     */
    public function getLesLabios()
    {
        return $this->lesLabios;
    }

    /**
     * Set lesCarri
     *
     * @param string $lesCarri
     * @return ExaIntraoral
     */
    public function setLesCarri($lesCarri)
    {
        $this->lesCarri = $lesCarri;

        return $this;
    }

    /**
     * Get lesCarri
     *
     * @return string 
     */
    public function getLesCarri()
    {
        return $this->lesCarri;
    }

    /**
     * Set lesVevsti
     *
     * @param string $lesVevsti
     * @return ExaIntraoral
     */
    public function setLesVevsti($lesVevsti)
    {
        $this->lesVevsti = $lesVevsti;

        return $this;
    }

    /**
     * Get lesVevsti
     *
     * @return string 
     */
    public function getLesVevsti()
    {
        return $this->lesVevsti;
    }

    /**
     * Set lesFreni
     *
     * @param string $lesFreni
     * @return ExaIntraoral
     */
    public function setLesFreni($lesFreni)
    {
        $this->lesFreni = $lesFreni;

        return $this;
    }

    /**
     * Get lesFreni
     *
     * @return string 
     */
    public function getLesFreni()
    {
        return $this->lesFreni;
    }

    /**
     * Set lesLeng
     *
     * @param string $lesLeng
     * @return ExaIntraoral
     */
    public function setLesLeng($lesLeng)
    {
        $this->lesLeng = $lesLeng;

        return $this;
    }

    /**
     * Get lesLeng
     *
     * @return string 
     */
    public function getLesLeng()
    {
        return $this->lesLeng;
    }

    /**
     * Set lesPboca
     *
     * @param string $lesPboca
     * @return ExaIntraoral
     */
    public function setLesPboca($lesPboca)
    {
        $this->lesPboca = $lesPboca;

        return $this;
    }

    /**
     * Get lesPboca
     *
     * @return string 
     */
    public function getLesPboca()
    {
        return $this->lesPboca;
    }

    /**
     * Set lesPalduro
     *
     * @param string $lesPalduro
     * @return ExaIntraoral
     */
    public function setLesPalduro($lesPalduro)
    {
        $this->lesPalduro = $lesPalduro;

        return $this;
    }

    /**
     * Get lesPalduro
     *
     * @return string 
     */
    public function getLesPalduro()
    {
        return $this->lesPalduro;
    }

    /**
     * Set lesPalblan
     *
     * @param string $lesPalblan
     * @return ExaIntraoral
     */
    public function setLesPalblan($lesPalblan)
    {
        $this->lesPalblan = $lesPalblan;

        return $this;
    }

    /**
     * Get lesPalblan
     *
     * @return string 
     */
    public function getLesPalblan()
    {
        return $this->lesPalblan;
    }

    /**
     * Set lesOrof
     *
     * @param string $lesOrof
     * @return ExaIntraoral
     */
    public function setLesOrof($lesOrof)
    {
        $this->lesOrof = $lesOrof;

        return $this;
    }

    /**
     * Get lesOrof
     *
     * @return string 
     */
    public function getLesOrof()
    {
        return $this->lesOrof;
    }

    /**
     * Set lesEncia
     *
     * @param string $lesEncia
     * @return ExaIntraoral
     */
    public function setLesEncia($lesEncia)
    {
        $this->lesEncia = $lesEncia;

        return $this;
    }

    /**
     * Get lesEncia
     *
     * @return string 
     */
    public function getLesEncia()
    {
        return $this->lesEncia;
    }

    /**
     * Set bolsasPerio
     *
     * @param string $bolsasPerio
     * @return ExaIntraoral
     */
    public function setBolsasPerio($bolsasPerio)
    {
        $this->bolsasPerio = $bolsasPerio;

        return $this;
    }

    /**
     * Get bolsasPerio
     *
     * @return string 
     */
    public function getBolsasPerio()
    {
        return $this->bolsasPerio;
    }

    /**
     * Set exaDiente
     *
     * @param string $exaDiente
     * @return ExaIntraoral
     */
    public function setExaDiente($exaDiente)
    {
        $this->exaDiente = $exaDiente;

        return $this;
    }

    /**
     * Get exaDiente
     *
     * @return string 
     */
    public function getExaDiente()
    {
        return $this->exaDiente;
    }

    /**
     * Set idFDx
     *
     * @param \foues\FDBundle\Entity\FDiagnostico $idFDx
     * @return ExaIntraoral
     */
    public function setIdFDx(\foues\FDBundle\Entity\FDiagnostico $idFDx = null)
    {
        $this->idFDx = $idFDx;

        return $this;
    }

    /**
     * Get idFDx
     *
     * @return \foues\FDBundle\Entity\FDiagnostico 
     */
    public function getIdFDx()
    {
        return $this->idFDx;
    }
}
