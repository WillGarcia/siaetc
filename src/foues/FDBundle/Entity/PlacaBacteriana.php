<?php

namespace foues\FDBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PlacaBacteriana
 *
 * @ORM\Table(name="placa_bacteriana", uniqueConstraints={@ORM\UniqueConstraint(name="placa_bacteriana_pk", columns={"id_pb"})}, indexes={@ORM\Index(name="fk_placa_bacteriana_f_dx_fk", columns={"id_f_dx"})})
 * @ORM\Entity
 */
class PlacaBacteriana
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_pb", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="placa_bacteriana_id_pb_seq", allocationSize=1, initialValue=1)
     */
    private $idPb;

    /**
     * @var integer
     *
     * @ORM\Column(name="ci_1_6", type="integer", nullable=true)
     */
    private $ci16;

    /**
     * @var integer
     *
     * @ORM\Column(name="ci_1_1", type="integer", nullable=true)
     */
    private $ci11;

    /**
     * @var integer
     *
     * @ORM\Column(name="ci_3_6", type="integer", nullable=true)
     */
    private $ci36;

    /**
     * @var integer
     *
     * @ORM\Column(name="ci_3_1", type="integer", nullable=true)
     */
    private $ci31;

    /**
     * @var integer
     *
     * @ORM\Column(name="ci_2_6", type="integer", nullable=true)
     */
    private $ci26;

    /**
     * @var integer
     *
     * @ORM\Column(name="ci_4_6", type="integer", nullable=true)
     */
    private $ci46;

    /**
     * @var integer
     *
     * @ORM\Column(name="num_sup", type="integer", nullable=false)
     */
    private $numSup;

    /**
     * @var float
     *
     * @ORM\Column(name="resultado", type="float", precision=10, scale=0, nullable=false)
     */
    private $resultado;

    /**
     * @var \FDiagnostico
     *
     * @ORM\ManyToOne(targetEntity="FDiagnostico")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_f_dx", referencedColumnName="id_f_dx")
     * })
     */
    private $idFDx;



    /**
     * Get idPb
     *
     * @return integer 
     */
    public function getIdPb()
    {
        return $this->idPb;
    }

    /**
     * Set ci16
     *
     * @param integer $ci16
     * @return PlacaBacteriana
     */
    public function setCi16($ci16)
    {
        $this->ci16 = $ci16;

        return $this;
    }

    /**
     * Get ci16
     *
     * @return integer 
     */
    public function getCi16()
    {
        return $this->ci16;
    }

    /**
     * Set ci11
     *
     * @param integer $ci11
     * @return PlacaBacteriana
     */
    public function setCi11($ci11)
    {
        $this->ci11 = $ci11;

        return $this;
    }

    /**
     * Get ci11
     *
     * @return integer 
     */
    public function getCi11()
    {
        return $this->ci11;
    }

    /**
     * Set ci36
     *
     * @param integer $ci36
     * @return PlacaBacteriana
     */
    public function setCi36($ci36)
    {
        $this->ci36 = $ci36;

        return $this;
    }

    /**
     * Get ci36
     *
     * @return integer 
     */
    public function getCi36()
    {
        return $this->ci36;
    }

    /**
     * Set ci31
     *
     * @param integer $ci31
     * @return PlacaBacteriana
     */
    public function setCi31($ci31)
    {
        $this->ci31 = $ci31;

        return $this;
    }

    /**
     * Get ci31
     *
     * @return integer 
     */
    public function getCi31()
    {
        return $this->ci31;
    }

    /**
     * Set ci26
     *
     * @param integer $ci26
     * @return PlacaBacteriana
     */
    public function setCi26($ci26)
    {
        $this->ci26 = $ci26;

        return $this;
    }

    /**
     * Get ci26
     *
     * @return integer 
     */
    public function getCi26()
    {
        return $this->ci26;
    }

    /**
     * Set ci46
     *
     * @param integer $ci46
     * @return PlacaBacteriana
     */
    public function setCi46($ci46)
    {
        $this->ci46 = $ci46;

        return $this;
    }

    /**
     * Get ci46
     *
     * @return integer 
     */
    public function getCi46()
    {
        return $this->ci46;
    }

    /**
     * Set numSup
     *
     * @param integer $numSup
     * @return PlacaBacteriana
     */
    public function setNumSup($numSup)
    {
        $this->numSup = $numSup;

        return $this;
    }

    /**
     * Get numSup
     *
     * @return integer 
     */
    public function getNumSup()
    {
        return $this->numSup;
    }

    /**
     * Set resultado
     *
     * @param float $resultado
     * @return PlacaBacteriana
     */
    public function setResultado($resultado)
    {
        $this->resultado = $resultado;

        return $this;
    }

    /**
     * Get resultado
     *
     * @return float 
     */
    public function getResultado()
    {
        return $this->resultado;
    }

    /**
     * Set idFDx
     *
     * @param \foues\FDBundle\Entity\FDiagnostico $idFDx
     * @return PlacaBacteriana
     */
    public function setIdFDx(\foues\FDBundle\Entity\FDiagnostico $idFDx = null)
    {
        $this->idFDx = $idFDx;

        return $this;
    }

    /**
     * Get idFDx
     *
     * @return \foues\FDBundle\Entity\FDiagnostico 
     */
    public function getIdFDx()
    {
        return $this->idFDx;
    }
}
