<?php

namespace foues\FDBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Devolucion
 *
 * @ORM\Table(name="devolucion", uniqueConstraints={@ORM\UniqueConstraint(name="devolucion_pk", columns={"id_devolucion"})}, indexes={@ORM\Index(name="fk_devoluci_genera2_prestamo_fk", columns={"id_prestamo"})})
 * @ORM\Entity
 */
class Devolucion
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_devolucion", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="devolucion_id_devolucion_seq", allocationSize=1, initialValue=1)
     */
    private $idDevolucion;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha_dev", type="date", nullable=false)
     */
    private $fechaDev;

    /**
     * @var \Prestamo
     *
     * @ORM\ManyToOne(targetEntity="Prestamo")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_prestamo", referencedColumnName="id_prestamo")
     * })
     */
    private $idPrestamo;



    /**
     * Get idDevolucion
     *
     * @return integer 
     */
    public function getIdDevolucion()
    {
        return $this->idDevolucion;
    }

    /**
     * Set fechaDev
     *
     * @param \DateTime $fechaDev
     * @return Devolucion
     */
    public function setFechaDev($fechaDev)
    {
        $this->fechaDev = $fechaDev;

        return $this;
    }

    /**
     * Get fechaDev
     *
     * @return \DateTime 
     */
    public function getFechaDev()
    {
        return $this->fechaDev;
    }

    /**
     * Set idPrestamo
     *
     * @param \foues\FDBundle\Entity\Prestamo $idPrestamo
     * @return Devolucion
     */
    public function setIdPrestamo(\foues\FDBundle\Entity\Prestamo $idPrestamo = null)
    {
        $this->idPrestamo = $idPrestamo;

        return $this;
    }

    /**
     * Get idPrestamo
     *
     * @return \foues\FDBundle\Entity\Prestamo 
     */
    public function getIdPrestamo()
    {
        return $this->idPrestamo;
    }
}
