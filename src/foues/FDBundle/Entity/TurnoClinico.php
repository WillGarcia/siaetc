<?php

namespace foues\FDBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TurnoClinico
 *
 * @ORM\Table(name="turno_clinico", uniqueConstraints={@ORM\UniqueConstraint(name="turno_clinico_pk", columns={"id_turno"})}, indexes={@ORM\Index(name="fk_turno_cl_imparte_ubicacio_fk", columns={"id_ubicacion"}), @ORM\Index(name="IDX_F1046ECD230266D4", columns={"id_docente"})})
 * @ORM\Entity
 */
class TurnoClinico
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_turno", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="turno_clinico_id_turno_seq", allocationSize=1, initialValue=1)
     */
    private $idTurno;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre_turno", type="string", length=50, nullable=false)
     */
    private $nombreTurno;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha_inicio", type="date", nullable=false)
     */
    private $fechaInicio;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha_fin", type="date", nullable=false)
     */
    private $fechaFin;

    /**
     * @var string
     *
     * @ORM\Column(name="dia", type="text", nullable=false)
     */
    private $dia;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="hora_inicio", type="date", nullable=false)
     */
    private $horaInicio;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="hora_fin", type="date", nullable=false)
     */
    private $horaFin;

    /**
     * @var integer
     *
     * @ORM\Column(name="cupo", type="integer", nullable=false)
     */
    private $cupo;

    /**
     * @var integer
     *
     * @ORM\Column(name="prerrequisito", type="integer", nullable=false)
     */
    private $prerrequisito;

    /**
     * @var string
     *
     * @ORM\Column(name="descripcion", type="string", length=100, nullable=true)
     */
    private $descripcion;

    /**
     * @var \Ubicacion
     *
     * @ORM\ManyToOne(targetEntity="Ubicacion")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_ubicacion", referencedColumnName="id_ubicacion")
     * })
     */
    private $idUbicacion;

    /**
     * @var \Docente
     *
     * @ORM\ManyToOne(targetEntity="Docente")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_docente", referencedColumnName="id_docente")
     * })
     */
    private $idDocente;



    /**
     * Get idTurno
     *
     * @return integer 
     */
    public function getIdTurno()
    {
        return $this->idTurno;
    }

    /**
     * Set nombreTurno
     *
     * @param string $nombreTurno
     * @return TurnoClinico
     */
    public function setNombreTurno($nombreTurno)
    {
        $this->nombreTurno = $nombreTurno;

        return $this;
    }

    /**
     * Get nombreTurno
     *
     * @return string 
     */
    public function getNombreTurno()
    {
        return $this->nombreTurno;
    }

    /**
     * Set fechaInicio
     *
     * @param \DateTime $fechaInicio
     * @return TurnoClinico
     */
    public function setFechaInicio($fechaInicio)
    {
        $this->fechaInicio = $fechaInicio;

        return $this;
    }

    /**
     * Get fechaInicio
     *
     * @return \DateTime 
     */
    public function getFechaInicio()
    {
        return $this->fechaInicio;
    }

    /**
     * Set fechaFin
     *
     * @param \DateTime $fechaFin
     * @return TurnoClinico
     */
    public function setFechaFin($fechaFin)
    {
        $this->fechaFin = $fechaFin;

        return $this;
    }

    /**
     * Get fechaFin
     *
     * @return \DateTime 
     */
    public function getFechaFin()
    {
        return $this->fechaFin;
    }

    /**
     * Set dia
     *
     * @param string $dia
     * @return TurnoClinico
     */
    public function setDia($dia)
    {
        $this->dia = $dia;

        return $this;
    }

    /**
     * Get dia
     *
     * @return string 
     */
    public function getDia()
    {
        return $this->dia;
    }

    /**
     * Set horaInicio
     *
     * @param \DateTime $horaInicio
     * @return TurnoClinico
     */
    public function setHoraInicio($horaInicio)
    {
        $this->horaInicio = $horaInicio;

        return $this;
    }

    /**
     * Get horaInicio
     *
     * @return \DateTime 
     */
    public function getHoraInicio()
    {
        return $this->horaInicio;
    }

    /**
     * Set horaFin
     *
     * @param \DateTime $horaFin
     * @return TurnoClinico
     */
    public function setHoraFin($horaFin)
    {
        $this->horaFin = $horaFin;

        return $this;
    }

    /**
     * Get horaFin
     *
     * @return \DateTime 
     */
    public function getHoraFin()
    {
        return $this->horaFin;
    }

    /**
     * Set cupo
     *
     * @param integer $cupo
     * @return TurnoClinico
     */
    public function setCupo($cupo)
    {
        $this->cupo = $cupo;

        return $this;
    }

    /**
     * Get cupo
     *
     * @return integer 
     */
    public function getCupo()
    {
        return $this->cupo;
    }

    /**
     * Set prerrequisito
     *
     * @param integer $prerrequisito
     * @return TurnoClinico
     */
    public function setPrerrequisito($prerrequisito)
    {
        $this->prerrequisito = $prerrequisito;

        return $this;
    }

    /**
     * Get prerrequisito
     *
     * @return integer 
     */
    public function getPrerrequisito()
    {
        return $this->prerrequisito;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     * @return TurnoClinico
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string 
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set idUbicacion
     *
     * @param \foues\FDBundle\Entity\Ubicacion $idUbicacion
     * @return TurnoClinico
     */
    public function setIdUbicacion(\foues\FDBundle\Entity\Ubicacion $idUbicacion = null)
    {
        $this->idUbicacion = $idUbicacion;

        return $this;
    }

    /**
     * Get idUbicacion
     *
     * @return \foues\FDBundle\Entity\Ubicacion 
     */
    public function getIdUbicacion()
    {
        return $this->idUbicacion;
    }

    /**
     * Set idDocente
     *
     * @param \foues\FDBundle\Entity\Docente $idDocente
     * @return TurnoClinico
     */
    public function setIdDocente(\foues\FDBundle\Entity\Docente $idDocente = null)
    {
        $this->idDocente = $idDocente;

        return $this;
    }

    /**
     * Get idDocente
     *
     * @return \foues\FDBundle\Entity\Docente 
     */
    public function getIdDocente()
    {
        return $this->idDocente;
    }
}
