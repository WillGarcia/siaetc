<?php

namespace foues\FDBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AreaClinica
 *
 * @ORM\Table(name="area_clinica", uniqueConstraints={@ORM\UniqueConstraint(name="area_clinica_pk", columns={"id_area"})})
 * @ORM\Entity
 */
class AreaClinica
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_area", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="area_clinica_id_area_seq", allocationSize=1, initialValue=1)
     */
    private $idArea;

    /**
     * @var string
     *
     * @ORM\Column(name="nom_area", type="string", length=15, nullable=false)
     */
    private $nomArea;



    /**
     * Get idArea
     *
     * @return integer 
     */
    public function getIdArea()
    {
        return $this->idArea;
    }

    /**
     * Set nomArea
     *
     * @param string $nomArea
     * @return AreaClinica
     */
    public function setNomArea($nomArea)
    {
        $this->nomArea = $nomArea;

        return $this;
    }

    /**
     * Get nomArea
     *
     * @return string 
     */
    public function getNomArea()
    {
        return $this->nomArea;
    }
}
