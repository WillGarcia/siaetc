<?php

namespace foues\FDBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Ciclo
 *
 * @ORM\Table(name="ciclo", uniqueConstraints={@ORM\UniqueConstraint(name="ciclo_pk", columns={"id_ciclo"})})
 * @ORM\Entity
 */
class Ciclo
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_ciclo", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="ciclo_id_ciclo_seq", allocationSize=1, initialValue=1)
     */
    private $idCiclo;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre_ciclo", type="string", length=10, nullable=false)
     */
    private $nombreCiclo;



    /**
     * Get idCiclo
     *
     * @return integer 
     */
    public function getIdCiclo()
    {
        return $this->idCiclo;
    }

    /**
     * Set nombreCiclo
     *
     * @param string $nombreCiclo
     * @return Ciclo
     */
    public function setNombreCiclo($nombreCiclo)
    {
        $this->nombreCiclo = $nombreCiclo;

        return $this;
    }

    /**
     * Get nombreCiclo
     *
     * @return string 
     */
    public function getNombreCiclo()
    {
        return $this->nombreCiclo;
    }
}
