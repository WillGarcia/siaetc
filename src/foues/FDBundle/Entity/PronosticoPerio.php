<?php

namespace foues\FDBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PronosticoPerio
 *
 * @ORM\Table(name="pronostico_perio", uniqueConstraints={@ORM\UniqueConstraint(name="pronostico_perio_pk", columns={"id_pro_gral"})}, indexes={@ORM\Index(name="fk_pronosti_realiza_p_f_period_", columns={"id_f_perio"})})
 * @ORM\Entity
 */
class PronosticoPerio
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_pro_gral", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="pronostico_perio_id_pro_gral_seq", allocationSize=1, initialValue=1)
     */
    private $idProGral;

    /**
     * @var string
     *
     * @ORM\Column(name="prono_gral", type="string", length=20, nullable=true)
     */
    private $pronoGral;

    /**
     * @var string
     *
     * @ORM\Column(name="maxilar", type="string", length=20, nullable=true)
     */
    private $maxilar;

    /**
     * @var string
     *
     * @ORM\Column(name="mandibular", type="string", length=20, nullable=true)
     */
    private $mandibular;

    /**
     * @var string
     *
     * @ORM\Column(name="ima_1_8", type="string", length=255, nullable=true)
     */
    private $ima18;

    /**
     * @var string
     *
     * @ORM\Column(name="desc_1_8", type="string", length=2, nullable=true)
     */
    private $desc18;

    /**
     * @var string
     *
     * @ORM\Column(name="ima_1_7", type="string", length=255, nullable=true)
     */
    private $ima17;

    /**
     * @var string
     *
     * @ORM\Column(name="desc_1_7", type="string", length=2, nullable=true)
     */
    private $desc17;

    /**
     * @var string
     *
     * @ORM\Column(name="ima_1_6", type="string", length=255, nullable=true)
     */
    private $ima16;

    /**
     * @var string
     *
     * @ORM\Column(name="desc_1_6", type="string", length=2, nullable=true)
     */
    private $desc16;

    /**
     * @var string
     *
     * @ORM\Column(name="ima_1_5", type="string", length=255, nullable=true)
     */
    private $ima15;

    /**
     * @var string
     *
     * @ORM\Column(name="desc_1_5", type="string", length=2, nullable=true)
     */
    private $desc15;

    /**
     * @var string
     *
     * @ORM\Column(name="ima_1_4", type="string", length=255, nullable=true)
     */
    private $ima14;

    /**
     * @var string
     *
     * @ORM\Column(name="desc_1_4", type="string", length=2, nullable=true)
     */
    private $desc14;

    /**
     * @var string
     *
     * @ORM\Column(name="ima_1_3", type="string", length=255, nullable=true)
     */
    private $ima13;

    /**
     * @var string
     *
     * @ORM\Column(name="desc_1_3", type="string", length=2, nullable=true)
     */
    private $desc13;

    /**
     * @var string
     *
     * @ORM\Column(name="ima_1_2", type="string", length=255, nullable=true)
     */
    private $ima12;

    /**
     * @var string
     *
     * @ORM\Column(name="desc_1_2", type="string", length=2, nullable=true)
     */
    private $desc12;

    /**
     * @var string
     *
     * @ORM\Column(name="ima_1_1", type="string", length=255, nullable=true)
     */
    private $ima11;

    /**
     * @var string
     *
     * @ORM\Column(name="desc_1_1", type="string", length=2, nullable=true)
     */
    private $desc11;

    /**
     * @var string
     *
     * @ORM\Column(name="ima_2_1", type="string", length=255, nullable=true)
     */
    private $ima21;

    /**
     * @var string
     *
     * @ORM\Column(name="d_2_1", type="string", length=2, nullable=true)
     */
    private $d21;

    /**
     * @var string
     *
     * @ORM\Column(name="ima_2_2", type="string", length=255, nullable=true)
     */
    private $ima22;

    /**
     * @var string
     *
     * @ORM\Column(name="d_2_2", type="string", length=2, nullable=true)
     */
    private $d22;

    /**
     * @var string
     *
     * @ORM\Column(name="ima_2_3", type="string", length=255, nullable=true)
     */
    private $ima23;

    /**
     * @var string
     *
     * @ORM\Column(name="d_2_3", type="string", length=2, nullable=true)
     */
    private $d23;

    /**
     * @var string
     *
     * @ORM\Column(name="ima_2_4", type="string", length=255, nullable=true)
     */
    private $ima24;

    /**
     * @var string
     *
     * @ORM\Column(name="d_2_4", type="string", length=2, nullable=true)
     */
    private $d24;

    /**
     * @var string
     *
     * @ORM\Column(name="ima_2_5", type="string", length=255, nullable=true)
     */
    private $ima25;

    /**
     * @var string
     *
     * @ORM\Column(name="d_2_5", type="string", length=2, nullable=true)
     */
    private $d25;

    /**
     * @var string
     *
     * @ORM\Column(name="ima_2_6", type="string", length=255, nullable=true)
     */
    private $ima26;

    /**
     * @var string
     *
     * @ORM\Column(name="d_2_6", type="string", length=2, nullable=true)
     */
    private $d26;

    /**
     * @var string
     *
     * @ORM\Column(name="ima_2_7", type="string", length=255, nullable=true)
     */
    private $ima27;

    /**
     * @var string
     *
     * @ORM\Column(name="d_2_7", type="string", length=2, nullable=true)
     */
    private $d27;

    /**
     * @var string
     *
     * @ORM\Column(name="ima_2_8", type="string", length=255, nullable=true)
     */
    private $ima28;

    /**
     * @var string
     *
     * @ORM\Column(name="d_2_8", type="string", length=2, nullable=true)
     */
    private $d28;

    /**
     * @var string
     *
     * @ORM\Column(name="ima_4_8", type="string", length=255, nullable=true)
     */
    private $ima48;

    /**
     * @var string
     *
     * @ORM\Column(name="d_4_8", type="string", length=2, nullable=true)
     */
    private $d48;

    /**
     * @var string
     *
     * @ORM\Column(name="ima_4_7", type="string", length=255, nullable=true)
     */
    private $ima47;

    /**
     * @var string
     *
     * @ORM\Column(name="d_4_7", type="string", length=2, nullable=true)
     */
    private $d47;

    /**
     * @var string
     *
     * @ORM\Column(name="ima_4_6", type="string", length=255, nullable=true)
     */
    private $ima46;

    /**
     * @var string
     *
     * @ORM\Column(name="d_4_6", type="string", length=2, nullable=true)
     */
    private $d46;

    /**
     * @var string
     *
     * @ORM\Column(name="ima_4_5", type="string", length=255, nullable=true)
     */
    private $ima45;

    /**
     * @var string
     *
     * @ORM\Column(name="d_4_5", type="string", length=2, nullable=true)
     */
    private $d45;

    /**
     * @var string
     *
     * @ORM\Column(name="ima_4_4", type="string", length=255, nullable=true)
     */
    private $ima44;

    /**
     * @var string
     *
     * @ORM\Column(name="d_4_4", type="string", length=2, nullable=true)
     */
    private $d44;

    /**
     * @var string
     *
     * @ORM\Column(name="ima_4_3", type="string", length=255, nullable=true)
     */
    private $ima43;

    /**
     * @var string
     *
     * @ORM\Column(name="d_4_3", type="string", length=2, nullable=true)
     */
    private $d43;

    /**
     * @var string
     *
     * @ORM\Column(name="ima_4_2", type="string", length=255, nullable=true)
     */
    private $ima42;

    /**
     * @var string
     *
     * @ORM\Column(name="d_4_2", type="string", length=2, nullable=true)
     */
    private $d42;

    /**
     * @var string
     *
     * @ORM\Column(name="ima_4_1", type="string", length=255, nullable=true)
     */
    private $ima41;

    /**
     * @var string
     *
     * @ORM\Column(name="d_4_1", type="string", length=2, nullable=true)
     */
    private $d41;

    /**
     * @var string
     *
     * @ORM\Column(name="ima_3_1", type="string", length=255, nullable=true)
     */
    private $ima31;

    /**
     * @var string
     *
     * @ORM\Column(name="d_3_1", type="string", length=2, nullable=true)
     */
    private $d31;

    /**
     * @var string
     *
     * @ORM\Column(name="ima_3_2", type="string", length=255, nullable=true)
     */
    private $ima32;

    /**
     * @var string
     *
     * @ORM\Column(name="d_3_2_", type="string", length=2, nullable=true)
     */
    private $d32;

    /**
     * @var string
     *
     * @ORM\Column(name="ima_3_3", type="string", length=255, nullable=true)
     */
    private $ima33;

    /**
     * @var string
     *
     * @ORM\Column(name="d_3_3", type="string", length=2, nullable=true)
     */
    private $d33;

    /**
     * @var string
     *
     * @ORM\Column(name="ima_3_4", type="string", length=255, nullable=true)
     */
    private $ima34;

    /**
     * @var string
     *
     * @ORM\Column(name="d_3_4", type="string", length=2, nullable=true)
     */
    private $d34;

    /**
     * @var string
     *
     * @ORM\Column(name="ima_3_5", type="string", length=255, nullable=true)
     */
    private $ima35;

    /**
     * @var string
     *
     * @ORM\Column(name="d_3_5", type="string", length=2, nullable=true)
     */
    private $d35;

    /**
     * @var string
     *
     * @ORM\Column(name="ima_3_6", type="string", length=255, nullable=true)
     */
    private $ima36;

    /**
     * @var string
     *
     * @ORM\Column(name="d_3_6", type="string", length=2, nullable=true)
     */
    private $d36;

    /**
     * @var string
     *
     * @ORM\Column(name="ima_3_7", type="string", length=255, nullable=true)
     */
    private $ima37;

    /**
     * @var string
     *
     * @ORM\Column(name="d_3_7", type="string", length=2, nullable=true)
     */
    private $d37;

    /**
     * @var string
     *
     * @ORM\Column(name="ima_3_8", type="string", length=255, nullable=true)
     */
    private $ima38;

    /**
     * @var string
     *
     * @ORM\Column(name="d_3_8", type="string", length=2, nullable=true)
     */
    private $d38;

    /**
     * @var \FPeriodoncia
     *
     * @ORM\ManyToOne(targetEntity="FPeriodoncia")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_f_perio", referencedColumnName="id_f_perio")
     * })
     */
    private $idFPerio;



    /**
     * Get idProGral
     *
     * @return integer 
     */
    public function getIdProGral()
    {
        return $this->idProGral;
    }

    /**
     * Set pronoGral
     *
     * @param string $pronoGral
     * @return PronosticoPerio
     */
    public function setPronoGral($pronoGral)
    {
        $this->pronoGral = $pronoGral;

        return $this;
    }

    /**
     * Get pronoGral
     *
     * @return string 
     */
    public function getPronoGral()
    {
        return $this->pronoGral;
    }

    /**
     * Set maxilar
     *
     * @param string $maxilar
     * @return PronosticoPerio
     */
    public function setMaxilar($maxilar)
    {
        $this->maxilar = $maxilar;

        return $this;
    }

    /**
     * Get maxilar
     *
     * @return string 
     */
    public function getMaxilar()
    {
        return $this->maxilar;
    }

    /**
     * Set mandibular
     *
     * @param string $mandibular
     * @return PronosticoPerio
     */
    public function setMandibular($mandibular)
    {
        $this->mandibular = $mandibular;

        return $this;
    }

    /**
     * Get mandibular
     *
     * @return string 
     */
    public function getMandibular()
    {
        return $this->mandibular;
    }

    /**
     * Set ima18
     *
     * @param string $ima18
     * @return PronosticoPerio
     */
    public function setIma18($ima18)
    {
        $this->ima18 = $ima18;

        return $this;
    }

    /**
     * Get ima18
     *
     * @return string 
     */
    public function getIma18()
    {
        return $this->ima18;
    }

    /**
     * Set desc18
     *
     * @param string $desc18
     * @return PronosticoPerio
     */
    public function setDesc18($desc18)
    {
        $this->desc18 = $desc18;

        return $this;
    }

    /**
     * Get desc18
     *
     * @return string 
     */
    public function getDesc18()
    {
        return $this->desc18;
    }

    /**
     * Set ima17
     *
     * @param string $ima17
     * @return PronosticoPerio
     */
    public function setIma17($ima17)
    {
        $this->ima17 = $ima17;

        return $this;
    }

    /**
     * Get ima17
     *
     * @return string 
     */
    public function getIma17()
    {
        return $this->ima17;
    }

    /**
     * Set desc17
     *
     * @param string $desc17
     * @return PronosticoPerio
     */
    public function setDesc17($desc17)
    {
        $this->desc17 = $desc17;

        return $this;
    }

    /**
     * Get desc17
     *
     * @return string 
     */
    public function getDesc17()
    {
        return $this->desc17;
    }

    /**
     * Set ima16
     *
     * @param string $ima16
     * @return PronosticoPerio
     */
    public function setIma16($ima16)
    {
        $this->ima16 = $ima16;

        return $this;
    }

    /**
     * Get ima16
     *
     * @return string 
     */
    public function getIma16()
    {
        return $this->ima16;
    }

    /**
     * Set desc16
     *
     * @param string $desc16
     * @return PronosticoPerio
     */
    public function setDesc16($desc16)
    {
        $this->desc16 = $desc16;

        return $this;
    }

    /**
     * Get desc16
     *
     * @return string 
     */
    public function getDesc16()
    {
        return $this->desc16;
    }

    /**
     * Set ima15
     *
     * @param string $ima15
     * @return PronosticoPerio
     */
    public function setIma15($ima15)
    {
        $this->ima15 = $ima15;

        return $this;
    }

    /**
     * Get ima15
     *
     * @return string 
     */
    public function getIma15()
    {
        return $this->ima15;
    }

    /**
     * Set desc15
     *
     * @param string $desc15
     * @return PronosticoPerio
     */
    public function setDesc15($desc15)
    {
        $this->desc15 = $desc15;

        return $this;
    }

    /**
     * Get desc15
     *
     * @return string 
     */
    public function getDesc15()
    {
        return $this->desc15;
    }

    /**
     * Set ima14
     *
     * @param string $ima14
     * @return PronosticoPerio
     */
    public function setIma14($ima14)
    {
        $this->ima14 = $ima14;

        return $this;
    }

    /**
     * Get ima14
     *
     * @return string 
     */
    public function getIma14()
    {
        return $this->ima14;
    }

    /**
     * Set desc14
     *
     * @param string $desc14
     * @return PronosticoPerio
     */
    public function setDesc14($desc14)
    {
        $this->desc14 = $desc14;

        return $this;
    }

    /**
     * Get desc14
     *
     * @return string 
     */
    public function getDesc14()
    {
        return $this->desc14;
    }

    /**
     * Set ima13
     *
     * @param string $ima13
     * @return PronosticoPerio
     */
    public function setIma13($ima13)
    {
        $this->ima13 = $ima13;

        return $this;
    }

    /**
     * Get ima13
     *
     * @return string 
     */
    public function getIma13()
    {
        return $this->ima13;
    }

    /**
     * Set desc13
     *
     * @param string $desc13
     * @return PronosticoPerio
     */
    public function setDesc13($desc13)
    {
        $this->desc13 = $desc13;

        return $this;
    }

    /**
     * Get desc13
     *
     * @return string 
     */
    public function getDesc13()
    {
        return $this->desc13;
    }

    /**
     * Set ima12
     *
     * @param string $ima12
     * @return PronosticoPerio
     */
    public function setIma12($ima12)
    {
        $this->ima12 = $ima12;

        return $this;
    }

    /**
     * Get ima12
     *
     * @return string 
     */
    public function getIma12()
    {
        return $this->ima12;
    }

    /**
     * Set desc12
     *
     * @param string $desc12
     * @return PronosticoPerio
     */
    public function setDesc12($desc12)
    {
        $this->desc12 = $desc12;

        return $this;
    }

    /**
     * Get desc12
     *
     * @return string 
     */
    public function getDesc12()
    {
        return $this->desc12;
    }

    /**
     * Set ima11
     *
     * @param string $ima11
     * @return PronosticoPerio
     */
    public function setIma11($ima11)
    {
        $this->ima11 = $ima11;

        return $this;
    }

    /**
     * Get ima11
     *
     * @return string 
     */
    public function getIma11()
    {
        return $this->ima11;
    }

    /**
     * Set desc11
     *
     * @param string $desc11
     * @return PronosticoPerio
     */
    public function setDesc11($desc11)
    {
        $this->desc11 = $desc11;

        return $this;
    }

    /**
     * Get desc11
     *
     * @return string 
     */
    public function getDesc11()
    {
        return $this->desc11;
    }

    /**
     * Set ima21
     *
     * @param string $ima21
     * @return PronosticoPerio
     */
    public function setIma21($ima21)
    {
        $this->ima21 = $ima21;

        return $this;
    }

    /**
     * Get ima21
     *
     * @return string 
     */
    public function getIma21()
    {
        return $this->ima21;
    }

    /**
     * Set d21
     *
     * @param string $d21
     * @return PronosticoPerio
     */
    public function setD21($d21)
    {
        $this->d21 = $d21;

        return $this;
    }

    /**
     * Get d21
     *
     * @return string 
     */
    public function getD21()
    {
        return $this->d21;
    }

    /**
     * Set ima22
     *
     * @param string $ima22
     * @return PronosticoPerio
     */
    public function setIma22($ima22)
    {
        $this->ima22 = $ima22;

        return $this;
    }

    /**
     * Get ima22
     *
     * @return string 
     */
    public function getIma22()
    {
        return $this->ima22;
    }

    /**
     * Set d22
     *
     * @param string $d22
     * @return PronosticoPerio
     */
    public function setD22($d22)
    {
        $this->d22 = $d22;

        return $this;
    }

    /**
     * Get d22
     *
     * @return string 
     */
    public function getD22()
    {
        return $this->d22;
    }

    /**
     * Set ima23
     *
     * @param string $ima23
     * @return PronosticoPerio
     */
    public function setIma23($ima23)
    {
        $this->ima23 = $ima23;

        return $this;
    }

    /**
     * Get ima23
     *
     * @return string 
     */
    public function getIma23()
    {
        return $this->ima23;
    }

    /**
     * Set d23
     *
     * @param string $d23
     * @return PronosticoPerio
     */
    public function setD23($d23)
    {
        $this->d23 = $d23;

        return $this;
    }

    /**
     * Get d23
     *
     * @return string 
     */
    public function getD23()
    {
        return $this->d23;
    }

    /**
     * Set ima24
     *
     * @param string $ima24
     * @return PronosticoPerio
     */
    public function setIma24($ima24)
    {
        $this->ima24 = $ima24;

        return $this;
    }

    /**
     * Get ima24
     *
     * @return string 
     */
    public function getIma24()
    {
        return $this->ima24;
    }

    /**
     * Set d24
     *
     * @param string $d24
     * @return PronosticoPerio
     */
    public function setD24($d24)
    {
        $this->d24 = $d24;

        return $this;
    }

    /**
     * Get d24
     *
     * @return string 
     */
    public function getD24()
    {
        return $this->d24;
    }

    /**
     * Set ima25
     *
     * @param string $ima25
     * @return PronosticoPerio
     */
    public function setIma25($ima25)
    {
        $this->ima25 = $ima25;

        return $this;
    }

    /**
     * Get ima25
     *
     * @return string 
     */
    public function getIma25()
    {
        return $this->ima25;
    }

    /**
     * Set d25
     *
     * @param string $d25
     * @return PronosticoPerio
     */
    public function setD25($d25)
    {
        $this->d25 = $d25;

        return $this;
    }

    /**
     * Get d25
     *
     * @return string 
     */
    public function getD25()
    {
        return $this->d25;
    }

    /**
     * Set ima26
     *
     * @param string $ima26
     * @return PronosticoPerio
     */
    public function setIma26($ima26)
    {
        $this->ima26 = $ima26;

        return $this;
    }

    /**
     * Get ima26
     *
     * @return string 
     */
    public function getIma26()
    {
        return $this->ima26;
    }

    /**
     * Set d26
     *
     * @param string $d26
     * @return PronosticoPerio
     */
    public function setD26($d26)
    {
        $this->d26 = $d26;

        return $this;
    }

    /**
     * Get d26
     *
     * @return string 
     */
    public function getD26()
    {
        return $this->d26;
    }

    /**
     * Set ima27
     *
     * @param string $ima27
     * @return PronosticoPerio
     */
    public function setIma27($ima27)
    {
        $this->ima27 = $ima27;

        return $this;
    }

    /**
     * Get ima27
     *
     * @return string 
     */
    public function getIma27()
    {
        return $this->ima27;
    }

    /**
     * Set d27
     *
     * @param string $d27
     * @return PronosticoPerio
     */
    public function setD27($d27)
    {
        $this->d27 = $d27;

        return $this;
    }

    /**
     * Get d27
     *
     * @return string 
     */
    public function getD27()
    {
        return $this->d27;
    }

    /**
     * Set ima28
     *
     * @param string $ima28
     * @return PronosticoPerio
     */
    public function setIma28($ima28)
    {
        $this->ima28 = $ima28;

        return $this;
    }

    /**
     * Get ima28
     *
     * @return string 
     */
    public function getIma28()
    {
        return $this->ima28;
    }

    /**
     * Set d28
     *
     * @param string $d28
     * @return PronosticoPerio
     */
    public function setD28($d28)
    {
        $this->d28 = $d28;

        return $this;
    }

    /**
     * Get d28
     *
     * @return string 
     */
    public function getD28()
    {
        return $this->d28;
    }

    /**
     * Set ima48
     *
     * @param string $ima48
     * @return PronosticoPerio
     */
    public function setIma48($ima48)
    {
        $this->ima48 = $ima48;

        return $this;
    }

    /**
     * Get ima48
     *
     * @return string 
     */
    public function getIma48()
    {
        return $this->ima48;
    }

    /**
     * Set d48
     *
     * @param string $d48
     * @return PronosticoPerio
     */
    public function setD48($d48)
    {
        $this->d48 = $d48;

        return $this;
    }

    /**
     * Get d48
     *
     * @return string 
     */
    public function getD48()
    {
        return $this->d48;
    }

    /**
     * Set ima47
     *
     * @param string $ima47
     * @return PronosticoPerio
     */
    public function setIma47($ima47)
    {
        $this->ima47 = $ima47;

        return $this;
    }

    /**
     * Get ima47
     *
     * @return string 
     */
    public function getIma47()
    {
        return $this->ima47;
    }

    /**
     * Set d47
     *
     * @param string $d47
     * @return PronosticoPerio
     */
    public function setD47($d47)
    {
        $this->d47 = $d47;

        return $this;
    }

    /**
     * Get d47
     *
     * @return string 
     */
    public function getD47()
    {
        return $this->d47;
    }

    /**
     * Set ima46
     *
     * @param string $ima46
     * @return PronosticoPerio
     */
    public function setIma46($ima46)
    {
        $this->ima46 = $ima46;

        return $this;
    }

    /**
     * Get ima46
     *
     * @return string 
     */
    public function getIma46()
    {
        return $this->ima46;
    }

    /**
     * Set d46
     *
     * @param string $d46
     * @return PronosticoPerio
     */
    public function setD46($d46)
    {
        $this->d46 = $d46;

        return $this;
    }

    /**
     * Get d46
     *
     * @return string 
     */
    public function getD46()
    {
        return $this->d46;
    }

    /**
     * Set ima45
     *
     * @param string $ima45
     * @return PronosticoPerio
     */
    public function setIma45($ima45)
    {
        $this->ima45 = $ima45;

        return $this;
    }

    /**
     * Get ima45
     *
     * @return string 
     */
    public function getIma45()
    {
        return $this->ima45;
    }

    /**
     * Set d45
     *
     * @param string $d45
     * @return PronosticoPerio
     */
    public function setD45($d45)
    {
        $this->d45 = $d45;

        return $this;
    }

    /**
     * Get d45
     *
     * @return string 
     */
    public function getD45()
    {
        return $this->d45;
    }

    /**
     * Set ima44
     *
     * @param string $ima44
     * @return PronosticoPerio
     */
    public function setIma44($ima44)
    {
        $this->ima44 = $ima44;

        return $this;
    }

    /**
     * Get ima44
     *
     * @return string 
     */
    public function getIma44()
    {
        return $this->ima44;
    }

    /**
     * Set d44
     *
     * @param string $d44
     * @return PronosticoPerio
     */
    public function setD44($d44)
    {
        $this->d44 = $d44;

        return $this;
    }

    /**
     * Get d44
     *
     * @return string 
     */
    public function getD44()
    {
        return $this->d44;
    }

    /**
     * Set ima43
     *
     * @param string $ima43
     * @return PronosticoPerio
     */
    public function setIma43($ima43)
    {
        $this->ima43 = $ima43;

        return $this;
    }

    /**
     * Get ima43
     *
     * @return string 
     */
    public function getIma43()
    {
        return $this->ima43;
    }

    /**
     * Set d43
     *
     * @param string $d43
     * @return PronosticoPerio
     */
    public function setD43($d43)
    {
        $this->d43 = $d43;

        return $this;
    }

    /**
     * Get d43
     *
     * @return string 
     */
    public function getD43()
    {
        return $this->d43;
    }

    /**
     * Set ima42
     *
     * @param string $ima42
     * @return PronosticoPerio
     */
    public function setIma42($ima42)
    {
        $this->ima42 = $ima42;

        return $this;
    }

    /**
     * Get ima42
     *
     * @return string 
     */
    public function getIma42()
    {
        return $this->ima42;
    }

    /**
     * Set d42
     *
     * @param string $d42
     * @return PronosticoPerio
     */
    public function setD42($d42)
    {
        $this->d42 = $d42;

        return $this;
    }

    /**
     * Get d42
     *
     * @return string 
     */
    public function getD42()
    {
        return $this->d42;
    }

    /**
     * Set ima41
     *
     * @param string $ima41
     * @return PronosticoPerio
     */
    public function setIma41($ima41)
    {
        $this->ima41 = $ima41;

        return $this;
    }

    /**
     * Get ima41
     *
     * @return string 
     */
    public function getIma41()
    {
        return $this->ima41;
    }

    /**
     * Set d41
     *
     * @param string $d41
     * @return PronosticoPerio
     */
    public function setD41($d41)
    {
        $this->d41 = $d41;

        return $this;
    }

    /**
     * Get d41
     *
     * @return string 
     */
    public function getD41()
    {
        return $this->d41;
    }

    /**
     * Set ima31
     *
     * @param string $ima31
     * @return PronosticoPerio
     */
    public function setIma31($ima31)
    {
        $this->ima31 = $ima31;

        return $this;
    }

    /**
     * Get ima31
     *
     * @return string 
     */
    public function getIma31()
    {
        return $this->ima31;
    }

    /**
     * Set d31
     *
     * @param string $d31
     * @return PronosticoPerio
     */
    public function setD31($d31)
    {
        $this->d31 = $d31;

        return $this;
    }

    /**
     * Get d31
     *
     * @return string 
     */
    public function getD31()
    {
        return $this->d31;
    }

    /**
     * Set ima32
     *
     * @param string $ima32
     * @return PronosticoPerio
     */
    public function setIma32($ima32)
    {
        $this->ima32 = $ima32;

        return $this;
    }

    /**
     * Get ima32
     *
     * @return string 
     */
    public function getIma32()
    {
        return $this->ima32;
    }

    /**
     * Set d32
     *
     * @param string $d32
     * @return PronosticoPerio
     */
    public function setD32($d32)
    {
        $this->d32 = $d32;

        return $this;
    }

    /**
     * Get d32
     *
     * @return string 
     */
    public function getD32()
    {
        return $this->d32;
    }

    /**
     * Set ima33
     *
     * @param string $ima33
     * @return PronosticoPerio
     */
    public function setIma33($ima33)
    {
        $this->ima33 = $ima33;

        return $this;
    }

    /**
     * Get ima33
     *
     * @return string 
     */
    public function getIma33()
    {
        return $this->ima33;
    }

    /**
     * Set d33
     *
     * @param string $d33
     * @return PronosticoPerio
     */
    public function setD33($d33)
    {
        $this->d33 = $d33;

        return $this;
    }

    /**
     * Get d33
     *
     * @return string 
     */
    public function getD33()
    {
        return $this->d33;
    }

    /**
     * Set ima34
     *
     * @param string $ima34
     * @return PronosticoPerio
     */
    public function setIma34($ima34)
    {
        $this->ima34 = $ima34;

        return $this;
    }

    /**
     * Get ima34
     *
     * @return string 
     */
    public function getIma34()
    {
        return $this->ima34;
    }

    /**
     * Set d34
     *
     * @param string $d34
     * @return PronosticoPerio
     */
    public function setD34($d34)
    {
        $this->d34 = $d34;

        return $this;
    }

    /**
     * Get d34
     *
     * @return string 
     */
    public function getD34()
    {
        return $this->d34;
    }

    /**
     * Set ima35
     *
     * @param string $ima35
     * @return PronosticoPerio
     */
    public function setIma35($ima35)
    {
        $this->ima35 = $ima35;

        return $this;
    }

    /**
     * Get ima35
     *
     * @return string 
     */
    public function getIma35()
    {
        return $this->ima35;
    }

    /**
     * Set d35
     *
     * @param string $d35
     * @return PronosticoPerio
     */
    public function setD35($d35)
    {
        $this->d35 = $d35;

        return $this;
    }

    /**
     * Get d35
     *
     * @return string 
     */
    public function getD35()
    {
        return $this->d35;
    }

    /**
     * Set ima36
     *
     * @param string $ima36
     * @return PronosticoPerio
     */
    public function setIma36($ima36)
    {
        $this->ima36 = $ima36;

        return $this;
    }

    /**
     * Get ima36
     *
     * @return string 
     */
    public function getIma36()
    {
        return $this->ima36;
    }

    /**
     * Set d36
     *
     * @param string $d36
     * @return PronosticoPerio
     */
    public function setD36($d36)
    {
        $this->d36 = $d36;

        return $this;
    }

    /**
     * Get d36
     *
     * @return string 
     */
    public function getD36()
    {
        return $this->d36;
    }

    /**
     * Set ima37
     *
     * @param string $ima37
     * @return PronosticoPerio
     */
    public function setIma37($ima37)
    {
        $this->ima37 = $ima37;

        return $this;
    }

    /**
     * Get ima37
     *
     * @return string 
     */
    public function getIma37()
    {
        return $this->ima37;
    }

    /**
     * Set d37
     *
     * @param string $d37
     * @return PronosticoPerio
     */
    public function setD37($d37)
    {
        $this->d37 = $d37;

        return $this;
    }

    /**
     * Get d37
     *
     * @return string 
     */
    public function getD37()
    {
        return $this->d37;
    }

    /**
     * Set ima38
     *
     * @param string $ima38
     * @return PronosticoPerio
     */
    public function setIma38($ima38)
    {
        $this->ima38 = $ima38;

        return $this;
    }

    /**
     * Get ima38
     *
     * @return string 
     */
    public function getIma38()
    {
        return $this->ima38;
    }

    /**
     * Set d38
     *
     * @param string $d38
     * @return PronosticoPerio
     */
    public function setD38($d38)
    {
        $this->d38 = $d38;

        return $this;
    }

    /**
     * Get d38
     *
     * @return string 
     */
    public function getD38()
    {
        return $this->d38;
    }

    /**
     * Set idFPerio
     *
     * @param \foues\FDBundle\Entity\FPeriodoncia $idFPerio
     * @return PronosticoPerio
     */
    public function setIdFPerio(\foues\FDBundle\Entity\FPeriodoncia $idFPerio = null)
    {
        $this->idFPerio = $idFPerio;

        return $this;
    }

    /**
     * Get idFPerio
     *
     * @return \foues\FDBundle\Entity\FPeriodoncia 
     */
    public function getIdFPerio()
    {
        return $this->idFPerio;
    }
}
