<?php

namespace foues\FDBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * EstadoCivil
 *
 * @ORM\Table(name="estado_civil", uniqueConstraints={@ORM\UniqueConstraint(name="estado_civil_pk", columns={"id_estado_c"})})
 * @ORM\Entity
 */
class EstadoCivil
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_estado_c", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="estado_civil_id_estado_c_seq", allocationSize=1, initialValue=1)
     */
    private $idEstadoC;

    /**
     * @var string
     *
     * @ORM\Column(name="nom_estado_c", type="string", length=12, nullable=false)
     */
    private $nomEstadoC;



    /**
     * Get idEstadoC
     *
     * @return integer 
     */
    public function getIdEstadoC()
    {
        return $this->idEstadoC;
    }

    /**
     * Set nomEstadoC
     *
     * @param string $nomEstadoC
     * @return EstadoCivil
     */
    public function setNomEstadoC($nomEstadoC)
    {
        $this->nomEstadoC = $nomEstadoC;

        return $this;
    }

    /**
     * Get nomEstadoC
     *
     * @return string 
     */
    public function getNomEstadoC()
    {
        return $this->nomEstadoC;
    }

    public function __toString(){
        return $this->getNomEstadoC();
    }
}
