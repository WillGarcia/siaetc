<?php

namespace foues\FDBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AreasDisenio
 *
 * @ORM\Table(name="areas_disenio", uniqueConstraints={@ORM\UniqueConstraint(name="areas_disenio_pk", columns={"id_area_d"})}, indexes={@ORM\Index(name="fk_areas_di_se_hace_f_restau_fk", columns={"id_f_resta"})})
 * @ORM\Entity
 */
class AreasDisenio
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_area_d", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="areas_disenio_id_area_d_seq", allocationSize=1, initialValue=1)
     */
    private $idAreaD;

    /**
     * @var string
     *
     * @ORM\Column(name="clase_retenedor", type="string", length=150, nullable=true)
     */
    private $claseRetenedor;

    /**
     * @var string
     *
     * @ORM\Column(name="nom_retenedor", type="string", length=150, nullable=true)
     */
    private $nomRetenedor;

    /**
     * @var string
     *
     * @ORM\Column(name="nom_ret_ine", type="string", length=200, nullable=true)
     */
    private $nomRetIne;

    /**
     * @var string
     *
     * @ORM\Column(name="nom_ret_ma", type="string", length=200, nullable=true)
     */
    private $nomRetMa;

    /**
     * @var string
     *
     * @ORM\Column(name="tipo_base", type="string", length=200, nullable=true)
     */
    private $tipoBase;

    /**
     * @var string
     *
     * @ORM\Column(name="desc_dis", type="string", length=500, nullable=true)
     */
    private $descDis;

    /**
     * @var string
     *
     * @ORM\Column(name="justificacion_d", type="string", length=500, nullable=true)
     */
    private $justificacionD;

    /**
     * @var \FRestaurativa
     *
     * @ORM\ManyToOne(targetEntity="FRestaurativa")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_f_resta", referencedColumnName="id_f_resta")
     * })
     */
    private $idFResta;



    /**
     * Get idAreaD
     *
     * @return integer 
     */
    public function getIdAreaD()
    {
        return $this->idAreaD;
    }

    /**
     * Set claseRetenedor
     *
     * @param string $claseRetenedor
     * @return AreasDisenio
     */
    public function setClaseRetenedor($claseRetenedor)
    {
        $this->claseRetenedor = $claseRetenedor;

        return $this;
    }

    /**
     * Get claseRetenedor
     *
     * @return string 
     */
    public function getClaseRetenedor()
    {
        return $this->claseRetenedor;
    }

    /**
     * Set nomRetenedor
     *
     * @param string $nomRetenedor
     * @return AreasDisenio
     */
    public function setNomRetenedor($nomRetenedor)
    {
        $this->nomRetenedor = $nomRetenedor;

        return $this;
    }

    /**
     * Get nomRetenedor
     *
     * @return string 
     */
    public function getNomRetenedor()
    {
        return $this->nomRetenedor;
    }

    /**
     * Set nomRetIne
     *
     * @param string $nomRetIne
     * @return AreasDisenio
     */
    public function setNomRetIne($nomRetIne)
    {
        $this->nomRetIne = $nomRetIne;

        return $this;
    }

    /**
     * Get nomRetIne
     *
     * @return string 
     */
    public function getNomRetIne()
    {
        return $this->nomRetIne;
    }

    /**
     * Set nomRetMa
     *
     * @param string $nomRetMa
     * @return AreasDisenio
     */
    public function setNomRetMa($nomRetMa)
    {
        $this->nomRetMa = $nomRetMa;

        return $this;
    }

    /**
     * Get nomRetMa
     *
     * @return string 
     */
    public function getNomRetMa()
    {
        return $this->nomRetMa;
    }

    /**
     * Set tipoBase
     *
     * @param string $tipoBase
     * @return AreasDisenio
     */
    public function setTipoBase($tipoBase)
    {
        $this->tipoBase = $tipoBase;

        return $this;
    }

    /**
     * Get tipoBase
     *
     * @return string 
     */
    public function getTipoBase()
    {
        return $this->tipoBase;
    }

    /**
     * Set descDis
     *
     * @param string $descDis
     * @return AreasDisenio
     */
    public function setDescDis($descDis)
    {
        $this->descDis = $descDis;

        return $this;
    }

    /**
     * Get descDis
     *
     * @return string 
     */
    public function getDescDis()
    {
        return $this->descDis;
    }

    /**
     * Set justificacionD
     *
     * @param string $justificacionD
     * @return AreasDisenio
     */
    public function setJustificacionD($justificacionD)
    {
        $this->justificacionD = $justificacionD;

        return $this;
    }

    /**
     * Get justificacionD
     *
     * @return string 
     */
    public function getJustificacionD()
    {
        return $this->justificacionD;
    }

    /**
     * Set idFResta
     *
     * @param \foues\FDBundle\Entity\FRestaurativa $idFResta
     * @return AreasDisenio
     */
    public function setIdFResta(\foues\FDBundle\Entity\FRestaurativa $idFResta = null)
    {
        $this->idFResta = $idFResta;

        return $this;
    }

    /**
     * Get idFResta
     *
     * @return \foues\FDBundle\Entity\FRestaurativa 
     */
    public function getIdFResta()
    {
        return $this->idFResta;
    }
}
