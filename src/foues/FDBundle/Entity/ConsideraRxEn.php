<?php

namespace foues\FDBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ConsideraRxEn
 *
 * @ORM\Table(name="considera_rx_en", uniqueConstraints={@ORM\UniqueConstraint(name="considera_rx_en_pk", columns={"id_consid"})}, indexes={@ORM\Index(name="fk_consider_tiene4_eva_pulp_fk", columns={"id_eva"})})
 * @ORM\Entity
 */
class ConsideraRxEn
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_consid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="considera_rx_en_id_consid_seq", allocationSize=1, initialValue=1)
     */
    private $idConsid;

    /**
     * @var string
     *
     * @ORM\Column(name="les_periapical", type="string", length=2, nullable=true)
     */
    private $lesPeriapical;

    /**
     * @var string
     *
     * @ORM\Column(name="rel_ca_pulpa", type="string", length=2, nullable=true)
     */
    private $relCaPulpa;

    /**
     * @var string
     *
     * @ORM\Column(name="cor_raiz1_2", type="string", length=2, nullable=true)
     */
    private $corRaiz12;

    /**
     * @var string
     *
     * @ORM\Column(name="cor_raiz1_1", type="string", length=2, nullable=true)
     */
    private $corRaiz11;

    /**
     * @var string
     *
     * @ORM\Column(name="cor_raiz2_1", type="string", length=2, nullable=true)
     */
    private $corRaiz21;

    /**
     * @var string
     *
     * @ORM\Column(name="tcr", type="string", length=2, nullable=true)
     */
    private $tcr;

    /**
     * @var \EvaPulparEndo
     *
     * @ORM\ManyToOne(targetEntity="EvaPulparEndo")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_eva", referencedColumnName="id_eva")
     * })
     */
    private $idEva;



    /**
     * Get idConsid
     *
     * @return integer 
     */
    public function getIdConsid()
    {
        return $this->idConsid;
    }

    /**
     * Set lesPeriapical
     *
     * @param string $lesPeriapical
     * @return ConsideraRxEn
     */
    public function setLesPeriapical($lesPeriapical)
    {
        $this->lesPeriapical = $lesPeriapical;

        return $this;
    }

    /**
     * Get lesPeriapical
     *
     * @return string 
     */
    public function getLesPeriapical()
    {
        return $this->lesPeriapical;
    }

    /**
     * Set relCaPulpa
     *
     * @param string $relCaPulpa
     * @return ConsideraRxEn
     */
    public function setRelCaPulpa($relCaPulpa)
    {
        $this->relCaPulpa = $relCaPulpa;

        return $this;
    }

    /**
     * Get relCaPulpa
     *
     * @return string 
     */
    public function getRelCaPulpa()
    {
        return $this->relCaPulpa;
    }

    /**
     * Set corRaiz12
     *
     * @param string $corRaiz12
     * @return ConsideraRxEn
     */
    public function setCorRaiz12($corRaiz12)
    {
        $this->corRaiz12 = $corRaiz12;

        return $this;
    }

    /**
     * Get corRaiz12
     *
     * @return string 
     */
    public function getCorRaiz12()
    {
        return $this->corRaiz12;
    }

    /**
     * Set corRaiz11
     *
     * @param string $corRaiz11
     * @return ConsideraRxEn
     */
    public function setCorRaiz11($corRaiz11)
    {
        $this->corRaiz11 = $corRaiz11;

        return $this;
    }

    /**
     * Get corRaiz11
     *
     * @return string 
     */
    public function getCorRaiz11()
    {
        return $this->corRaiz11;
    }

    /**
     * Set corRaiz21
     *
     * @param string $corRaiz21
     * @return ConsideraRxEn
     */
    public function setCorRaiz21($corRaiz21)
    {
        $this->corRaiz21 = $corRaiz21;

        return $this;
    }

    /**
     * Get corRaiz21
     *
     * @return string 
     */
    public function getCorRaiz21()
    {
        return $this->corRaiz21;
    }

    /**
     * Set tcr
     *
     * @param string $tcr
     * @return ConsideraRxEn
     */
    public function setTcr($tcr)
    {
        $this->tcr = $tcr;

        return $this;
    }

    /**
     * Get tcr
     *
     * @return string 
     */
    public function getTcr()
    {
        return $this->tcr;
    }

    /**
     * Set idEva
     *
     * @param \foues\FDBundle\Entity\EvaPulparEndo $idEva
     * @return ConsideraRxEn
     */
    public function setIdEva(\foues\FDBundle\Entity\EvaPulparEndo $idEva = null)
    {
        $this->idEva = $idEva;

        return $this;
    }

    /**
     * Get idEva
     *
     * @return \foues\FDBundle\Entity\EvaPulparEndo 
     */
    public function getIdEva()
    {
        return $this->idEva;
    }
}
