<?php

namespace foues\FDBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * FDiagnostico
 *
 * @ORM\Table(name="f_diagnostico", uniqueConstraints={@ORM\UniqueConstraint(name="f_diagnostico_pk", columns={"id_f_dx"})}, indexes={@ORM\Index(name="fk_f_diagno_contempla_ficha_cl_", columns={"id_ficha"})})
 * @ORM\Entity
 */
class FDiagnostico
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_f_dx", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="f_diagnostico_id_f_dx_seq", allocationSize=1, initialValue=1)
     */
    private $idFDx;

    /**
     * @var integer
     *
     * @ORM\Column(name="area_ficha", type="integer", nullable=false)
     */
    private $areaFicha;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha_inicio", type="date", nullable=true)
     */
    private $fechaInicio;

    /**
     * @var string
     *
     * @ORM\Column(name="mot_consulta", type="string", length=100, nullable=true)
     */
    private $motConsulta;

    /**
     * @var string
     *
     * @ORM\Column(name="hp_enfer", type="string", length=100, nullable=true)
     */
    private $hpEnfer;

    /**
     * @var string
     *
     * @ORM\Column(name="am_personal", type="string", length=60, nullable=true)
     */
    private $amPersonal;

    /**
     * @var string
     *
     * @ORM\Column(name="am_familiar", type="string", length=70, nullable=true)
     */
    private $amFamiliar;

    /**
     * @var string
     *
     * @ORM\Column(name="hist_medica", type="string", length=200, nullable=true)
     */
    private $histMedica;

    /**
     * @var string
     *
     * @ORM\Column(name="exa_lab", type="string", length=60, nullable=true)
     */
    private $exaLab;

    /**
     * @var string
     *
     * @ORM\Column(name="presion_arterial", type="string", length=6, nullable=true)
     */
    private $presionArterial;

    /**
     * @var integer
     *
     * @ORM\Column(name="pulso", type="integer", nullable=true)
     */
    private $pulso;

    /**
     * @var integer
     *
     * @ORM\Column(name="f_respiratoria", type="integer", nullable=true)
     */
    private $fRespiratoria;

    /**
     * @var string
     *
     * @ORM\Column(name="contextura", type="string", length=15, nullable=true)
     */
    private $contextura;

    /**
     * @var float
     *
     * @ORM\Column(name="estatura", type="float", precision=10, scale=0, nullable=true)
     */
    private $estatura;

    /**
     * @var integer
     *
     * @ORM\Column(name="peso", type="integer", nullable=true)
     */
    private $peso;

    /**
     * @var string
     *
     * @ORM\Column(name="otros_ef", type="string", length=25, nullable=true)
     */
    private $otrosEf;

    /**
     * @var string
     *
     * @ORM\Column(name="ho_familiar", type="string", length=100, nullable=true)
     */
    private $hoFamiliar;

    /**
     * @var string
     *
     * @ORM\Column(name="ho_personal", type="string", length=100, nullable=true)
     */
    private $hoPersonal;

    /**
     * @var string
     *
     * @ORM\Column(name="extraoral", type="string", length=100, nullable=true)
     */
    private $extraoral;

    /**
     * @var boolean
     *
     * @ORM\Column(name="alta", type="boolean", nullable=true)
     */
    private $alta;

    /**
     * @var \FichaClinica
     *
     * @ORM\ManyToOne(targetEntity="FichaClinica")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_ficha", referencedColumnName="id_ficha")
     * })
     */
    private $idFicha;



    /**
     * Get idFDx
     *
     * @return integer 
     */
    public function getIdFDx()
    {
        return $this->idFDx;
    }

    /**
     * Set areaFicha
     *
     * @param integer $areaFicha
     * @return FDiagnostico
     */
    public function setAreaFicha($areaFicha)
    {
        $this->areaFicha = $areaFicha;

        return $this;
    }

    /**
     * Get areaFicha
     *
     * @return integer 
     */
    public function getAreaFicha()
    {
        return $this->areaFicha;
    }

    /**
     * Set fechaInicio
     *
     * @param \DateTime $fechaInicio
     * @return FDiagnostico
     */
    public function setFechaInicio($fechaInicio)
    {
        $this->fechaInicio = $fechaInicio;

        return $this;
    }

    /**
     * Get fechaInicio
     *
     * @return \DateTime 
     */
    public function getFechaInicio()
    {
        return $this->fechaInicio;
    }

    /**
     * Set motConsulta
     *
     * @param string $motConsulta
     * @return FDiagnostico
     */
    public function setMotConsulta($motConsulta)
    {
        $this->motConsulta = $motConsulta;

        return $this;
    }

    /**
     * Get motConsulta
     *
     * @return string 
     */
    public function getMotConsulta()
    {
        return $this->motConsulta;
    }

    /**
     * Set hpEnfer
     *
     * @param string $hpEnfer
     * @return FDiagnostico
     */
    public function setHpEnfer($hpEnfer)
    {
        $this->hpEnfer = $hpEnfer;

        return $this;
    }

    /**
     * Get hpEnfer
     *
     * @return string 
     */
    public function getHpEnfer()
    {
        return $this->hpEnfer;
    }

    /**
     * Set amPersonal
     *
     * @param string $amPersonal
     * @return FDiagnostico
     */
    public function setAmPersonal($amPersonal)
    {
        $this->amPersonal = $amPersonal;

        return $this;
    }

    /**
     * Get amPersonal
     *
     * @return string 
     */
    public function getAmPersonal()
    {
        return $this->amPersonal;
    }

    /**
     * Set amFamiliar
     *
     * @param string $amFamiliar
     * @return FDiagnostico
     */
    public function setAmFamiliar($amFamiliar)
    {
        $this->amFamiliar = $amFamiliar;

        return $this;
    }

    /**
     * Get amFamiliar
     *
     * @return string 
     */
    public function getAmFamiliar()
    {
        return $this->amFamiliar;
    }

    /**
     * Set histMedica
     *
     * @param string $histMedica
     * @return FDiagnostico
     */
    public function setHistMedica($histMedica)
    {
        $this->histMedica = $histMedica;

        return $this;
    }

    /**
     * Get histMedica
     *
     * @return string 
     */
    public function getHistMedica()
    {
        return $this->histMedica;
    }

    /**
     * Set exaLab
     *
     * @param string $exaLab
     * @return FDiagnostico
     */
    public function setExaLab($exaLab)
    {
        $this->exaLab = $exaLab;

        return $this;
    }

    /**
     * Get exaLab
     *
     * @return string 
     */
    public function getExaLab()
    {
        return $this->exaLab;
    }

    /**
     * Set presionArterial
     *
     * @param string $presionArterial
     * @return FDiagnostico
     */
    public function setPresionArterial($presionArterial)
    {
        $this->presionArterial = $presionArterial;

        return $this;
    }

    /**
     * Get presionArterial
     *
     * @return string 
     */
    public function getPresionArterial()
    {
        return $this->presionArterial;
    }

    /**
     * Set pulso
     *
     * @param integer $pulso
     * @return FDiagnostico
     */
    public function setPulso($pulso)
    {
        $this->pulso = $pulso;

        return $this;
    }

    /**
     * Get pulso
     *
     * @return integer 
     */
    public function getPulso()
    {
        return $this->pulso;
    }

    /**
     * Set fRespiratoria
     *
     * @param integer $fRespiratoria
     * @return FDiagnostico
     */
    public function setFRespiratoria($fRespiratoria)
    {
        $this->fRespiratoria = $fRespiratoria;

        return $this;
    }

    /**
     * Get fRespiratoria
     *
     * @return integer 
     */
    public function getFRespiratoria()
    {
        return $this->fRespiratoria;
    }

    /**
     * Set contextura
     *
     * @param string $contextura
     * @return FDiagnostico
     */
    public function setContextura($contextura)
    {
        $this->contextura = $contextura;

        return $this;
    }

    /**
     * Get contextura
     *
     * @return string 
     */
    public function getContextura()
    {
        return $this->contextura;
    }

    /**
     * Set estatura
     *
     * @param float $estatura
     * @return FDiagnostico
     */
    public function setEstatura($estatura)
    {
        $this->estatura = $estatura;

        return $this;
    }

    /**
     * Get estatura
     *
     * @return float 
     */
    public function getEstatura()
    {
        return $this->estatura;
    }

    /**
     * Set peso
     *
     * @param integer $peso
     * @return FDiagnostico
     */
    public function setPeso($peso)
    {
        $this->peso = $peso;

        return $this;
    }

    /**
     * Get peso
     *
     * @return integer 
     */
    public function getPeso()
    {
        return $this->peso;
    }

    /**
     * Set otrosEf
     *
     * @param string $otrosEf
     * @return FDiagnostico
     */
    public function setOtrosEf($otrosEf)
    {
        $this->otrosEf = $otrosEf;

        return $this;
    }

    /**
     * Get otrosEf
     *
     * @return string 
     */
    public function getOtrosEf()
    {
        return $this->otrosEf;
    }

    /**
     * Set hoFamiliar
     *
     * @param string $hoFamiliar
     * @return FDiagnostico
     */
    public function setHoFamiliar($hoFamiliar)
    {
        $this->hoFamiliar = $hoFamiliar;

        return $this;
    }

    /**
     * Get hoFamiliar
     *
     * @return string 
     */
    public function getHoFamiliar()
    {
        return $this->hoFamiliar;
    }

    /**
     * Set hoPersonal
     *
     * @param string $hoPersonal
     * @return FDiagnostico
     */
    public function setHoPersonal($hoPersonal)
    {
        $this->hoPersonal = $hoPersonal;

        return $this;
    }

    /**
     * Get hoPersonal
     *
     * @return string 
     */
    public function getHoPersonal()
    {
        return $this->hoPersonal;
    }

    /**
     * Set extraoral
     *
     * @param string $extraoral
     * @return FDiagnostico
     */
    public function setExtraoral($extraoral)
    {
        $this->extraoral = $extraoral;

        return $this;
    }

    /**
     * Get extraoral
     *
     * @return string 
     */
    public function getExtraoral()
    {
        return $this->extraoral;
    }

    /**
     * Set alta
     *
     * @param boolean $alta
     * @return FDiagnostico
     */
    public function setAlta($alta)
    {
        $this->alta = $alta;

        return $this;
    }

    /**
     * Get alta
     *
     * @return boolean 
     */
    public function getAlta()
    {
        return $this->alta;
    }

    /**
     * Set idFicha
     *
     * @param \foues\FDBundle\Entity\FichaClinica $idFicha
     * @return FDiagnostico
     */
    public function setIdFicha(\foues\FDBundle\Entity\FichaClinica $idFicha = null)
    {
        $this->idFicha = $idFicha;

        return $this;
    }

    /**
     * Get idFicha
     *
     * @return \foues\FDBundle\Entity\FichaClinica 
     */
    public function getIdFicha()
    {
        return $this->idFicha;
    }
}
