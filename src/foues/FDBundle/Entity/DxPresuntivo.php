<?php

namespace foues\FDBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * DxPresuntivo
 *
 * @ORM\Table(name="dx_presuntivo", uniqueConstraints={@ORM\UniqueConstraint(name="dx_presuntivo_pk", columns={"id_dp"})}, indexes={@ORM\Index(name="fk_dx_presu_incluye2_f_diagno_f", columns={"id_f_dx"})})
 * @ORM\Entity
 */
class DxPresuntivo
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_dp", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="dx_presuntivo_id_dp_seq", allocationSize=1, initialValue=1)
     */
    private $idDp;

    /**
     * @var string
     *
     * @ORM\Column(name="ap_psico_cond", type="string", length=100, nullable=true)
     */
    private $apPsicoCond;

    /**
     * @var string
     *
     * @ORM\Column(name="alte_sis", type="string", length=100, nullable=true)
     */
    private $alteSis;

    /**
     * @var string
     *
     * @ORM\Column(name="les_tb", type="string", length=100, nullable=true)
     */
    private $lesTb;

    /**
     * @var string
     *
     * @ORM\Column(name="les_td", type="string", length=100, nullable=true)
     */
    private $lesTd;

    /**
     * @var string
     *
     * @ORM\Column(name="periodontal", type="string", length=60, nullable=true)
     */
    private $periodontal;

    /**
     * @var string
     *
     * @ORM\Column(name="pulpar", type="string", length=50, nullable=true)
     */
    private $pulpar;

    /**
     * @var string
     *
     * @ORM\Column(name="periapical", type="string", length=50, nullable=true)
     */
    private $periapical;

    /**
     * @var string
     *
     * @ORM\Column(name="les_cariosas", type="string", length=40, nullable=true)
     */
    private $lesCariosas;

    /**
     * @var string
     *
     * @ORM\Column(name="riesgo_cario", type="string", length=8, nullable=true)
     */
    private $riesgoCario;

    /**
     * @var string
     *
     * @ORM\Column(name="protesico", type="string", length=50, nullable=true)
     */
    private $protesico;

    /**
     * @var string
     *
     * @ORM\Column(name="f_oclusion", type="string", length=20, nullable=true)
     */
    private $fOclusion;

    /**
     * @var string
     *
     * @ORM\Column(name="otros_d", type="string", length=60, nullable=true)
     */
    private $otrosD;

    /**
     * @var \FDiagnostico
     *
     * @ORM\ManyToOne(targetEntity="FDiagnostico")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_f_dx", referencedColumnName="id_f_dx")
     * })
     */
    private $idFDx;



    /**
     * Get idDp
     *
     * @return integer 
     */
    public function getIdDp()
    {
        return $this->idDp;
    }

    /**
     * Set apPsicoCond
     *
     * @param string $apPsicoCond
     * @return DxPresuntivo
     */
    public function setApPsicoCond($apPsicoCond)
    {
        $this->apPsicoCond = $apPsicoCond;

        return $this;
    }

    /**
     * Get apPsicoCond
     *
     * @return string 
     */
    public function getApPsicoCond()
    {
        return $this->apPsicoCond;
    }

    /**
     * Set alteSis
     *
     * @param string $alteSis
     * @return DxPresuntivo
     */
    public function setAlteSis($alteSis)
    {
        $this->alteSis = $alteSis;

        return $this;
    }

    /**
     * Get alteSis
     *
     * @return string 
     */
    public function getAlteSis()
    {
        return $this->alteSis;
    }

    /**
     * Set lesTb
     *
     * @param string $lesTb
     * @return DxPresuntivo
     */
    public function setLesTb($lesTb)
    {
        $this->lesTb = $lesTb;

        return $this;
    }

    /**
     * Get lesTb
     *
     * @return string 
     */
    public function getLesTb()
    {
        return $this->lesTb;
    }

    /**
     * Set lesTd
     *
     * @param string $lesTd
     * @return DxPresuntivo
     */
    public function setLesTd($lesTd)
    {
        $this->lesTd = $lesTd;

        return $this;
    }

    /**
     * Get lesTd
     *
     * @return string 
     */
    public function getLesTd()
    {
        return $this->lesTd;
    }

    /**
     * Set periodontal
     *
     * @param string $periodontal
     * @return DxPresuntivo
     */
    public function setPeriodontal($periodontal)
    {
        $this->periodontal = $periodontal;

        return $this;
    }

    /**
     * Get periodontal
     *
     * @return string 
     */
    public function getPeriodontal()
    {
        return $this->periodontal;
    }

    /**
     * Set pulpar
     *
     * @param string $pulpar
     * @return DxPresuntivo
     */
    public function setPulpar($pulpar)
    {
        $this->pulpar = $pulpar;

        return $this;
    }

    /**
     * Get pulpar
     *
     * @return string 
     */
    public function getPulpar()
    {
        return $this->pulpar;
    }

    /**
     * Set periapical
     *
     * @param string $periapical
     * @return DxPresuntivo
     */
    public function setPeriapical($periapical)
    {
        $this->periapical = $periapical;

        return $this;
    }

    /**
     * Get periapical
     *
     * @return string 
     */
    public function getPeriapical()
    {
        return $this->periapical;
    }

    /**
     * Set lesCariosas
     *
     * @param string $lesCariosas
     * @return DxPresuntivo
     */
    public function setLesCariosas($lesCariosas)
    {
        $this->lesCariosas = $lesCariosas;

        return $this;
    }

    /**
     * Get lesCariosas
     *
     * @return string 
     */
    public function getLesCariosas()
    {
        return $this->lesCariosas;
    }

    /**
     * Set riesgoCario
     *
     * @param string $riesgoCario
     * @return DxPresuntivo
     */
    public function setRiesgoCario($riesgoCario)
    {
        $this->riesgoCario = $riesgoCario;

        return $this;
    }

    /**
     * Get riesgoCario
     *
     * @return string 
     */
    public function getRiesgoCario()
    {
        return $this->riesgoCario;
    }

    /**
     * Set protesico
     *
     * @param string $protesico
     * @return DxPresuntivo
     */
    public function setProtesico($protesico)
    {
        $this->protesico = $protesico;

        return $this;
    }

    /**
     * Get protesico
     *
     * @return string 
     */
    public function getProtesico()
    {
        return $this->protesico;
    }

    /**
     * Set fOclusion
     *
     * @param string $fOclusion
     * @return DxPresuntivo
     */
    public function setFOclusion($fOclusion)
    {
        $this->fOclusion = $fOclusion;

        return $this;
    }

    /**
     * Get fOclusion
     *
     * @return string 
     */
    public function getFOclusion()
    {
        return $this->fOclusion;
    }

    /**
     * Set otrosD
     *
     * @param string $otrosD
     * @return DxPresuntivo
     */
    public function setOtrosD($otrosD)
    {
        $this->otrosD = $otrosD;

        return $this;
    }

    /**
     * Get otrosD
     *
     * @return string 
     */
    public function getOtrosD()
    {
        return $this->otrosD;
    }

    /**
     * Set idFDx
     *
     * @param \foues\FDBundle\Entity\FDiagnostico $idFDx
     * @return DxPresuntivo
     */
    public function setIdFDx(\foues\FDBundle\Entity\FDiagnostico $idFDx = null)
    {
        $this->idFDx = $idFDx;

        return $this;
    }

    /**
     * Get idFDx
     *
     * @return \foues\FDBundle\Entity\FDiagnostico 
     */
    public function getIdFDx()
    {
        return $this->idFDx;
    }
}
