<?php

namespace foues\FDBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * HistoriaPresenteEnf
 *
 * @ORM\Table(name="historia_presente_enf", uniqueConstraints={@ORM\UniqueConstraint(name="historia_presente_enf_pk", columns={"id_historia"})}, indexes={@ORM\Index(name="fk_historia_posee4_f_endodo_fk", columns={"id_f_endo"})})
 * @ORM\Entity
 */
class HistoriaPresenteEnf
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_historia", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="historia_presente_enf_id_historia_seq", allocationSize=1, initialValue=1)
     */
    private $idHistoria;

    /**
     * @var string
     *
     * @ORM\Column(name="num_diente", type="string", length=4, nullable=true)
     */
    private $numDiente;

    /**
     * @var string
     *
     * @ORM\Column(name="descripcion", type="string", length=80, nullable=true)
     */
    private $descripcion;

    /**
     * @var \FEndodoncia
     *
     * @ORM\ManyToOne(targetEntity="FEndodoncia")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_f_endo", referencedColumnName="id_f_endo")
     * })
     */
    private $idFEndo;



    /**
     * Get idHistoria
     *
     * @return integer 
     */
    public function getIdHistoria()
    {
        return $this->idHistoria;
    }

    /**
     * Set numDiente
     *
     * @param string $numDiente
     * @return HistoriaPresenteEnf
     */
    public function setNumDiente($numDiente)
    {
        $this->numDiente = $numDiente;

        return $this;
    }

    /**
     * Get numDiente
     *
     * @return string 
     */
    public function getNumDiente()
    {
        return $this->numDiente;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     * @return HistoriaPresenteEnf
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string 
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set idFEndo
     *
     * @param \foues\FDBundle\Entity\FEndodoncia $idFEndo
     * @return HistoriaPresenteEnf
     */
    public function setIdFEndo(\foues\FDBundle\Entity\FEndodoncia $idFEndo = null)
    {
        $this->idFEndo = $idFEndo;

        return $this;
    }

    /**
     * Get idFEndo
     *
     * @return \foues\FDBundle\Entity\FEndodoncia 
     */
    public function getIdFEndo()
    {
        return $this->idFEndo;
    }
}
