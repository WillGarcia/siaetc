<?php

namespace foues\FDBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('fouesFDBundle:Default:index.html.twig');
    }
}
