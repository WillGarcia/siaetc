<?php

namespace foues\FDBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use foues\FDBundle\Entity\FDiagnostico;
use foues\FDBundle\Form\FDiagnosticoType;
use foues\FDBundle\Form\PacienteType;
use foues\FDBundle\Entity\Paciente;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

/**
 * FDiagnostico controller.
 *
 */
class FDiagnosticoController extends Controller
{
    /**
     * Lists all FDiagnostico entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $fDiagnosticos = $em->getRepository('fouesFDBundle:FDiagnostico')->findAll();

        return $this->render('fdiagnostico/index.html.twig', array(
            'fDiagnosticos' => $fDiagnosticos,
        ));
    }

    /**
     * Creates a new FDiagnostico entity.
     *
     */
    public function newAction(Request $request)
    {
        $fDiagnostico = new FDiagnostico();
        $form = $this->createForm('foues\FDBundle\Form\FDiagnosticoType', $fDiagnostico);
        $form->handleRequest($request);

         $paciente=new paciente();
        $formp= $this->createForm('foues\FDBundle\Form\PacienteType', $paciente);
        $formp->handleRequest($request);

        $em=$this->getDoctrine()->getManager();
        $departamento=$em->getRepository('fouesFDBundle:Departamento')->findAll();

         $defaultData = array('titulo'=>'Datos del expediente');

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($fDiagnostico);
            $em->flush();

            return $this->redirectToRoute('fdiagnostico_show', array('id' => $fDiagnostico->getId()));
        }

        return $this->render('fdiagnostico/new.html.twig', array(
            'fDiagnostico' => $fDiagnostico,
            'form' => $form->createView(),
             'paciente' => $paciente,
            'formp' => $formp->createView(),
            'departamento'=>$departamento
        ));
    }


    //funcion que funciona para desplegar los municipios despues de seleccionar un departamenteo
    public function selectMunicipioAction(Request $request){

        $idMunicipio=$request->request->get('id_depto');

        $em=$this->getDoctrine()->getManager();
        $query=$em->createQuery("
            SELECT m FROM fouesFDBundle:Municipio m 
            WHERE m.idDepartamento = :idDepto")
        ->setParameter("idDepto",$idMunicipio);

        $Municipio=$query->getArrayResult();

        return new JsonResponse($Municipio);



    }

    /**
     * Finds and displays a FDiagnostico entity.
     *
     */
    public function showAction(FDiagnostico $fDiagnostico)
    {
        $deleteForm = $this->createDeleteForm($fDiagnostico);

        return $this->render('fdiagnostico/show.html.twig', array(
            'fDiagnostico' => $fDiagnostico,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing FDiagnostico entity.
     *
     */
    public function editAction(Request $request, FDiagnostico $fDiagnostico)
    {
        $deleteForm = $this->createDeleteForm($fDiagnostico);
        $editForm = $this->createForm('foues\FDBundle\Form\FDiagnosticoType', $fDiagnostico);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($fDiagnostico);
            $em->flush();

            return $this->redirectToRoute('fdiagnostico_edit', array('id' => $fDiagnostico->getId()));
        }

        return $this->render('fdiagnostico/edit.html.twig', array(
            'fDiagnostico' => $fDiagnostico,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a FDiagnostico entity.
     *
     */
    public function deleteAction(Request $request, FDiagnostico $fDiagnostico)
    {
        $form = $this->createDeleteForm($fDiagnostico);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($fDiagnostico);
            $em->flush();
        }

        return $this->redirectToRoute('fdiagnostico_index');
    }

    /**
     * Creates a form to delete a FDiagnostico entity.
     *
     * @param FDiagnostico $fDiagnostico The FDiagnostico entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(FDiagnostico $fDiagnostico)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('fdiagnostico_delete', array('id' => $fDiagnostico->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
