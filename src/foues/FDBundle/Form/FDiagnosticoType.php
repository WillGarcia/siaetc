<?php

namespace foues\FDBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class FDiagnosticoType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('idFicha',TextType::class, array('label'=>'Id Ficha'))
            ->add('areaFicha', TextType::class, array('label'=>'Area Ficha'))
          //  ->add('fechaInicio', 'date')
            ->add('motConsulta',TextareaType::class, array('label'=>'Motivo de Consulta'))
            ->add('hpEnfer',TextareaType::class, array('label'=>'Historia de la presente enfermeada'))
            ->add('amPersonal',TextareaType::class, array('label'=>'Antecedentes medicos personales'))
            ->add('amFamiliar',TextareaType::class, array('label'=>'Antecedentes medicos familiares'))
            ->add('histMedica',TextareaType::class, array('label'=>'Historia medica'))
            ->add('exaLab',TextareaType::class, array('label'=>'Examenes de laboratorio'))
            ->add('presionArterial',TextType::class, array('label'=>'Presion Arterial'))
            ->add('pulso',TextType::class, array('label'=>'Pulso'))
            ->add('fRespiratoria',TextType::class, array('label'=>'Frecuencia respiratoria'))
            ->add('contextura',TextType::class, array('label'=>'Contextura'))
            ->add('estatura',TextType::class, array('label'=>'Estatura'))
            ->add('peso',TextType::class, array('label'=>'Peso'))
            ->add('otrosEf',TextareaType::class, array('label'=>'Otras Enfermedades'))
            ->add('hoFamiliar',TextareaType::class, array('label'=>'Historia odontologica familiar'))
            ->add('hoPersonal',TextareaType::class, array('label'=>'Historia odontologica personal'))
            ->add('extraoral',TextType::class, array('label'=>'Extraoral'))
            ->add('alta',TextType::class, array('label'=>'Alta'))
        ;
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'foues\FDBundle\Entity\FDiagnostico'
        ));
    }
}
