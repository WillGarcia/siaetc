<?php

namespace foues\FDBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PacienteType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('dui')
            ->add('numExpediente')
            ->add('fechaApertura', 'date')
            ->add('nom1Pac')
            ->add('nom2Pac')
            ->add('nom3Pac')
            ->add('ape1Pac')
            ->add('ape2Pac')
            ->add('ape3Pac')
            ->add('fechaNac', 'date')
            ->add('edad')
            ->add('deptoNac')
            ->add('munNac')
            ->add('deptoRes')
            ->add('munRes')
            ->add('nivelEduc')
            ->add('domicilio')
            ->add('telefonoPx')
            ->add('movilPx')
            ->add('dirTrabajo')
            ->add('telTrabajo')
            ->add('responsable')
            ->add('telResponsable')
            ->add('ocupacion')
            ->add('activo')
            ->add('idGenero')
            ->add('idEstadoC')
        ;
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'foues\FDBundle\Entity\Paciente'
        ));
    }
}
