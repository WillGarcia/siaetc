<?php

namespace SeguridadBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * BitacoraSist
 */
class BitacoraSist
{
    /**
     * @var integer
     */
    private $idBitacora;

    /**
     * @var string
     */
    private $nomAccion;

    /**
     * @var string
     */
    private $nomUsuario;

    /**
     * @var \DateTime
     */
    private $fecha;

    /**
     * @var \SeguridadBundle\Entity\Usuario
     */
    private $idUsuario;


    /**
     * Get idBitacora
     *
     * @return integer 
     */
    public function getIdBitacora()
    {
        return $this->idBitacora;
    }

    /**
     * Set nomAccion
     *
     * @param string $nomAccion
     * @return BitacoraSist
     */
    public function setNomAccion($nomAccion)
    {
        $this->nomAccion = $nomAccion;

        return $this;
    }

    /**
     * Get nomAccion
     *
     * @return string 
     */
    public function getNomAccion()
    {
        return $this->nomAccion;
    }

    /**
     * Set nomUsuario
     *
     * @param string $nomUsuario
     * @return BitacoraSist
     */
    public function setNomUsuario($nomUsuario)
    {
        $this->nomUsuario = $nomUsuario;

        return $this;
    }

    /**
     * Get nomUsuario
     *
     * @return string 
     */
    public function getNomUsuario()
    {
        return $this->nomUsuario;
    }

    /**
     * Set fecha
     *
     * @param \DateTime $fecha
     * @return BitacoraSist
     */
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;

        return $this;
    }

    /**
     * Get fecha
     *
     * @return \DateTime 
     */
    public function getFecha()
    {
        return $this->fecha;
    }

    /**
     * Set idUsuario
     *
     * @param \SeguridadBundle\Entity\Usuario $idUsuario
     * @return BitacoraSist
     */
    public function setIdUsuario(\SeguridadBundle\Entity\Usuario $idUsuario = null)
    {
        $this->idUsuario = $idUsuario;

        return $this;
    }

    /**
     * Get idUsuario
     *
     * @return \SeguridadBundle\Entity\Usuario 
     */
    public function getIdUsuario()
    {
        return $this->idUsuario;
    }
}
