<?php

namespace SeguridadBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\HttpFoundation\File\File;

/**
 * Usuario
 */
class Usuario implements UserInterface, \Serializable
{
    /**
     * @var integer
     */
    private $idUsuario;

    /**
     * @var string
     */
    private $tipoUsuario;

    /**
     * @var string
     */
    private $userName;
    
     /**
     * @var string
     */
    private $password;

    /**
     * @var string
     */
    private $contrasenia;

    /**
     * @var string
     */
    private $email;

    /**
     * @var boolean
     */
    private $activo;
    
     /**
     * @var string
     */
    private $fotoUser;
    
      /**
     * @Assert\NotBlank()
     * @Assert\Length(max=4096)
     */
    //private $plainPassword; 
    



    /**
     * Get idUsuario
     *
     * @return integer 
     */
    public function getIdUsuario()
    {
        return $this->idUsuario;
    }

    /**
     * Set tipoUsuario
     *
     * @param string $tipoUsuario
     * @return Usuario
     */
    public function setTipoUsuario($tipoUsuario)
    {
        $this->tipoUsuario = $tipoUsuario;

        return $this;
    }

    /**
     * Get tipoUsuario
     *
     * @return string 
     */
    public function getTipoUsuario()
    {
        return $this->tipoUsuario;
    }

    /**
     * Set userName
     *
     * @param string $userName
     * @return Usuario
     */
    public function setUserName($userName)
    {
        $this->userName = strtolower($userName);

        return $this;
    }

    /**
     * Get userName
     *
     * @return string 
     */
    public function getUserName()
    {
        return $this->userName;
    }

    /**
     * Set password
     *
     * @param string $password
     * @return Usuario
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Get password
     *
     * @return string 
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return Usuario
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set activo
     *
     * @param boolean $activo
     * @return Usuario
     */
    public function setActivo($activo)
    {
        $this->activo = $activo;

        return $this;
    }

    /**
     * Get activo
     *
     * @return boolean 
     */
    public function getActivo()
    {
        return $this->activo;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $bitacora;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->bitacora = new \Doctrine\Common\Collections\ArrayCollection();
        $this->activo = true;
        //parent::__construct();
        // $this->salt = md5(uniqid(null, true));
    }

    /**
     * Add bitacora
     *
     * @param \SeguridadBundle\Entity\BitacoraSist $bitacora
     * @return Usuario
     */
    public function addBitacora(\SeguridadBundle\Entity\BitacoraSist $bitacora)
    {
        $this->bitacora[] = $bitacora;

        return $this;
    }

    /**
     * Remove bitacora
     *
     * @param \SeguridadBundle\Entity\BitacoraSist $bitacora
     */
    public function removeBitacora(\SeguridadBundle\Entity\BitacoraSist $bitacora)
    {
        $this->bitacora->removeElement($bitacora);
    }

    /**
     * Get bitacora
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getBitacora()
    {
        return $this->bitacora;
    }
    
     public function getRoles()
    {
        //return array('ROLE_ADMIN');
        return array($this->tipoUsuario);
    }

    public function eraseCredentials()
    {
    }

    /** @see \Serializable::serialize() */
    public function serialize()
    {
        return serialize(array(
            $this->idUsuario,
            $this->userName,
            $this->password,
            // see section on salt below
            // $this->salt,
        ));
    }
    
    /** @see \Serializable::unserialize() */
    public function unserialize($serialized)
    {
        list (
            $this->idUsuario,
            $this->userName,
            $this->password,
            // see section on salt below
            // $this->salt
        ) = unserialize($serialized);
    }
    
     public function getSalt()
    {
        // you *may* need a real salt depending on your encoder
        // see section on salt below
        return null;
    }
    
    /*public function getPlainPassword()
    {
        return $this->plainPassword;
    }

    public function setPlainPassword($password)
    {
        $this->plainPassword = $password;
    }*/

   public function isPasswordLegal()
    {
    return $this->userName !== $this->password;
    }
   
    /**
     * Set fotoUser
     *
     * @param string $fotoUser
     * @return Usuario
     */
    public function setFotoUser($fotoUser)
    {
        $this->fotoUser = $fotoUser;

        return $this;
    }

    /**
     * Get fotoUser
     *
     * @return string
     */
    public function getFotoUser()
    {
        return $this->fotoUser;
    }
}
