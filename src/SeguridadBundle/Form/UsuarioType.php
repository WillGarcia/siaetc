<?php

namespace SeguridadBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\FileType;

class UsuarioType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('tipoUsuario', ChoiceType::class, array(
                  'choices'  => array(
                  'Administrador' => 'ROLE_ADMIN',
                  'Director de Clinica' => 'ROLE_TURNO_CLINICO',
                  'Personal de Archivo' => 'ROLE_ARCHIVO',
                  'Personal de Citas' => 'ROLE_CITA',
                  'Secretaria' => 'ROLE_SECRETARIA',
                  'Estudiante' => 'ROLE_ESTUDIANTE',
                  ),
                  'choices_as_values' => true,
                  'placeholder' => 'Seleccionar tipo de usuario',
                  ))
            ->add('userName')
            ->add('password', RepeatedType::class, array(
                  'type' => PasswordType::class,
                  'first_options'  => array('label' => 'Password: '),
                  'second_options' => array('label' => 'Repetir Password: '),'invalid_message'=>'No coinciden las contraseņas',
                  )
                 )
            ->add('email', EmailType::class)
            ->add('fotoUser',FileType::class,array('required'=>false,))
            //->addEventListener(FormEvents::SUBMIT,array($this, 'estudiantePreSetData'))
        ;
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'SeguridadBundle\Entity\Usuario'
        ));
    }
    
     public function onPreSetData(FormEvent $event)
    {
        //$pass = $event->get('password')-> getData();
        $user=$event->getData();
        $form = $event->getForm();

        if (!$form->get('password')->getData()) {
            $form->get('email')->setData('TRUE@gmail.com');
            $user['email']='holaholahola@gmail.com';
            $event->setData($user); 
            //$em = $this->getDoctrine()->getManager();
            //$passOld  = $em->getRepository('SeguridadBundle:Usuario')->findByUserName($user['userName']);
        }
        else{
          //$user['password']='holaholahola'; 
           $form->get('email')->setData('FALSE@gmail.com');
           $user['email']='holaholahola@gmail.com'; 
           $event->setData($user);  
        }
    }//onPresSetData
    
     public function estudiantePreSetData(FormEvent $event)
    {
        $user=$event->getData();
        $form = $event->getForm();
        if($form->get('tipoUsuario')->getData()=='ROLE_ESTUDIANTE'){
            $estudiante=$this->getDoctrine()->getRepository('FichaEndodonciaBundle:Estudiante')->find($form->get('UserName')->getData());
            if(!$estudiante){
			throw $this-> createNotFoundException('Estudiante no encontrado. No existe en la base de datos.');
		}
       }//if
        
    }//estudiantePresSetData
}
