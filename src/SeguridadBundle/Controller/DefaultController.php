<?php

namespace SeguridadBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('SeguridadBundle:Default:index.html.twig');
    }
    public function accesoDenegadoAction()
    {
        return $this->render(':acceso-denegado:error403.html.twig');
    }
}
