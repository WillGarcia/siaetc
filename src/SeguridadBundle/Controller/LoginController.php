<?php

namespace SeguridadBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use SeguridadBundle\Entity\Usuario;

class LoginController extends Controller
{
   public function loginAction(Request $request)
    {
      if ($this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')) {
            return $this -> redirectToRoute('admin_homepage');
         }elseif($this->get('security.authorization_checker')->isGranted('ROLE_ESTUDIANTE')){
            return $this -> redirectToRoute('estudiante_homepage');
            }elseif($this->get('security.authorization_checker')->isGranted('ROLE_CITA')){
              return $this -> redirectToRoute('cita_homepage');  
            }
      
        $authenticationUtils = $this->get('security.authentication_utils');

        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();
        
        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();
         return $this->render(
        'SeguridadBundle:login:login.html.twig',
        array(
            // last username entered by the user
            'last_username' => $lastUsername,
            'error'         => $error,
             )
         );
    }
}
