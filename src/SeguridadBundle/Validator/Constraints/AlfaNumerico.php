<?php

/**
 * @author Jorge Castro
 * @copyright 2016
 */

namespace SeguridadBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class AlfaNumerico extends Constraint
{
    public $message = 'La cadena "%string%" contiene caracteres no v�lidos';
}

?>