<?php

use Symfony\Component\Routing\Exception\MethodNotAllowedException;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Routing\RequestContext;

/**
 * appDevUrlMatcher.
 *
 * This class has been auto-generated
 * by the Symfony Routing Component.
 */
class appDevUrlMatcher extends Symfony\Bundle\FrameworkBundle\Routing\RedirectableUrlMatcher
{
    /**
     * Constructor.
     */
    public function __construct(RequestContext $context)
    {
        $this->context = $context;
    }

    public function match($pathinfo)
    {
        $allow = array();
        $pathinfo = rawurldecode($pathinfo);
        $context = $this->context;
        $request = $this->request;

        if (0 === strpos($pathinfo, '/_')) {
            // _wdt
            if (0 === strpos($pathinfo, '/_wdt') && preg_match('#^/_wdt/(?P<token>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => '_wdt')), array (  '_controller' => 'web_profiler.controller.profiler:toolbarAction',));
            }

            if (0 === strpos($pathinfo, '/_profiler')) {
                // _profiler_home
                if (rtrim($pathinfo, '/') === '/_profiler') {
                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', '_profiler_home');
                    }

                    return array (  '_controller' => 'web_profiler.controller.profiler:homeAction',  '_route' => '_profiler_home',);
                }

                if (0 === strpos($pathinfo, '/_profiler/search')) {
                    // _profiler_search
                    if ($pathinfo === '/_profiler/search') {
                        return array (  '_controller' => 'web_profiler.controller.profiler:searchAction',  '_route' => '_profiler_search',);
                    }

                    // _profiler_search_bar
                    if ($pathinfo === '/_profiler/search_bar') {
                        return array (  '_controller' => 'web_profiler.controller.profiler:searchBarAction',  '_route' => '_profiler_search_bar',);
                    }

                }

                // _profiler_purge
                if ($pathinfo === '/_profiler/purge') {
                    return array (  '_controller' => 'web_profiler.controller.profiler:purgeAction',  '_route' => '_profiler_purge',);
                }

                // _profiler_info
                if (0 === strpos($pathinfo, '/_profiler/info') && preg_match('#^/_profiler/info/(?P<about>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_info')), array (  '_controller' => 'web_profiler.controller.profiler:infoAction',));
                }

                // _profiler_phpinfo
                if ($pathinfo === '/_profiler/phpinfo') {
                    return array (  '_controller' => 'web_profiler.controller.profiler:phpinfoAction',  '_route' => '_profiler_phpinfo',);
                }

                // _profiler_search_results
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/search/results$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_search_results')), array (  '_controller' => 'web_profiler.controller.profiler:searchResultsAction',));
                }

                // _profiler
                if (preg_match('#^/_profiler/(?P<token>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler')), array (  '_controller' => 'web_profiler.controller.profiler:panelAction',));
                }

                // _profiler_router
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/router$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_router')), array (  '_controller' => 'web_profiler.controller.router:panelAction',));
                }

                // _profiler_exception
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/exception$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_exception')), array (  '_controller' => 'web_profiler.controller.exception:showAction',));
                }

                // _profiler_exception_css
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/exception\\.css$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_exception_css')), array (  '_controller' => 'web_profiler.controller.exception:cssAction',));
                }

            }

            // _twig_error_test
            if (0 === strpos($pathinfo, '/_error') && preg_match('#^/_error/(?P<code>\\d+)(?:\\.(?P<_format>[^/]++))?$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => '_twig_error_test')), array (  '_controller' => 'twig.controller.preview_error:previewErrorPageAction',  '_format' => 'html',));
            }

        }

        if (0 === strpos($pathinfo, '/fperiodoncia')) {
            // fperiodoncia_index
            if (rtrim($pathinfo, '/') === '/fperiodoncia') {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_fperiodoncia_index;
                }

                if (substr($pathinfo, -1) !== '/') {
                    return $this->redirect($pathinfo.'/', 'fperiodoncia_index');
                }

                return array (  '_controller' => 'foues\\FPBundle\\Controller\\FPeriodonciaController::indexAction',  '_route' => 'fperiodoncia_index',);
            }
            not_fperiodoncia_index:

            // fperiodoncia_show
            if (preg_match('#^/fperiodoncia/(?P<id>[^/]++)/show$#s', $pathinfo, $matches)) {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_fperiodoncia_show;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'fperiodoncia_show')), array (  '_controller' => 'foues\\FPBundle\\Controller\\FPeriodonciaController::showAction',));
            }
            not_fperiodoncia_show:

            // fperiodoncia_new
            if ($pathinfo === '/fperiodoncia/new') {
                if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                    goto not_fperiodoncia_new;
                }

                return array (  '_controller' => 'foues\\FPBundle\\Controller\\FPeriodonciaController::newAction',  '_route' => 'fperiodoncia_new',);
            }
            not_fperiodoncia_new:

            // fperiodoncia_edit
            if (preg_match('#^/fperiodoncia/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
                if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                    goto not_fperiodoncia_edit;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'fperiodoncia_edit')), array (  '_controller' => 'foues\\FPBundle\\Controller\\FPeriodonciaController::editAction',));
            }
            not_fperiodoncia_edit:

            // fperiodoncia_delete
            if (preg_match('#^/fperiodoncia/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
                if ($this->context->getMethod() != 'DELETE') {
                    $allow[] = 'DELETE';
                    goto not_fperiodoncia_delete;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'fperiodoncia_delete')), array (  '_controller' => 'foues\\FPBundle\\Controller\\FPeriodonciaController::deleteAction',));
            }
            not_fperiodoncia_delete:

            // fperiodoncia_periodonto
            if ($pathinfo === '/fperiodoncia/p_odontograma') {
                if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                    goto not_fperiodoncia_periodonto;
                }

                return array (  '_controller' => 'foues\\FPBundle\\Controller\\FPeriodonciaController::periodontoAction',  '_route' => 'fperiodoncia_periodonto',);
            }
            not_fperiodoncia_periodonto:

        }

        // foues_fp_homepage
        if (rtrim($pathinfo, '/') === '') {
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', 'foues_fp_homepage');
            }

            return array (  '_controller' => 'foues\\FPBundle\\Controller\\DefaultController::indexAction',  '_route' => 'foues_fp_homepage',);
        }

        if (0 === strpos($pathinfo, '/paciente')) {
            // paciente_index
            if (rtrim($pathinfo, '/') === '/paciente') {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_paciente_index;
                }

                if (substr($pathinfo, -1) !== '/') {
                    return $this->redirect($pathinfo.'/', 'paciente_index');
                }

                return array (  '_controller' => 'foues\\FDBundle\\Controller\\PacienteController::indexAction',  '_route' => 'paciente_index',);
            }
            not_paciente_index:

            // paciente_show
            if (preg_match('#^/paciente/(?P<id>[^/]++)/show$#s', $pathinfo, $matches)) {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_paciente_show;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'paciente_show')), array (  '_controller' => 'foues\\FDBundle\\Controller\\PacienteController::showAction',));
            }
            not_paciente_show:

            // paciente_new
            if ($pathinfo === '/paciente/new') {
                if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                    goto not_paciente_new;
                }

                return array (  '_controller' => 'foues\\FDBundle\\Controller\\PacienteController::newAction',  '_route' => 'paciente_new',);
            }
            not_paciente_new:

            // paciente_edit
            if (preg_match('#^/paciente/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
                if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                    goto not_paciente_edit;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'paciente_edit')), array (  '_controller' => 'foues\\FDBundle\\Controller\\PacienteController::editAction',));
            }
            not_paciente_edit:

            // paciente_delete
            if (preg_match('#^/paciente/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
                if ($this->context->getMethod() != 'DELETE') {
                    $allow[] = 'DELETE';
                    goto not_paciente_delete;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'paciente_delete')), array (  '_controller' => 'foues\\FDBundle\\Controller\\PacienteController::deleteAction',));
            }
            not_paciente_delete:

        }

        if (0 === strpos($pathinfo, '/fdiagnostico')) {
            // fdiagnostico_index
            if (rtrim($pathinfo, '/') === '/fdiagnostico') {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_fdiagnostico_index;
                }

                if (substr($pathinfo, -1) !== '/') {
                    return $this->redirect($pathinfo.'/', 'fdiagnostico_index');
                }

                return array (  '_controller' => 'foues\\FDBundle\\Controller\\FDiagnosticoController::indexAction',  '_route' => 'fdiagnostico_index',);
            }
            not_fdiagnostico_index:

            // fdiagnostico_show
            if (preg_match('#^/fdiagnostico/(?P<id>[^/]++)/show$#s', $pathinfo, $matches)) {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_fdiagnostico_show;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'fdiagnostico_show')), array (  '_controller' => 'foues\\FDBundle\\Controller\\FDiagnosticoController::showAction',));
            }
            not_fdiagnostico_show:

            // fdiagnostico_new
            if ($pathinfo === '/fdiagnostico/new') {
                if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                    goto not_fdiagnostico_new;
                }

                return array (  '_controller' => 'foues\\FDBundle\\Controller\\FDiagnosticoController::newAction',  '_route' => 'fdiagnostico_new',);
            }
            not_fdiagnostico_new:

            // fdiagnostico_edit
            if (preg_match('#^/fdiagnostico/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
                if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                    goto not_fdiagnostico_edit;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'fdiagnostico_edit')), array (  '_controller' => 'foues\\FDBundle\\Controller\\FDiagnosticoController::editAction',));
            }
            not_fdiagnostico_edit:

            // foues_fd_select_municipio
            if ($pathinfo === '/fdiagnostico/select_municipio') {
                if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                    goto not_foues_fd_select_municipio;
                }

                return array (  '_controller' => 'foues\\FDBundle\\Controller\\FDiagnosticoController::selectMunicipioAction',  '_route' => 'foues_fd_select_municipio',);
            }
            not_foues_fd_select_municipio:

            // fdiagnostico_delete
            if (preg_match('#^/fdiagnostico/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
                if ($this->context->getMethod() != 'DELETE') {
                    $allow[] = 'DELETE';
                    goto not_fdiagnostico_delete;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'fdiagnostico_delete')), array (  '_controller' => 'foues\\FDBundle\\Controller\\FDiagnosticoController::deleteAction',));
            }
            not_fdiagnostico_delete:

        }

        if (0 === strpos($pathinfo, '/expediente')) {
            // expediente_index
            if (rtrim($pathinfo, '/') === '/expediente') {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_expediente_index;
                }

                if (substr($pathinfo, -1) !== '/') {
                    return $this->redirect($pathinfo.'/', 'expediente_index');
                }

                return array (  '_controller' => 'foues\\FDBundle\\Controller\\ExpedienteController::indexAction',  '_route' => 'expediente_index',);
            }
            not_expediente_index:

            // expediente_show
            if (preg_match('#^/expediente/(?P<id>[^/]++)/show$#s', $pathinfo, $matches)) {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_expediente_show;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'expediente_show')), array (  '_controller' => 'foues\\FDBundle\\Controller\\ExpedienteController::showAction',));
            }
            not_expediente_show:

            // expediente_new
            if ($pathinfo === '/expediente/new') {
                if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                    goto not_expediente_new;
                }

                return array (  '_controller' => 'foues\\FDBundle\\Controller\\ExpedienteController::newAction',  '_route' => 'expediente_new',);
            }
            not_expediente_new:

            // expediente_edit
            if (preg_match('#^/expediente/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
                if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                    goto not_expediente_edit;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'expediente_edit')), array (  '_controller' => 'foues\\FDBundle\\Controller\\ExpedienteController::editAction',));
            }
            not_expediente_edit:

            // expediente_delete
            if (preg_match('#^/expediente/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
                if ($this->context->getMethod() != 'DELETE') {
                    $allow[] = 'DELETE';
                    goto not_expediente_delete;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'expediente_delete')), array (  '_controller' => 'foues\\FDBundle\\Controller\\ExpedienteController::deleteAction',));
            }
            not_expediente_delete:

        }

        if (0 === strpos($pathinfo, '/fichaclinica')) {
            // fichaclinica_index
            if (rtrim($pathinfo, '/') === '/fichaclinica') {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_fichaclinica_index;
                }

                if (substr($pathinfo, -1) !== '/') {
                    return $this->redirect($pathinfo.'/', 'fichaclinica_index');
                }

                return array (  '_controller' => 'foues\\FDBundle\\Controller\\FichaClinicaController::indexAction',  '_route' => 'fichaclinica_index',);
            }
            not_fichaclinica_index:

            // fichaclinica_show
            if (preg_match('#^/fichaclinica/(?P<id>[^/]++)/show$#s', $pathinfo, $matches)) {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_fichaclinica_show;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'fichaclinica_show')), array (  '_controller' => 'foues\\FDBundle\\Controller\\FichaClinicaController::showAction',));
            }
            not_fichaclinica_show:

            // fichaclinica_new
            if ($pathinfo === '/fichaclinica/new') {
                if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                    goto not_fichaclinica_new;
                }

                return array (  '_controller' => 'foues\\FDBundle\\Controller\\FichaClinicaController::newAction',  '_route' => 'fichaclinica_new',);
            }
            not_fichaclinica_new:

            // fichaclinica_edit
            if (preg_match('#^/fichaclinica/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
                if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                    goto not_fichaclinica_edit;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'fichaclinica_edit')), array (  '_controller' => 'foues\\FDBundle\\Controller\\FichaClinicaController::editAction',));
            }
            not_fichaclinica_edit:

            // fichaclinica_delete
            if (preg_match('#^/fichaclinica/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
                if ($this->context->getMethod() != 'DELETE') {
                    $allow[] = 'DELETE';
                    goto not_fichaclinica_delete;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'fichaclinica_delete')), array (  '_controller' => 'foues\\FDBundle\\Controller\\FichaClinicaController::deleteAction',));
            }
            not_fichaclinica_delete:

        }

        // foues_fd_homepage
        if (rtrim($pathinfo, '/') === '') {
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', 'foues_fd_homepage');
            }

            return array (  '_controller' => 'foues\\FDBundle\\Controller\\DefaultController::indexAction',  '_route' => 'foues_fd_homepage',);
        }

        // homepage
        if (rtrim($pathinfo, '/') === '') {
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', 'homepage');
            }

            return array (  '_controller' => 'AppBundle\\Controller\\DefaultController::indexAction',  '_route' => 'homepage',);
        }

        if (0 === strpos($pathinfo, '/log')) {
            // login
            if ($pathinfo === '/login') {
                return array (  '_controller' => 'SeguridadBundle\\Controller\\LoginController::loginAction',  '_route' => 'login',);
            }

            // logout
            if ($pathinfo === '/logout') {
                return array('_route' => 'logout');
            }

        }

        throw 0 < count($allow) ? new MethodNotAllowedException(array_unique($allow)) : new ResourceNotFoundException();
    }
}
